import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { defaultVars } from 'src/environments/environment';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css']
})
export class RegisterEmployeeComponent implements OnInit {

  public employeeRegistrationGroup!: FormGroup;
  private registerService: RegisterService = new RegisterService(this.httpClient, this.router, this.modalService);

  constructor(private router: Router, private httpClient: HttpClient, private modalService: NgbModal) {
    this.employeeRegistrationGroup = new FormGroup({
      firstName : new FormControl('', [
        Validators.required,
        Validators.min(2),
        Validators.max(25)
      ]),
      lastName : new FormControl('', [
        Validators.required,
        Validators.min(2),
        Validators.max(5)
      ]),
      password : new FormControl('', [
        Validators.required
      ]),
      passwordConfirm : new FormControl('', [
        Validators.required
      ]),
      email : new FormControl('', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.pattern("(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$")
      ]),
      securityQuestion : new FormControl('', [
        Validators.required
      ]),
      answer : new FormControl ('', [
        Validators.required
      ])
    });
  }

  public get firstName(){
    return this.employeeRegistrationGroup.get('firstName');
  }

  public get lastName(){
    return this.employeeRegistrationGroup.get('lastName');
  }

  public get password(){
    return this.employeeRegistrationGroup.get('password');
  }

  public get passwordConfirm(){
    return this.employeeRegistrationGroup.get('passwordConfirm');
  }

  public get email(){
    return this.employeeRegistrationGroup.get('email');
  }

  public get phoneNumber(){
    return this.employeeRegistrationGroup.get('phoneNumber');
  }

  public get securityQuestion(){
    return this.employeeRegistrationGroup.get('securityQuestion');
  }

  public get answer(){
    return this.employeeRegistrationGroup.get('answer');
  }

  ngOnInit(): void {
    
  }

  public registerEmployee(){

    if(this.employeeRegistrationGroup.valid){
      console.log('ajunge aici');
      // if(this.checkPasswordValidity(this.employeeRegistrationGroup.value.password, this.employeeRegistrationGroup.value.confirmPassword) == true){
        console.log('si aici');
        const registration_values = {
          "firstName" : this.employeeRegistrationGroup.value.firstName,
          "lastName" : this.employeeRegistrationGroup.value.lastName,
          "password" : this.employeeRegistrationGroup.value.password,
          "emailAddress" : this.employeeRegistrationGroup.value.email,
          "phoneNumber" : this.employeeRegistrationGroup.value.phoneNumber,
          "securityQuestion" : this.employeeRegistrationGroup.value.securityQuestion,
          "securityQuestionAnswer" : this.employeeRegistrationGroup.value.answer,
          "address": "Str. Florilor, nr.30",
        };
        
        this.registerService.EmployeeRegister(registration_values).then((response) => {
          this.redirectLoginForm(true);
        })
      // }
    }
  }

  public checkPasswordValidity(password: any, confirmPassword : any) : Boolean {
    if(password != '' || confirmPassword != ''){
      if(password === confirmPassword){
        console.log('aici nu e nimic');
          return true;
      } else {
        return false;
      }
    }
    return false;
  }

  private redirectLoginForm(value: boolean){
    if(value == true){
      this.router.navigate(['/login']);
    }
  }

  // register(form: NgForm){
  //   const registerData = {
  //     "firstName": form.value.firstName,
  //     "lastName": form.value.lastName,
  //     "address": form.value.address,
  //     "phoneNumber": form.value.phoneNumber,
  //     "emailAddress": form.value.emailAddress,
  //     "password": form.value.password,
  //     "positionId": 3
  //   };

  //   this.httpClient.post(defaultVars.debugURI + "form", registerData)
  //     .subscribe(response => {
  //       this.ngOnInit();
  //     });
  // }

}
