import { formatDate } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { throwIfEmpty } from 'rxjs/operators';
import { isThisTypeNode } from 'typescript';
import { Department } from '../objects/Department';
import { Position } from '../objects/Position';
import { DepartmentService } from '../_services/department.service';
import { PositionService } from '../_services/position.service';


export class CustomErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null) : boolean {
    const isSubmitted = form && form.submitted;
    // return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    return control! && control!.invalid && control!.touched;
  }
}


@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})

export class PositionComponent implements OnInit {

  // Table columns elements
  public dataSource = new  MatTableDataSource<Position>();
  public displayedColumns: string[] = ['positionName', 'departmentName', 'availableRecruiting', 'vacantPosition', 'updatePosition', 'deletePosition'];
  public filteredColumns: string[] = ['positionNameFilter', 'departmentNameFilter'];
  public timer = 1000;
  public timeoutVal = 1000;

  //devextreme + angular materials - Positions 
  public isUpdatePopupVisible: boolean = false;
  public isAddPopupVisible: boolean = false;
  public isDeletePopupVisibile: boolean = false;
  public isAvailable: boolean = false;

  //devextreme + angular materials - Departments
  public isUpdateDepartmentVisible: boolean = false;
  public isAddDepartmentVisible: boolean = false;
  public isDeleteDepartmentVisible: boolean = false;
  public isAvailableDepartment: boolean = false;

  //FormGroups departments
  public departmentUpdateForm!: FormGroup;
  public departmentDetails!: FormGroup;

  //current elements
  public crtDepartment!: Department;

  //other elements
  public departments: Department[] = [];


  // paginator
  public pageLength: number = 0;
  public splicedData!: any[];
  public requests: any;
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  public sortedData!: any;

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort, {static: false}) sort: MatSort | undefined;
  @ViewChild('f') addPositionForm;

  positions: Position[] = [];
  closedResults!: string;
  editForm!: FormGroup;
  vacantForm!: FormGroup;
  positionDetails!: FormGroup;

  private updateId?: number = 0;
  private deleteId?: number = 0;

  matcher = new CustomErrorStateMatcher();
  selectionMatcher = new CustomErrorStateMatcher();
  descriptionMatcher = new CustomErrorStateMatcher();
  availablePositionsMatcher = new CustomErrorStateMatcher();

  departmentMatcher = new CustomErrorStateMatcher();
  departmentDescriptionMatcher = new CustomErrorStateMatcher();

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private positionService: PositionService,
    private departmentService: DepartmentService
  ) {
    this.editForm = this.formBuilder.group({
      positionName: [''],
      department: ['']
    });

    this.vacantForm = this.formBuilder.group({
      positionName: [''],
      department: [''],
      noVacantPositions: [''],
      availablePosition: ['']
    });

    this.positionDetails = new FormGroup({
      positionName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]),
      department: new FormControl('', [
        Validators.required
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ]),
      noAvailablePositions: new FormControl('', [
        Validators.required
      ])
    });

    this.departmentUpdateForm = new FormGroup({
      departmentName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ]),
      noOfPositions: new FormControl('', [
        Validators.required
      ]),
      availableDepartment: new FormControl('', [
        Validators.required
      ])
    });

    this.departmentDetails = new FormGroup({
      departmentName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ]),
      availableDepartment: new FormControl('', [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.getAllPositions();
    this.getDepartments();
  }

  public getDepartments() {
    this.departmentService.getDepartments().then(async response => {
      this.departments = [];
      response.forEach(department => {
        this.departments.push(department);
      });
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }


  // Sort methods
  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if(!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch(sort.active) {
        case 'positionName': return this.compare(a.positionName!, b.positionName, isAsc);
        case 'departmentName': return this.compare(a.department!, b.department!, isAsc);
        default: return 0;
      }
    });
  }

  public compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1: -1);
  }

  // Filters
  public applyPositionNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getPositionsByNameFilter(elements);
    }, this.timeoutVal);
  }

  public getPositionsByNameFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllPositions();
      } else {
        this.positionService.customSearchByPosition(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public applyDepartmentNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getPositionsByDepartmentFilter(elements);
    }, this.timeoutVal);
  }

  public getPositionsByDepartmentFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllPositions();
      } else {
        this.positionService.customSearchByDepartment(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }


  pageChangeEvent(event) {
    const offset = ((event.pageIndex + 1) - 1) * event.pageSize;
    this.splicedData = this.positions.slice(offset).slice(0, event.pageSize);
  }

  public getAllPositions() {
    this.positionService.getCurrentPositions().then(async response => {
      let retrievedPositions = response;
      this.positions = [];
      retrievedPositions.forEach(position => {
        if (position.positionName != 'Candidate registered') {
          this.positions.push(position);
        }
      });

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closedResults = `Closed with: ${result}`;
    }, (reason) => {
      this.closedResults = `Dismissied ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on backdrop';
    } else {
      return `with ${reason}`;
    }
  }

  openEdit(targetModal: any, position: Position) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.updateId = position.positionId;

    this.editForm?.patchValue({
      positionName: position.positionName,
      department: position.department
    });
  }

  onSave() {
    this.positionService.updatePositionDetails(this.editForm.value, this.updateId!).then((response) => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public openVacantEdit(targetModal: any, position: Position) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.updateId = position.positionId;

    this.vacantForm?.patchValue({
      positionName: position.positionName,
      department: position.department,
      noVacantPositions: position.noVacantPositions
    });
  }

  // Update vacant positions method
  public onSaveVacantUpdate() {
    let noOfVacantPositions = this.vacantForm.value.noVacantPositions;
    if (noOfVacantPositions < 0) {
      this.toastr.warning("Cannot set the number of vacant positions to a negative value");
    } else {
      let vacantFormValues = {
        positionName: this.vacantForm.value.positionName,
        departmentId: this.vacantForm.value.department,
        noVacantPositions: this.vacantForm.value.noVacantPositions
      };

      this.positionService.updateVacantPositions(vacantFormValues, this.updateId!).then(async results => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${results}`);
        this.modalService.dismissAll();
        this.clearCrtDepartment();
        this.isUpdatePopupVisible = false;
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
        this.isUpdatePopupVisible = false;
      });
    }
  }


  //Update department details function
  public onSaveDepartmentDetails() {
    let departmentUpdateDetails = {
      departmentName: this.departmentUpdateForm.value.departmentName,
      description: this.departmentUpdateForm.value.description,
      active: this.departmentUpdateForm.value.availableDepartment
    };

    this.departmentService.updateDepartmentDetails(this.crtDepartment.departmentId, departmentUpdateDetails).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeUpdateDepartmentPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  //clear functions
  public clearCrtDepartment() {
    this.crtDepartment = undefined!;
  }

  public openDelete(targetModal: any, position: Position) {
    this.deleteId = position.positionId;

    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  public onDelete() {
    this.positionService.removePosition(this.deleteId!).then(async response => {
      this.ngOnInit();
      this.closeDeletePopup();
      this.toastr.success(`Server response: ${response}`);
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onSubmit(f: any) {
    // const headers = new HttpHeaders().set('Content-Type', 'application/json');

    var positionDetails = {
      positionName: f.value.positionName,
      departmentId: f.value.department,
      description: f.value.description,
      noVacantPositions: f.value.noAvailablePositions
    };

    console.log(positionDetails);

    if(this.positionDetails.valid) {
      this.positionService.createNewPosition(positionDetails).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeAddPositionPopoup();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }

    // if (this.positionDetails.valid) {
    //   this.positionService.createNewPosition(f.value, headers).then((response) => {
    //     this.ngOnInit();
    //     this.modalService.dismissAll();
    //   });
    // }
  }

  public exportData() {
    this.positionService.exportData().then((response) => {

      this.downloadFile(response);

      this.toastr.success("The document was succesfully loaded. Please wait till is fully downloaded");
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public downloadFile(data: any) {
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, 'dd/MM/yyyy HH:mm:ss', 'en-US');

    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Positions ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }


  //Position getters
  get positionName() {
    return this.positionDetails.get('positionName');
  }

  get department() {
    return this.positionDetails.get('department');
  }

  public get description() {
    return this.positionDetails.get('description');
  }

  public get noAvailablePositions() {
    return this.positionDetails.get('noAvailablePositions');
  }

  public showIndex(event: any) {
    console.log(event);
  }

  //Department getters

  public get departmentName() {
    return this.departmentDetails.get('departmentName');
  }

  public get descriptionDepartment() {
    return this.departmentDetails.get('description');
  }

  public get availableDepartment() {
    return this.departmentDetails.get('availableDepartment');
  }


  //new functions to call popup forms available in devextreme - Positions

  public openUpdateDialog = (event):void => {
    this.isUpdatePopupVisible = !this.isUpdatePopupVisible;

    var crtPosition = this.positions[event.row.rowIndex];
    this.crtDepartment = this.positions[event.row.rowIndex].department!;

    this.updateId = crtPosition.positionId;

    this.vacantForm?.patchValue({
      positionName: crtPosition.positionName,
      noVacantPositions: crtPosition.noVacantPositions
    });
    
    this.vacantForm.controls['department'].setValue(this.crtDepartment.departmentId);
    this.isAvailable = crtPosition.availableForRecruting!;
    // this.vacantForm.controls['availablePosition'].setValue(crtPosition.availableForRecruting);
  }

  public openDeleteDialog = (event): void => {
    this.isDeletePopupVisibile = !this.isDeletePopupVisibile;
    var crtPosition = this.positions[event.row.rowIndex];
    this.deleteId = crtPosition.positionId;
  }

  public openAddPositionDialog() {
    this.isAddPopupVisible = !this.isAddPopupVisible;
  }

  //new functions to call popup forms available in devextreme - Departments
  public openUpdateDepartmentDialog = (event): void => {
    this.isUpdateDepartmentVisible = !this.isUpdateDepartmentVisible;
    this.crtDepartment = this.departments[event.row.rowIndex];
    
    this.departmentUpdateForm?.patchValue({
      departmentName: this.crtDepartment.departmentName,
      noOfPositions: this.crtDepartment.currentNumberOfPositions,
      description: this.crtDepartment.description
    });

    this.isAvailableDepartment = this.crtDepartment.active;
  }

  public opeDeleteDepartmentDialog = (event) : void => {
    this.isDeleteDepartmentVisible = !this.isDeleteDepartmentVisible;
    this.crtDepartment = this.departments[event.row.rowIndex];
  }

  public openAddDepartmentDialog() {
    this.isAddDepartmentVisible = !this.isAddDepartmentVisible;
  }

  public onSubmitDepartment(f: any) {
    var departmentDetailsForm = {
      departmentName: f.value.departmentName,
      description: f.value.description,
      active: f.value.availableDepartment
    };

    console.log(departmentDetailsForm);

    if(this.departmentDetails.valid) {
      this.departmentService.addDepartment(departmentDetailsForm).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeAddDepartmentPopup();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }
  }


  //functions to call departments operations
  public onDeleteDepartment() {
    this.departmentService.removeDepartment(this.crtDepartment.departmentId).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response ${response}`);
      this.closeDeleteDepartmentPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onDonwloadDepartmentsExcel() {
    this.departmentService.sendExportRequest().then(async response => {
      this.downloadDepartmentExcel(response);
      this.toastr.success("The document was succesfully loaded. Please wait till is fully downloaded");
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }
  
  public downloadDepartmentExcel(data: any) {
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, 'dd/MM/yyyy HH:mm:ss', 'en-US');

    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Departments ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  //functions to close Popups - Positions
  public closePopup() {
    this.isUpdatePopupVisible = !this.isUpdatePopupVisible;
  }

  public closeDeletePopup() {
    this.isDeletePopupVisibile = !this.isDeletePopupVisibile;
  }

  public closeAddPositionPopoup() {
    this.isAddPopupVisible = !this.isAddPopupVisible;
    this.addPositionForm.resetForm();
  }


  //functions to close Popups - Departments
  public closeUpdateDepartmentPopup() {
    this.isUpdateDepartmentVisible = !this.isUpdateDepartmentVisible;
  }

  public closeAddDepartmentPopup() {
    this.isAddDepartmentVisible = !this.isAddDepartmentVisible;
  } 

  public closeDeleteDepartmentPopup() {
    this.isDeletePopupVisibile = !this.isDeletePopupVisibile;
  }
}
