import { PortalHostDirective } from '@angular/cdk/portal';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PositionService } from '../_services/position.service';

import { PositionComponent } from './position.component';

describe('PositionComponent', () => {
  let component: PositionComponent;
  let fixture: ComponentFixture<PositionComponent>;
  let positionService: PositionService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ PositionComponent ],
      imports: [HttpClientModule],
      providers: [PositionService]
    });
   
    positionService = TestBed.inject(PositionService);
  });

  // it('should retrieve all posts from API via GET', () => {
  //   positionService.getCurrentPositions().then(async positions => {
  //     expect(positions.length).toBe(2);
  //   });
  // });

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(PositionComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
