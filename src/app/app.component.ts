import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Notification } from './objects/Notification';
import { User } from './objects/User';
import { TransferService } from './services/transfer.service';
import { AccountService } from './_services/account.service';
import { NotificationService } from './_services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'InterviewsNET-UI';
  returnUrl!: string;
  submitted = false;
  loading = false;
  error = '';
  public currentUser!: Observable<User>;

  public availableNotifications!: Notification[];

  public numberOfNotifications!: number;

  public obtainedUser!: User;

  public hidden = false;

  public profilePhotoURL = "";

  public redirectLinkProfile = "";

  public enabledNotifications: boolean = false;

  userType: string = '';

  public model: any = {};

  constructor(
    private transferService: TransferService,
    private toastr: ToastrService,
    private notificationService: NotificationService,
    private router: Router,
    private accountService: AccountService) {
    // this.getCurrentUser();

    this.obtainedUser = this.accountService.currentUserValue;
    this.getAvailableNotifications();
  }

  ngOnInit() {
    $("#wrapper").hide();
    $("#button-toggler").on("click", function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }

  public get username() {    
    if (this.obtainedUser.role != "Anonymous") {
      let customName = this.obtainedUser.firstName + " " + this.obtainedUser.lastName;
      return customName;
    } else {
      let customName = "Anonymous";
      return customName;
    }
  }

  public get noOfNotifications() {
    return this.numberOfNotifications;
  }

  public get redirectProfileLink() {
    return this.redirectLinkProfile;
  }

  public toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }

  //Account operations

  //Login new method
  public doLogin() {
    console.log(this.model);
    this.accountService.login(this.model).then((response) => {
      let element: HTMLElement = document.getElementById('loginDismissBtn') as HTMLElement;
      element.click();
      this.toastr.success(`You have succesfully logged in. Welcome back ${this.obtainedUser.firstName + " " + this.obtainedUser.lastName}`);
      setTimeout(() => {
        window.location.reload();
      }, 2500);
    }, error => {
      console.log(error);
      this.toastr.error(error.error);
    })


    // this.accountService.login(this.model).subscribe(response => {
    //   let element: HTMLElement = document.getElementById('loginDismissBtn') as HTMLElement;
    //   element.click();
    //   this.getCurrentUser();
    //   this.toastr.success(`You have successfully logged in. Welcome back`);
    //   setTimeout(() => {
    //     window.location.reload();
    //   }, 2500);
    // }, error => {
    //   console.log(error);
    //   this.toastr.error(error.error);
    // });
  }

  //Logout new method
  public doLogout() {
    // this.authenticationService.logout();
    this.accountService.logout();
    this.toastr.success("You have successfully logged out from account");
    setTimeout(() => {
      let element: HTMLElement = document.getElementById('logoutDismissBtn') as HTMLElement;
      element.click();
      window.location.reload();
      this.router.navigateByUrl(' ');
    }, 1500);
  }

  //Get current user - new method
  // public getCurrentUser() {
  //   this.accountService.getCurrentUser().then(async response => {
  //     if (response != null) {
  //       this.obtainedUser = response;
  //       sessionStorage.setItem('role', this.obtainedUser.role!);
  //       if (response.profilePhotoURL == null) {
  //         this.profilePhotoURL = "../assets/images/user-profile-default.png";
  //       } else {
  //         this.profilePhotoURL = response.profilePhotoURL!;
  //       }
  //       this.getAvailableNotifications();
  //     }

  //     if (response.role != 'Anonymous') {
  //       this.redirectLinkProfile = "/profile";
  //     } else {
  //       this.redirectLinkProfile = "#";
  //     }
  //   });
  // }

  //Notifications methods
  public getAvailableNotifications() {
    let currentUserRole = sessionStorage.getItem('userRole');
    console.log(currentUserRole);
    if(currentUserRole != 'Anonymous') {
      console.log('intra aici?');
      if (this.obtainedUser.enableNotifications == true) {
        var receiverId = this.obtainedUser.id!;
        this.notificationService.getNotifications(receiverId).then((response) => {
          this.availableNotifications = response;
          this.enabledNotifications = true;
  
          this.numberOfNotifications = response.length;
        });
      }
    }
  }

  public removeNotification(notification: Notification) {
    var notificationView = {
      userId: this.obtainedUser.id,
      notificationId: notification.notificationId
    };

    this.notificationService.removeNotification(notificationView).then(() => {
      this.showToaster();
      this.getAvailableNotifications();
    });
  }

  public showToaster() {
    this.toastr.success("The notification was successfully removed");
  }

  public SendData(notification: Notification) {
    this.transferService.setData(notification);
    this.CloseModal();
    this.router.navigate(["/notification/details"]);
  }

  public CloseModal() {
    let element: HTMLElement = document.getElementById('btnDismiss') as HTMLElement;
    element.click();
  }


  //Deprecated methods

  //Obtain current user - old method -- To be removed in final version

  // public obtainCurrentUser(){
  //   var currentUser = this.authenticationService.currentUserValue;
  //   let customHeaders = new HttpHeaders({ Authorization: "Bearer " + currentUser.token });

  //   return this.httpClient.get<User>(`${defaultVars.defaultURI}api/auth/currentUser`).toPromise().then(async response => {
  //       if(response != null){
  //         this.obtainedUser = response;
  //         if(response.profilePhotoURL == null) {
  //           this.profilePhotoURL = "../assets/images/user-profile-default.png";
  //         } else {
  //           this.profilePhotoURL = response.profilePhotoURL!;
  //         }
  //         this.getAvailableNotifications();
  //       }

  //       if(response.role != 'Anonymous'){
  //         this.redirectLinkProfile = "/user/profile";
  //       } else {
  //         this.redirectLinkProfile = "#";
  //       }
  //   });
  // }

  //Login old method -- To be removed in final version

  // public doLogin(){ 
  //   this.submitted = true;

  //     if(this.userLogin.invalid){
  //       return;
  //     }

  //     this.loading = true;

  //     this.authenticationService.login(this.f.emailAddress.value, this.f.password.value)
  //       .pipe(first()).subscribe(() => {
  //               console.log('ceva');
  //               this.modalService.dismissAll();
  //               this.router.navigateByUrl('/');
  //           },
  //           error => {
  //             this.error = error;
  //             this.loading = false;
  //           });
  // }

}
