import { Component, OnInit, ChangeDetectionStrategy, AfterContentChecked, ChangeDetectorRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Answer } from '../objects/Answer';
import { Question } from '../objects/Question';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { AnswerService } from '../_services/answer.service';
import { QuestionService } from '../_services/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  public availableQuestions!: Question[];
  
  public answersPanelState: boolean = false;

  public answerQuestionDetailsPanel: boolean = false;

  public questionAnswers!: Answer[];
  
  // Private id's
  private currentQuestionId!: number;
  private currentUser!: User;

  // FormGroup declarations
  public contentQuestionDetailsFG!: FormGroup;
  public contentQuestionUpdateFG!: FormGroup;
  public questionAnswerAdderFG!: FormGroup;
  public items!: FormArray;

  constructor(
    private questionService: QuestionService,
    private accountService: AccountService,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private answerService: AnswerService,
    private ref: ChangeDetectorRef,
    private formBuilder: FormBuilder) {

    this.contentQuestionDetailsFG = new FormGroup({
      questionBodyDetails: new FormControl(''),
      questionCreatedByDetails: new FormControl(''),
      questionCreatedAtDetails: new FormControl(''),
      questionPositionDetails: new FormControl('')
    });

    this.questionAnswerAdderFG = this.formBuilder.group({
      items: this.formBuilder.array([this.createItem()])
    });

    this.currentUser = this.accountService.currentUserValue;
  }

  createItem(): any {
    return this.formBuilder.group({
      answerBody: new FormControl('', Validators.required)
    });
  }

  addItem(): void {
    this.items = this.questionAnswerAdderFG.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  removeItem(i: number) {
    const control = this.questionAnswerAdderFG.get('items') as FormArray;
    control.removeAt(i);
  }

  ngOnInit(): void {
    this.getAllQuestions();
    // this.getCurrentUserId();

  }

  public getAllQuestions() {
    this.questionService.getAllQuestions().then((response) => {
      this.availableQuestions = response;
    }, error => {
      console.log(error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public openQuestionDetails(targetModal: any, question: Question) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.contentQuestionDetailsFG.controls['questionBodyDetails'].setValue(question.questionBody);
    this.contentQuestionDetailsFG.controls['questionCreatedByDetails'].setValue(question.createdBy);
    this.contentQuestionDetailsFG.controls['questionCreatedAtDetails'].setValue(question.createdAt);
    this.contentQuestionDetailsFG.controls['questionPositionDetails'].setValue(question.position);

    this.currentQuestionId = question.questionId;
    this.getQuestionAnswers();
  }

  public openQuestionUpdate(targetModal: any, question: Question) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.currentQuestionId = question.questionId!;
    this.contentQuestionUpdateFG = new FormGroup({
      questionBodyUpdate: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(100)
      ]),
      questionCreatedByUpdate: new FormControl(''),
      questionCreatedAtUpdate: new FormControl(''),
      questionPositionUpdate: new FormControl('')
    });

    this.contentQuestionUpdateFG.controls['questionBodyUpdate'].setValue(question.questionBody);
    this.contentQuestionUpdateFG.controls['questionCreatedByUpdate'].setValue(question.createdBy);
    this.contentQuestionUpdateFG.controls['questionCreatedAtUpdate'].setValue(question.createdAt);
    this.contentQuestionUpdateFG.controls['questionPositionUpdate'].setValue(question.position);
  }

  public openQuestionDelete(targetModal: any, question: Question) {
    this.modalService.open(targetModal,  {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.currentQuestionId = question.questionId!;
  }

  public openQuestionAnswerCreator(targetModal: any) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
  }

  public updateQuestion() {
    let updatedValues = {
      questionBody: this.contentQuestionUpdateFG.value.questionBodyUpdate,
      lastModifiedById: this.currentUser.id
    };

    this.questionService.updateQuestion(this.currentQuestionId, updatedValues).then((response) => {
      this.toastr.success(`Server response: ${response}`);
      this.ngOnInit();
      let element: HTMLElement = document.getElementById('contentQUpdateBtnDismiss') as HTMLElement;
      element.click(); 
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public removeQuestion() {
    this.questionService.removeQuestion(this.currentQuestionId).then((response) => {
      this.toastr.success(`Server response: ${response}`);
      this.ngOnInit();
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public getQuestionAnswers() {
    this.questionService.getQuestionAnswers(this.currentQuestionId).then((response) => {
      this.questionAnswers = response;
    }, error => {
      console.log(error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  // public getCurrentUserId() {
  //   this.accountService.getCurrentUser().then((response) => {
  //     this.currentUserId = response.id!;
  //   }, error => {
  //     console.log(error.error);
  //     this.toastr.error(error.error);
  //   });
  // }

  public submitAnswers() {
    let answersArray: any[] = [];

    this.items.controls.forEach((element, index) => {
      answersArray.push(element.value.answerBody);
    });

    var createdById = this.currentUser.id;

    let finalArray : any[] = [];

    if(answersArray.length > 0) {
      answersArray.forEach(answer => {
        if(answer != '') {
          var answerSend = {
            answerBody: answer,
            questionId: this.currentQuestionId,
            createdById: createdById
          };

          finalArray.push(answerSend);
        }
      });
    }

    if(finalArray.length > 0) {
          this.answerService.addAnswer(finalArray).then((response) => {
            this.toastr.success(`Server response: ${response}`);
            this.modalService.dismissAll();
          }, error => {
            console.log(error.error);
            this.toastr.error(`Error ${error.error}`);
          });
    }
  }

}
