import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { AuthenticationService } from '../guards/AuthenticationService';
import { Position } from '../objects/Position';
import { ApplicationService } from '../services/application.service';
import { TransferService } from '../services/transfer.service';

@Component({
  selector: 'app-user-submission',
  templateUrl: './user-submission.component.html',
  styleUrls: ['./user-submission.component.css']
})
export class UserSubmissionComponent implements OnInit {

  public availablePositions!: Position[];
  private applicationService: ApplicationService = new ApplicationService(this.httpClient, this.authenticationService);
  // private transferService: TransferService = new TransferService(this.router);

  public data!: Position;
  public message!: string;

  constructor(private router: Router, private httpClient: HttpClient, private authenticationService: AuthenticationService, private transferService: TransferService) { }

  ngOnInit(): void {
    this.GetAvailablePositions();
  }

  public GetAvailablePositions(){
    this.applicationService.getAllAvailablePositions().then(response => {
      console.log(response);
      this.availablePositions = response;
    });
  }

  public SendData(position: Position){
    this.data = position;
    // console.log(this.data);

    this.transferService.setData(this.data);
    // console.log(this.data);
    this.router.navigate(["/applications/application_registration"]);
  }


}
