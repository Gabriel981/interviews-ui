import { Component, OnInit } from '@angular/core';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public profilePhotoURL = "";
  
  public redirectLinkProfile = "";

  public obtainedUser!: User;

  constructor(private accountService: AccountService) {
    // this.getCurrentUser();
    this.obtainedUser = this.accountService.currentUserValue;
  }

  ngOnInit(): void {
  }

  public get redirectProfileLink() {
    return this.redirectLinkProfile;
  }

  // public getCurrentUser() {
  //   this.accountService.getCurrentUser().then(async response => {
  //     console.log(response);
  //     if(response != null){
  //       this.obtainedUser = response;
  //       if(response.profilePhotoURL == null) {
  //         this.profilePhotoURL = "../assets/images/user-profile-default.png";
  //       } else {
  //         this.profilePhotoURL = response.profilePhotoURL!;
  //       }
  //     }

  //     if(response.roles != 'Anonymous'){
  //       this.redirectLinkProfile = "/profile";
  //     } else {
  //       this.redirectLinkProfile = "#";
  //     }
  //   });
  // }

}
