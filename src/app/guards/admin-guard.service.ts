import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { AccountService } from '../_services/account.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  constructor(private router: Router, private toastr: ToastrService, private accountService: AccountService) { }

  public canActivate(route: ActivatedRouteSnapshot) {
    let userRole = sessionStorage.getItem('userRole');
    console.log(userRole);
    if(userRole == 'Administrator') {
      return true;
    }

    this.toastr.error("You're not allowed to access the administrator pages");
        setTimeout(() => {
            this.router.navigate(['']);
        }, 1500);

    return false;
  }

}
