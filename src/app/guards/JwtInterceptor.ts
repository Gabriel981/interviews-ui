import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountService } from "../_services/account.service";
import { AuthenticationService } from "./AuthenticationService";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    
    constructor(private accountService: AccountService) { }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // let currentUser = this.authenticationService.currentUserValue;
        let currentUser = this.accountService.currentUserValue;
        if(currentUser && currentUser.jwtToken){
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.jwtToken}`
                }
                // headers: req.headers.set('Authorization', `Bearer ${currentUser.token}`)
            });
        }

        return next.handle(req);
    }
    
}