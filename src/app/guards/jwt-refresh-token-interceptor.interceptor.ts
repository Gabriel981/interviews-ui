import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent
} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { AccountService } from '../_services/account.service';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';
import { User } from '../objects/User';

@Injectable()
export class JwtRefreshTokenInterceptorInterceptor implements HttpInterceptor {

  private isRefreshingToken: boolean = false;
  private tokenSubject: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);

  constructor(private accountService: AccountService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
    return next.handle(this.addTokenToRequest(request, this.accountService.getAuthToken()))
      .pipe(catchError(error => {
        if(error) {
          switch (error.status) {
            case 401:
              return this.handle401Error(request, next);
            case 403:
              return <any>this.accountService.logout;
            default:
              return throwError(error.error || error.status);
          }
        } else {
          return throwError(error.error || error.status);
        }
      }));
  }

  private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: 
      {
        Authorization: `Bearer ${token}`
      }
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if(!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      this.tokenSubject.next(null);

      return this.accountService.refreshToken().pipe(switchMap((user: User) => {
        if(user) {
          this.tokenSubject.next(user.jwtToken!);
          localStorage.setItem('currentUser', JSON.stringify(user));
          return next.handle(this.addTokenToRequest(request, user.jwtToken!));
        }

        return <any>this.accountService.logout();
      }),
      catchError(error => {
        return <any>this.accountService.logout();
      }),
      finalize(() => {
        this.isRefreshingToken = false;
      })
      );
    } else {
      this.isRefreshingToken = false;

      return this.tokenSubject.pipe(filter(token => token != null), take(1), switchMap(token => {
        return next.handle(this.addTokenToRequest(request, token!));
      }));
    }
  }
  
}
