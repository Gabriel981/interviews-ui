import { HttpClient } from "@angular/common/http";
import { ElementRef, Injectable, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { defaultVars } from "src/environments/environment.prod";
import { User } from "../objects/User";


@Injectable({ providedIn: 'root' })
export class AuthenticationService{
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser!: Observable<User>;
    public obtainedUser!: User;

    @ViewChild('loginModal') loginModal!: ElementRef;


    userType : string = '';
    // userType: BehaviorSubject<string> = new BehaviorSubject<string>(this.getUserType() || '{}');

    constructor(private http: HttpClient, private modalService: NgbModal){
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('jwt') || '{}'));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    getUserType(){
        return this.userType;
    }

    // public get obtainedUserRole(){
    //     console.log(this.obtainedUser.role);
    //     return this.obtainedUser.role;
    // }

    public jwtUserRole() {
        let jwt = localStorage.getItem('currentUser');
        console.log('JWT TOKEN: ' + jwt);

        let jwtData = jwt!.split('.')[1];
        let decodedJwtJsonData = window.atob(jwtData);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);

        console.log('Role: ' + decodedJwtData.name);
    }

    login(emailAddress: string, password: string){
        return this.http.post<any>(`${defaultVars.defaultURI}api/auth/user/login`, {emailAddress, password})
            .pipe(map(user =>{
                localStorage.setItem('jwt', JSON.stringify(user));
                this.currentUserSubject.next(user);
                let element: HTMLElement = document.getElementById('loginDismissBtn') as HTMLElement;
                element.click();
                window.location.reload();
                this.userType = user.emailAddress.value;
                return user;
            }));
    }

    logout(){
        console.log("apeleaza Logout")
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null || {});

        let element : HTMLElement = document.getElementById('logoutDismissBtn') as HTMLElement;
        element.click();
        window.location.reload();
        
        document.getElementById('candidateLoginBtn')!.style.visibility = 'visible';
        document.getElementById('employeeLoginBtn')!.style.visibility = 'visible';
        document.getElementById('logoutBtn')!.style.visibility = 'hidden';
    }

    public resetChangePassword(email: string, token: string, newPassword: string, confirmPassword: string) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.http.post(`${defaultVars.defaultURI}api/auth/reset/password`, {newPassword, confirmPassword, email, token}, requestOptions);
    }
}