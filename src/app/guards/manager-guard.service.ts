import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerGuardService implements CanActivate {

  constructor(private router: Router, private toastr: ToastrService) { }

  public canActivate(route: ActivatedRouteSnapshot){
    let userRole = sessionStorage.getItem('role');
    if(userRole == 'Manager departament resurse umane') {
      return true;
    } 

    this.toastr.error("You're not allowed to access the manager pages");
        setTimeout(() => {
            this.router.navigate(['']);
        }, 1500);

    return false;
  }


}
