import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Injectable( { providedIn: 'root' })
export class AuthGuard implements CanActivate{

    constructor(private router: Router, private toastr: ToastrService){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(localStorage.getItem('currentUser')) {
            return true;
        }

        this.toastr.error("You're not allowed to access this page. Please login in");
        setTimeout(() => {
            this.router.navigate([' '], { queryParams : {returnUrl: state.url }});
        }, 1500);

        return false;
    }

    //varianta complexa - cu eroare
     // var currentUser : any;

        // this.accountService.getCurrentUser().then(response => {
        //     console.log(response);
        //     currentUser = response;
        // });

        // // const currentUser = this.authenticationService.currentUserValue;
        // // console.log(currentUser);
        // // console.log(this.authenticationService.jwtUserRole());
        // if(currentUser){
        //     if(route.data.roles && route.data.roles.indexOf(currentUser.role) === -1){
        //         console.log("ajunge aici")
        //         this.router.navigate([' ']);
        //         return false;
        //     }

        //     // document.getElementById('employeeLoginBtn')!.style.visibility = "hidden";
        //     return true; //logged in
        // }

        // // document.getElementById('employeeLoginBtn')!.style.visibility = "visible";
        // // console.log("aici nu ajunge")
        // this.router.navigate([' '], {queryParams: {returnUrl: state.url }});
        // return false;
}