import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminOptionsComponent } from './admin-options/admin-options.component';
import { ApplicationRegistrationComponent } from './application-registration/application-registration.component';
import { TwoStepVerificationGoogleComponent } from './authentication/two-step-verification-google/two-step-verification-google.component';
import { TwoStepVerificationComponent } from './authentication/two-step-verification/two-step-verification.component';
import { CandidateAdminComponent } from './candidate-admin/candidate-admin.component';
import { CandidateTrackingComponent } from './candidate-tracking/candidate-tracking.component';
import { CandidateComponent } from './candidate/candidate.component';
import { DepartmentComponent } from './department/department.component';
import { EmployeeComponent } from './employee/employee.component';
import { AdminGuardService } from './guards/admin-guard.service';
import { AuthGuard } from './guards/auth-guard.service';
import { ManagerGuardService } from './guards/manager-guard.service';
import { HiringProcessComponent } from './hiring-process/hiring-process.component';
import { HomeComponent } from './home/home.component';
import { InterviewTrackingComponent } from './interview-tracking/interview-tracking.component';
import { InterviewComponent } from './interview/interview.component';
import { LoginComponent } from './login/login.component';
import { ManagerPanelComponent } from './manager-panel/manager-panel.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PositionComponent } from './position/position.component';
import { ProfileComponent } from './profile/profile.component';
import { QuestionComponent } from './question/question.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import { RegisteredSubmissionComponent } from './registered-submission/registered-submission.component';
import { SubmissionComponent } from './submission/submission.component';
import { UserApplicationsComponent } from './user-applications/user-applications.component';
import { UserProfileSettingsComponent } from './user-profile-settings/user-profile-settings.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { UserSubmissionComponent } from './user-submission/user-submission.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {path: 'candidates', component: CandidateComponent},
      {path: 'positions', component: PositionComponent},
      {path: 'submissions', component: SubmissionComponent},
      {path: 'hirings', component: HiringProcessComponent},
      {path: 'interviews', component: InterviewComponent},
      // {path: 'login', component: LoginComponent},
      {path: 'user/options', component: UserSettingsComponent},
      {path: 'user/applications', component: UserApplicationsComponent},
      {path: 'user/applications/list', component: RegisteredSubmissionComponent},
      {path: 'notification/details', component: NotificationDetailsComponent},
      {path: 'user/settings', component: UserProfileSettingsComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'questions', component: QuestionComponent},
      {path: 'departments', component: DepartmentComponent}
    ]
  },
  
  {path: 'admin', component: AdminDashboardComponent, canActivate: [AdminGuardService], runGuardsAndResolvers: 'always'},
  {path: 'admin/employees', component: EmployeeComponent, canActivate: [AdminGuardService], runGuardsAndResolvers: 'always'},
  {path: 'admin/candidates', component: CandidateAdminComponent, canActivate: [AdminGuardService], runGuardsAndResolvers: 'always'},
  {path: 'admin/options', component: AdminOptionsComponent, canActivate: [AdminGuardService], runGuardsAndResolvers: 'always'},
  {path: 'register_user', component: RegisterEmployeeComponent, canActivate: [AdminGuardService], runGuardsAndResolvers: 'always'},
  
  {path: 'manager/panel', component: ManagerPanelComponent, canActivate: [ManagerGuardService], runGuardsAndResolvers: 'always'},
  {path: 'manager/panel/candidate/tracking', component: CandidateTrackingComponent, canActivate: [ManagerGuardService], runGuardsAndResolvers: 'always'},
  {path: 'manager/panel/interview-tracking', component: InterviewTrackingComponent, canActivate: [ManagerGuardService], runGuardsAndResolvers: 'always'},

  {path: 'applications', component: UserSubmissionComponent},
  {path: 'applications/application_registration', component: ApplicationRegistrationComponent},
  {path: 'reset-password/:id/:code', component: PasswordResetComponent},
  {path: 'twostepverification', component: TwoStepVerificationComponent},
  {path: 'twostepverificationgoogle', component: TwoStepVerificationGoogleComponent},
  {path: '**', component: HomeComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
