import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../_services/application.service';
import { AuditService } from '../_services/audit.service';
import { SubmissionService } from '../_services/submission.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  public chartData: any[] = [];
  public employeeChartData: any[] = [];
  public submissionChartData: any[] = [];

  constructor(
    private auditService: AuditService,
    private applicationService: ApplicationService,
    private submissionService: SubmissionService) { }

  ngOnInit(): void {
    this.getChartData();
    this.getEmployeeChartData();
    this.getSubmissionChartData();
  }

  public getChartData() {
    this.auditService.getChartData().then(async response => {
      this.chartData = [];
      this.chartData = response;
    });
  }

  public getEmployeeChartData() {
    this.applicationService.getEmployeeChartData().then(async response => {
      this.employeeChartData = [];
      this.employeeChartData = response;
    });
  }

  public getSubmissionChartData() {
    this.submissionService.getSubmissionsDataChart().then(async response => {
      this.submissionChartData = [];
      this.submissionChartData = response;
    });
  }

}
