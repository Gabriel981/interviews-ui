import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { ToastrService } from 'ngx-toastr';
import { CandidateAdminView } from '../objects/CandidateAdminView';
import { FieldStateMatcher } from '../_helpers/state.matcher.helper';
import { AccountService } from '../_services/account.service';
import { AdminService } from '../_services/admin.service';
import { ApplicationService } from '../_services/application.service';

@Component({
  selector: 'app-candidate-admin',
  templateUrl: './candidate-admin.component.html',
  styleUrls: ['./candidate-admin.component.css']
})
export class CandidateAdminComponent implements OnInit {

  @ViewChild('gridAdminCandidates', {static: false}) gridCandidates!: DxDataGridComponent;

  // devextreme elements
  public availableCandidates: CandidateAdminView[] = [];
  public selectedCandidates: CandidateAdminView[] = [];

  public isUpdateCandidateAvailable: boolean = false;
  public isDeleteCandidateAvailable: boolean = false;
  public isUpdateStatusAvailable: boolean = false;
  public isResetSelectedStatus: boolean = false;

  public crtCandidate!: CandidateAdminView;

  public candidateUpdateForm!: FormGroup;
  public candidateChangeStatusForm!: FormGroup;

  public allMode!: string;
  public checkBoxesMode!: string;

  public isValid: boolean = false;

  //date validators
  public minBirthDate = new Date(1920, 1, 1);
  public maxYear = new Date().getFullYear() - 18;
  public maxBirthDate = new Date(this.maxYear, 1, 1); 

  //state matchers
  public firstNameMatcher = new FieldStateMatcher();
  public lastNameMatcher = new FieldStateMatcher();
  public birthDateMatcher = new FieldStateMatcher();
  public addressMatcher = new FieldStateMatcher();
  public emailMatcher = new FieldStateMatcher();
  public reasonMatcher = new FieldStateMatcher();

  constructor(
    private applicationService: ApplicationService, 
    private toastr: ToastrService,
    private adminService: AdminService,
    private accountService: AccountService) { 

      this.allMode = 'allPages';
      this.checkBoxesMode = 'onClick';
  }

  ngOnInit() {
    this.getAvailableCandidates();
  }

  public getAvailableCandidates() {
    this.applicationService.getAllAvailableCandidates().then(async response => {
      this.availableCandidates = [];
      this.availableCandidates = response;
    });
  }

  //devextreme popups zone
  //devextreme - open popup zones
  public openUpdatePopup = (event): void => {
    this.isUpdateCandidateAvailable = !this.isUpdateCandidateAvailable;
    this.crtCandidate = this.availableCandidates[event.row.rowIndex];

    let firstName = this.crtCandidate.firstName;
    let lastName = this.crtCandidate.lastName;
    let address = this.crtCandidate.address;
    let birthDate = this.crtCandidate.dateOfBirth;
    let appliedOn = this.crtCandidate.submissionDate;
    let emailAddress = this.crtCandidate.email;
    let appliedPosition = this.crtCandidate.positionName;

    this.candidateUpdateForm = new FormGroup({
      firstName: new FormControl(firstName, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      lastName: new FormControl(lastName, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      address: new FormControl(address, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
      ]),
      birthDate: new FormControl(birthDate, [
        Validators.required,
      ]),
      appliedOn: new FormControl(appliedOn),
      emailAddress: new FormControl(emailAddress,[
        Validators.required,
        Validators.email,
        Validators.minLength(8),
        Validators.maxLength(50)
      ]),
      appliedPosition: new FormControl(appliedPosition)
    });
  }

  public openCandidateRemovalPopup = (event): void => {
    this.isDeleteCandidateAvailable = !this.isDeleteCandidateAvailable;
    this.crtCandidate = this.availableCandidates[event.row.rowIndex];
  }

  public openCandidateStatusPopup = (event): void => {
    this.isUpdateStatusAvailable = !this.isUpdateStatusAvailable;
    this.crtCandidate = this.availableCandidates[event.row.rowIndex];

    this.candidateChangeStatusForm = new FormGroup({
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public openMultipleResetPopup() {
    this.selectedCandidates = this.gridCandidates.instance.getSelectedRowsData();

    if(this.selectedCandidates.length == 0) {
      this.toastr.warning("You have not selected any candidates from table");
    }

    if(this.selectedCandidates.length > 0 && this.selectedCandidates.length < 2)
    {
      this.toastr.warning("Select at least two candidates to execute this operation");
    }

    if(this.selectedCandidates.length >= 2) {
      this.isResetSelectedStatus = !this.isResetSelectedStatus;
    }
  }

  //devextreme - close popup zones
  public closeUpdatePopup() {
    this.isUpdateCandidateAvailable = false;
  }

  public closeCandidateRemovalPopup() {
    this.isDeleteCandidateAvailable = false;
  }

  public closeUpdateStatusPopup() {
    this.isUpdateStatusAvailable = false;
  }

  public closeMultipleResetPopup() {
    this.isResetSelectedStatus = !this.isResetSelectedStatus;
  }

  //devextreme other methods
  public updateCandidateDetails() {    
    let pipe = new DatePipe('en-US');
    let birthDateFormatted = pipe.transform(this.candidateUpdateForm.value.birthDate, 'dd/MM/yyyy');

    var candidateDetails = {
      firstName: this.candidateUpdateForm.value.firstName,
      lastName: this.candidateUpdateForm.value.lastName,
      dateOfBirth: birthDateFormatted,
      address: this.candidateUpdateForm.value.address,
      email: this.candidateUpdateForm.value.emailAddress
    };

    this.adminService.UpdateCandidateDetailsByAdmin(candidateDetails).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeUpdatePopup();
    }, error => {
      console.log(`Error ${error}`);
      this.toastr.error(`Error ${error}`);
      this.closeUpdatePopup();
    });
  }

  public removeCandidate() {
    let crtUser = this.accountService.currentUserValue;

    var completeDetailsToBeSent = {
      candidateFirstName: this.crtCandidate.firstName,
      candidateLastNmae: this.crtCandidate.lastName,
      candidateEmail: this.crtCandidate.email,
      adminId: crtUser.id,
      adminFirstName: crtUser.firstName,
      adminLastName: crtUser.lastName,
      adminEmail: crtUser.email,
      positionName: crtUser.position?.positionName
    };

    this.applicationService.proposeRemovalOfCandidate(completeDetailsToBeSent).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeCandidateRemovalPopup();
    }, error => {
      console.log(`Error ${error}`);
      this.toastr.error(`Error: ${error}`);
      this.closeCandidateRemovalPopup();
    });
  }

  public updateStatus() {
      if(this.candidateChangeStatusForm.valid) {
        let crtUser = this.accountService.currentUserValue;
        
        var candidateResetDetails = {
          senderId: crtUser.id,
          email: this.crtCandidate.email,
          reason: this.candidateChangeStatusForm.value.reason
        };

        this.adminService.UpdateCandidateSituation(candidateResetDetails).then(async response => {
          this.ngOnInit();
          this.toastr.success(`Server response: ${response}`);
          this.closeUpdateStatusPopup();
        }, error => {
          console.log(`Error ${error}`);
          this.toastr.error(`Error ${error}`);
          this.closeUpdateStatusPopup();
        });
      }
  }

  public resetMultipleStatus() {
    this.selectedCandidates = this.gridCandidates.instance.getSelectedRowsData();

    if(this.selectedCandidates.length >= 2) {
      let serverResponse = '';

      let count = 0;

      this.selectedCandidates.forEach(selectedCandidate => {
        let crtUser = this.accountService.currentUserValue;
        var crtCandidateDetails = {
          senderId: crtUser.id,
          email: selectedCandidate.email,
          reason: 'All resets test'
        };
  
       this.adminService.UpdateCandidateSituation(crtCandidateDetails).then(async response => {
          serverResponse = response;
          this.ngOnInit();
          this.isValid = true;
          console.log(this.isValid);
          count++;
        }, error => {
          console.log(`Error ${error}`);
          serverResponse = error;
          this.isValid = false;
          console.log(`error ` + this.isValid);
          count++;
        });
      });

      if(count == this.selectedCandidates.length) {
        console.log(`line 270 ` + this.isValid);
        if(this.isValid == true) {
          this.toastr.success(`Server response ${serverResponse}`);
          this.closeMultipleResetPopup();
        } else {
          this.toastr.error(`Error ${serverResponse}`);
          this.closeMultipleResetPopup();
        }
      }
    }
  }
}
