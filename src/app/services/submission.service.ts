import { HttpClient } from "@angular/common/http";
import { defaultVars } from "src/environments/environment.prod";
import { AuthenticationService } from "../guards/AuthenticationService";
import { Candidate } from "../objects/Candidate";
import { Position } from "../objects/Position";
import { Submission } from "../objects/Submission";

export class SubmissionService{

    constructor(
        private httpClient: HttpClient,
        private authenticationService: AuthenticationService
    ){ }

    public getAllSubmissions(){
        return this.httpClient.get<Submission[]>(defaultVars.defaultURI + 'submissions/').toPromise()
            .then((response: Submission[]) => {
                let submissionList: Submission[] = [];

                response.forEach(submission => {
                    submissionList.push(submission);
                });

            return submissionList;
            });
    }

    public getAllCandidates(){
        return this.httpClient.get<Candidate[]>(defaultVars.defaultURI + 'candidates/').toPromise();
    }

    public getAllPositions(){
        return this.httpClient.get<Position[]>(defaultVars.defaultURI + 'positions/').toPromise()
            .then((response:Position[]) => {
                let positionList: Position[] = [];

                response.forEach(position => {
                    if(position.availableForRecruting == true && position.noVacantPositions! > 0){
                        positionList.push(position);
                    }
                });

                return positionList;
            });
    }

    public uploadSubmission(values: any){
        return this.httpClient.post<Submission>(`${defaultVars.defaultURI}submissions/upload`, values).toPromise();
    }

    public updateSubmissionDetails(values: any, updateId: number){
        return this.httpClient.put<Submission>(defaultVars.defaultURI + 'submissions/' + updateId, values).toPromise();
    }

    public removeSubmission(deleteId: number){
        return this.httpClient.delete<Submission>(defaultVars.defaultURI + 'submissions/' + deleteId).toPromise();
    }

    public getUserSubmissions(userId: any){
        return this.httpClient.get<Submission[]>(`${defaultVars.defaultURI}api/candidate/available/submissions/` + userId).toPromise()
            .then((response: Submission[]) => {
                let submissionList: Submission[] = [];

                response.forEach(submission => {
                    submissionList.push(submission);
                });

                return submissionList;
            });
    }

    public getCurrentSubmissionCV(submissionId: number){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}submissions/cv/` + submissionId).toPromise();
    }

    public exportSubmissionData(){
        const requiredOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${defaultVars.defaultURI}submissions/export`, requiredOptions).toPromise();
    }
}