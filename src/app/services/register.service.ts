import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BehaviorSubject, Observable } from "rxjs";
import { defaultVars } from "src/environments/environment.prod";
import { User } from "../objects/User";

export class RegisterService {

    private currentUserSubject!: BehaviorSubject<User>;
    public currentUser!: Observable<User>;
    private invalidLogin!: boolean;

    constructor(private httpClient: HttpClient, private router: Router, private modalService: NgbModal){ }

    public EmployeeRegister(employeeData: any){
        return this.httpClient.post<any>(defaultVars.defaultURI + 'api/auth/user/register', employeeData).toPromise();
    }

    public EmployeeLogin(loginData: any) {
        return this.httpClient.post(defaultVars.defaultURI + 'api/auth/user/login', loginData)
            .subscribe(response => {
                localStorage.setItem('jwt', JSON.stringify(response));
                this.currentUserSubject.next(response);
                this.modalService.dismissAll();
                return response;
            }, err =>{
                this.invalidLogin = true;
            });
    }

    public CandidateRegister(candidateData: any){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/auth/user/register/candidates`, candidateData).toPromise();
    }
}