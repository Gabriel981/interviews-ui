import { HttpClient, HttpHeaderResponse, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { defaultVars } from "src/environments/environment";
import { AuthenticationService } from "../guards/AuthenticationService";
import { HiringProcess } from "../objects/HiringProcess";
import { Interview } from "../objects/Interview";



export class InterviewService {

    constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService){ }

    public getStoredInterviews()
    {
        return this.httpClient.get<Interview[]>(`${defaultVars.defaultURI}api/interviews`).toPromise()
            .then((response : Interview[]) => {
                let interviewList: Interview[] = [];

                response.forEach(interview => {
                    interviewList.push(interview);
                });

                return interviewList;
            });
    }

    public getStoreHiringProcesses(){
        return this.httpClient.get<HiringProcess[]>(`${defaultVars.defaultURI}api/interviews/hirings`).toPromise()
            .then((response: HiringProcess[]) => {
                let hiringsList: HiringProcess[] = [];

                response.forEach(hiringProcess => {
                    hiringsList.push(hiringProcess);
                });

                return hiringsList;
            });
    }

    public scheduleNewInterview(data: any){
        return this.httpClient.post<Interview>(`${defaultVars.defaultURI}api/interviews/schedule`, data).toPromise();
    }

    public removeScheduledInterview(deleteId: any){
        return this.httpClient.delete<Interview>(`${defaultVars.defaultURI}api/interviews/` + deleteId).toPromise();
    }

    public updateInterviewDetails(updateId, interview: Interview){
        return this.httpClient.put<Interview>(`${defaultVars.defaultURI}api/interviews/update/` + updateId, interview).toPromise();
    }

    public allocateParticipantInterview(interviewParticipantData: any, senderId: string){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/interviews/addParticipant/` + senderId, interviewParticipantData).toPromise();
    }

    public getAllAvailableEmployees(){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/interviews/employees`).toPromise()
            .then((response: any) => {
                let employeesList : any[] = [];
    
                response.forEach(employee => {
                    employeesList.push(employee);
                });

                return employeesList;
            });
    }

    public getAllAvailableParticipantsByInterview(interviewId: number){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/interviews/participants/` + interviewId).toPromise()
            .then((response: any[]) => {
                let participantsList : any[] = [];

                response.forEach(participant => {
                    participantsList.push(participant);
                });

                return participantsList;
            });
    }

    public removeParticipantFromInterview(interviewId: number, participantId: string){
        return this.httpClient.delete<any>(`${defaultVars.defaultURI}api/interviews/participants/remove/` + interviewId + "/" + participantId).toPromise();
    }

    public exportAllAvailableInterviews(){
        const requiredOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/interviews/export`, requiredOptions).toPromise();
    }
}