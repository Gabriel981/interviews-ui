import { HttpClient } from "@angular/common/http";
import { defaultVars } from "src/environments/environment.prod";
import { HiringProcess } from "../objects/HiringProcess";

export class HiringServices {

    constructor(
        private httpClient: HttpClient
    ){ }

    public getAllHiringProcesses(){
        return this.httpClient.get<HiringProcess[]>(defaultVars.defaultURI + 'hirings/').toPromise()
            .then((response: HiringProcess[]) => {
                let hiringProcessesList: HiringProcess[] = [];

                response.forEach(hiring => {
                    hiringProcessesList.push(new HiringProcess(hiring));
                })

                return hiringProcessesList;
            });
    }
}