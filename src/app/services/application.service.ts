import { HttpClient } from "@angular/common/http";
import { defaultVars } from "src/environments/environment.prod";
import { AuthenticationService } from "../guards/AuthenticationService";
import { CandidateAdminView } from "../objects/CandidateAdminView";
import { Position } from "../objects/Position";
import { User } from "../objects/User";

export class ApplicationService {

    constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService){ }

    public getAllAvailablePositions() {
        return this.httpClient.get<Position[]>(`${defaultVars.defaultURI}api/candidate/available/positions`)
            .toPromise().then((response: Position[]) => {
                let positionList: Position[] = [];

                response.forEach(position => {
                    positionList.push(position);
                });

                return positionList;
            });
    }

    public getAllAvailableEmployees(){
        return this.httpClient.get<User[]>(`${defaultVars.defaultURI}api/admin/employees`)
            .toPromise().then((response) => {
                let employeeList: User[] = [];

                response.forEach(employee => {
                    employeeList.push(employee);
                });

                return employeeList;
            });
    }
    
    public updateAvailableEmployee(data: any, updateId: string){
        return this.httpClient.put<User>(`${defaultVars.defaultURI}api/admin/` + updateId, data).toPromise();
    }

    public removeAvailableEmployee(employeeRemoveId: string){
        return this.httpClient.delete<User>(`${defaultVars.defaultURI}api/admin/` + employeeRemoveId).toPromise();
    }

    public updateAvailableCandidate(data: any, updateId: string){
        return this.httpClient.put<User>(`${defaultVars.defaultURI}api/user/update/` + updateId, data).toPromise();
    }

    public getAllAvailableCandidates(){
        return this.httpClient.get<CandidateAdminView[]>(`${defaultVars.defaultURI}api/admin/candidates`).toPromise()
            .then((response: CandidateAdminView[]) => {
                let candidatesList: CandidateAdminView[] = [];

                response.forEach(candidate => {
                    candidatesList.push(candidate);
                });

                return candidatesList;
            });
    }

    public proposeRemovalOfCandidate(candidateData: any){
        return this.httpClient.post(`${defaultVars.defaultURI}api/admin/removal_proposal`, candidateData).toPromise();
    }
}