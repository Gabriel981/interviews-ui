import { HttpClient, HttpHeaders } from "@angular/common/http";
import { defaultVars } from "src/environments/environment.prod";
import { AuthenticationService } from "../guards/AuthenticationService";
import { Proposal } from "../objects/Proposal";
import { Submission } from "../objects/Submission";

export class ManagerService {

    constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService){ }

    public GetNotificationDetails(notificationId: any, currentUserId: string){
        return this.httpClient.get<Proposal>(`${defaultVars.defaultURI}api/notifications/notificationDetails/` + notificationId + "/" + currentUserId).toPromise();
    }

    public GetAllSubmissionsData(){
        return this.httpClient.get<Submission[]>(`${defaultVars.defaultURI}api/manager/submissions`).toPromise()
            .then((response: Submission[]) => {
                let submissionList: Submission[] = [];

                response.forEach(submission => {
                    submissionList.push(submission);
                });

                return submissionList;
            });
    }

    public ApproveCandidate(submissionId: number, managerId: string){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/approve/` + submissionId + '/' + managerId, null).toPromise();
    }

    public RejectCandidate(submissionId: number, managerId: string){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/reject/` + submissionId + "/" + managerId, null).toPromise();
    }

    public RequestStatusChange(submissionId: number, managerId: string){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/requestChange/` + submissionId + "/" + managerId, null).toPromise();
    }

    public RequestCandidateRemoval(submissionId: number, managerId: string){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/requestRemoval/` + submissionId + "/" + managerId, null).toPromise();
    }

    public GetAllAvailableCandidates(){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/availableCandidates`).toPromise()
            .then((response: any) => {
                let candidatesList: any[] = [];

                response.forEach(candidate => {
                    candidatesList.push(candidate);
                });

                return candidatesList;
            });
    }

    public GetAllAvailableInterviews(){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/availableInterviews`).toPromise()
            .then((response: any) => {
                let availableInterviewsList : any[] = [];

                response.forEach(interview => {
                    availableInterviewsList.push(interview);
                });

                return availableInterviewsList;
            });
    }

    public RequestInterviewChangeDetails(interviewDetails: any){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/requestInterviewChange`, interviewDetails).toPromise();
    }

    public RequestInterviewRemovalRecruiter(interviewDetails: any){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/requestInterviewRemoval`, interviewDetails).toPromise();
    }

    public RequestSpecialInterviewRemoval(interviewDetails: any){
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/manager/requestSpecialInterviewRemoval`, interviewDetails).toPromise();
    }

    public GetAllParticipantsByInterviewId(interviewId: any){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/participants/interview/` + interviewId).toPromise()
            .then((response: any[]) => {
                let participantsList: any[] = [];

                response.forEach(participant => {
                    participantsList.push(participant);
                });

                return participantsList;
            });
    }

    public GetSelectedCandidateCV(selectedCandidateId: string){
        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/cv/` + selectedCandidateId).toPromise();
    }

    public ExportDataExcel(){
        const requestOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/export/submissions`, requestOptions).toPromise();
    }


    public ExporInterviewsDataExcel(){
        const currentUser = this.authenticationService.currentUserValue;
        const requestOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${defaultVars.defaultURI}api/manager/export/interviews`, requestOptions).toPromise();
    }
}