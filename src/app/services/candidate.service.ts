import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { defaultVars } from "src/environments/environment";
import { Candidate } from "../objects/Candidate";

export class CandidateService {

    constructor(
        private httpClient: HttpClient
    ){ }

    public getAllCandidates(){
        return this.httpClient.get<Candidate[]>(defaultVars.defaultURI + 'candidates/').toPromise();
    }

    public submitCandidateDetails(values: any){
        return this.httpClient.post<Candidate>(defaultVars.defaultURI + 'candidates/', values).toPromise();
    }

    public removeCandidate(deleteId: number){
        return this.httpClient.delete<Candidate>(defaultVars.defaultURI + 'candidates/' + deleteId).toPromise(); 
    }
}