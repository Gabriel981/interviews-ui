import { HttpClient } from "@angular/common/http";
import { defaultVars } from "src/environments/environment";
import { AuthenticationService } from "../guards/AuthenticationService";
import { User } from "../objects/User";

export class AuthenticationUserService {
    
    constructor(private httpClient: HttpClient){ }

    public ObtainCurrentUser(){
        return this.httpClient.get<User>(`${defaultVars.defaultURI}api/auth/currentUser`).toPromise();
    }

    public removeCurrentAccount(model: any) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/auth/scheduleDelete`, model, requestOptions);
    }
}