import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { defaultVars } from "src/environments/environment.prod";
import { AuthenticationService } from "../guards/AuthenticationService";
import { Position } from "../objects/Position";

@Injectable({ providedIn: 'root'})
export class PositionServices {

    private defaultURI = defaultVars.defaultURI;

    constructor (
        private httpClient: HttpClient,
    ){ }

    updateVacantPositions(position: Position, positionId: number){
        const requestOptions : Object = {
            responseType: 'text'
        };

        return this.httpClient.put<Position>(`${this.defaultURI}api/positions/vacant/` + positionId, position, requestOptions)
            .toPromise();        
    }

    getCurrentPositions(){
        return this.httpClient.get<Position[]>(`${this.defaultURI}api/positions`).toPromise()
            .then((response: Position[]) => {
                let positionList: Position[] = [];

                response.forEach(position => {
                    positionList.push(position);
                })

                return positionList;
            });
    }

    updatePositionDetails(position: Position, positionId: number){
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.put<Position>(`${this.defaultURI}api/positions/` + positionId, position, requestOptions)
            .toPromise();
    }

    removePosition(positionId: number){
        return this.httpClient.delete<Position>(`${this.defaultURI}api/positions/` + positionId)
            .toPromise();
    }

    createNewPosition(position: Position, headers: HttpHeaders){
        return this.httpClient.post<Position>(`${this.defaultURI}api/positions`, position, {'headers': headers})
            .toPromise();
    }
}