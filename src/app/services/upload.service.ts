import { HttpClient, HttpEvent, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { defaultVars } from "src/environments/environment.prod";
import { AuthenticationService } from "../guards/AuthenticationService";

@Injectable({ providedIn : 'root' })
export class UploadService{

    private baseApiUrl!: string;
    private apiUploadUrl!: string;
    private apiFileUrl!: string;

    constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService){
        this.baseApiUrl = defaultVars.defaultURI + "api/candidate/available/";
        this.apiUploadUrl = this.baseApiUrl + "upload";
     }

    public uploadFile(file: Blob, email: string): Observable<HttpEvent<void>> {
        let customHeaders2 = new HttpHeaders();
        customHeaders2.set('Content-Type', 'multipart/form-data');

        const formData = new FormData();
        formData.append('file', file);
        formData.append('email', email);

        return this.httpClient.post<any>(this.apiUploadUrl, formData, {headers: customHeaders2});
    }

    public uploadProfilePhoto(file: Blob, email: string): Observable<HttpEvent<void>> {
        let customHeaders = new HttpHeaders();
        customHeaders.append('Content-Type', 'multipart/form-data');
        
        const formData = new FormData();
        formData.append('file', file);
        formData.append('email', email);

        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/user/upload`, formData, {headers: customHeaders});
    }





}