import { HttpClient, HttpHeaders } from "@angular/common/http";
import { defaultVars } from "src/environments/environment";
import { AuthenticationService } from "../guards/AuthenticationService";
import { Notification } from "../objects/Notification";
import { Proposal } from "../objects/Proposal";

export class NotificationService {

    constructor(private httpClient:HttpClient, private authenticationService: AuthenticationService){}

    public sendNotification(notification: any){
        return this.httpClient.post<Notification>(`${defaultVars.defaultURI}api/notifications/send`, notification).toPromise();
    }

    public getNotifications(receiverId: string){
        return this.httpClient.get<Notification[]>(`${defaultVars.defaultURI}api/notifications/show/` + receiverId).toPromise()
            .then((response: Notification[]) => {
                let notificationList:Notification[] = [];
                
                response.forEach(notification => {
                    notificationList.push(notification);
                });

                return notificationList;
            });
    }

    public removeNotification(notificationView: any){
        return this.httpClient.post<Notification>(`${defaultVars.defaultURI}api/notifications/remove`, notificationView).toPromise();
    }

    public GetNotificationDetails(notificationId: string, senderId: string){
        return this.httpClient.get<Proposal>(`${defaultVars.defaultURI}api/notifications/notificationDetails/` + notificationId + "/" + senderId).toPromise();
    }
    
    public ChangeNotificationStatus(userId: string, value: boolean) {
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/user/notifications/` + userId + "/" + value, null).toPromise();
    }
}