import { HttpClient, HttpHeaders } from "@angular/common/http";
import { defaultVars } from "src/environments/environment";
import { AuthenticationService } from "../guards/AuthenticationService";

export class AdminService {

    constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) { }

    public UpdateCandidateDetailsByAdmin(candidateDetails: any) {
        return this.httpClient.put<any>(`${defaultVars.defaultURI}api/admin/candidate/update`, candidateDetails).toPromise();
    }

    public UpdateCandidateSituation(candidateResetDetails: any) {
        return this.httpClient.post<any>(`${defaultVars.defaultURI}api/admin/candidate/status`, candidateResetDetails).toPromise();
    } 
}