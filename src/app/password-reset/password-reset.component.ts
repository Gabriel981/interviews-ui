import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';
import { ApplicationService } from '../_services/application.service';

export class CustomErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null) : boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

  private userId!: string;
  private token!: string;
  private crtEmail!: string;

  matcher = new CustomErrorStateMatcher();

  //form items
  public passwordResetFormGroup!: FormGroup;

  constructor(
    private route: ActivatedRoute, 
    private applicationService: ApplicationService, 
    private toastr: ToastrService,
    private router: Router,
    private accountService: AccountService) { 

      this.passwordResetFormGroup = new FormGroup({
        email: new FormControl('', [Validators.required]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(25)
        ]),
        confirmPassword: new FormControl('', [
          Validators.required,
          PasswordResetComponent.matchPasswords('password')
        ])
      });
    }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.userId = params.get("id")!;
      this.token = params.get('code')!;
    });

    this.getEmail();
    
    console.log('ajunge aici la passwordResetFormGroup.controls');
  }

  public static matchPasswords(matchTo: string) : (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent && !!control.parent.value && control.value === control.parent.controls[matchTo].value ? null : { isMatching: false };
    };
  }

  public resetPassword() {
    let values = {
      email: this.passwordResetFormGroup.value.email,
      token: this.token,
      newPassword: this.passwordResetFormGroup.value.password
    };

    this.applicationService.changePassword(values).then(async response => {
      this.toastr.success(`Server response: ${response}`);
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 3000);
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public getEmail() {
    this.accountService.getEmail(this.userId).then(async response =>{
      console.log(response);
      this.crtEmail = response;
      this.passwordResetFormGroup.controls["email"].setValue(this.crtEmail);
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

}
