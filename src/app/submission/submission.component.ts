import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Candidate } from '../objects/Candidate';
import { Position } from '../objects/Position';
import { Submission } from '../objects/Submission';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { SubmissionService } from '../_services/submission.service';

@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})


export class SubmissionComponent implements OnInit {

  submissions: Submission[] = [];
  closeResult!: string;
  positions!: Position[];
  candidates!: Candidate[];

  // New table variables
  public dataSource = new MatTableDataSource<Submission>();
  public displayedColumns: string[] = ['submissionDate', 'candidateName', 'mentions', 'moreDetails', 'interviewSchedule', 'removeObject'];
  public filterColumns: string[] = ['submissionDateFilter', 'candidateNameFilter'];
  public timer = 1000;
  public timeoutVal = 1000;
  public fromDate!: any;
  public toDate!: any;

  public isLinear = true;

  private selectedSubmissionId!: number;

  private selectedSubmissionCVlink!: string;

  public submissionDetails!: FormGroup;
  public fastInterviewCreatorFG!: FormGroup;
  public submissionDetailsFG!: FormGroup;
  public candidateDetailsFG!: FormGroup;

  private deleteId!: number;
  private currentSubmissionId!: number;

  // paginator elements
  public pageLength: number = 0;
  public splicedData!: any[];
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;


  //DxPopups elements - Submissions
  public isUpdateSubmissionAvailable: boolean  = false;
  public isDeleteSubmissionAvailable: boolean = false;
  public submissionUpdateForm!: FormGroup;
  public crtSubmission!: Submission;


  //DxPopups elements - Candidates
  public isRemovalProposalAvailable: boolean = false;
  public candidateUpdateForm!: FormGroup;
  public crtCandidate!: Candidate;
  public crtUser!: User;

  constructor(
    private toastr: ToastrService,
    private modalService: NgbModal,
    private submissionService: SubmissionService,
    private accountService: AccountService
  ) {
    this.submissionDetails = new FormGroup({
      position: new FormControl('', [
        Validators.required
      ]),
      candidate: new FormControl('', [
        Validators.required
      ]),
      mentions: new FormControl('', [
      ])
    });
  }

  ngOnInit(): void {
    this.getAvailableSubmissions();
    this.getAvailableCandidates();
    this.getAvailablePositions();
  }

  public pageChangeEvent(event) {
    const offset = ((event.pageIndex + 1) - 1) * event.pageSize;
    this.splicedData = this.submissions.slice(offset).slice(0, event.pageSize);
  }

  public get position() {
    return this.submissionDetails.get('position');
  }

  public get candidate() {
    return this.submissionDetails.get('candidate');
  }

  public get mentions() {
    return this.submissionDetails.get('mentions');
  }

  // Submissions filter
  public applySubmissionDateFilter(dataRangeStart: HTMLInputElement, dataRangeEnd: HTMLInputElement) {
    let dataRangeStartValue = dataRangeStart.value.toString();
    let dataRangeEndValue = dataRangeEnd.value.toString();

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        startDate: dataRangeStartValue,
        lastDate: dataRangeEndValue
      };

      this.getAvailableSubmissionsByDate(elements);
    }, this.timeoutVal);
  }
  
  public getAvailableSubmissionsByDate(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAvailableSubmissions();
      } else {
        this.submissionService.customSearchByDateFilter(key).then((response) =>{
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public resetForm(){
    this.fromDate = undefined;
    this.toDate = undefined;

    this.getAvailableSubmissions();
  }

  public applyCandidateNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSubmissionsByCandidateFilter(elements);
    }, this.timeoutVal);
  }

  public getSubmissionsByCandidateFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAvailableSubmissions();
      } else {
        this.submissionService.customSearchByCandidate(key).then((response) =>{
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  //Populate main page with available submissions
  public getAvailableSubmissions() {
    this.submissionService.getAllSubmissions().then((response) => {
      let retrievedSubmissions = response;

      this.submissions = [];

      retrievedSubmissions.forEach(submission => {
        this.submissions.push(submission);
      });

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  //All data for upload submission - retrieval
  public getAvailableCandidates() {
    this.submissionService.getAllCandidates().then(async response => {
      this.candidates = response;
    });
  }

  public getAvailablePositions() {
    this.submissionService.getAllPositions().then((response) => {
      this.positions = response;
    });
  }

  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if(!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch(sort.active) {
        case 'candidateName': return this.compare(a.user.firstName!, b.user.firstName!, isAsc);
        case 'submissionDate': return this.compare(a.dateOfSubmission.toString(), b.dateOfSubmission.toString(), isAsc);
        default: return 0;
      }
    });
  }

  public compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  //Methods for opening of submission modal

  public open(content: any) {
    // if(this.positions.length != 0){
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {
        this.closeResult = `Closed with: $(result)`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    // }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public onSubmit() {
    const submissionData = {
      candidateId: this.submissionDetails.value.candidate.candidateID,
      positionId: this.submissionDetails.value.position.positionId,
      mentions: this.submissionDetails.value.mentions
    };

    this.submissionService.uploadSubmission(submissionData).then((response) => {
      console.log(response);
      this.ngOnInit();
      this.modalService.dismissAll();
    });
  }

  public openDetails(targetModal: any, submission: Submission) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.selectedSubmissionId = submission.submissionId!;

    let pipe = new DatePipe('en-US');
    let formattedDate = pipe.transform(submission.dateOfSubmission, 'dd/MM/yyyy');

    //first formgroup
    let candidateCompleteName = submission.user?.firstName + " " + submission.user?.lastName;
    let appliedPositionName = submission.appliedPosition.positionName;
    let submissionDate = formattedDate?.toString();
    let submissionMentions = '';

    //second formgroup
    let birthDateFormatted = pipe.transform(submission.user?.dateOfBirth);
    let address = submission.user?.address;
    let emailAddress = submission.user?.email;
    let phone = submission.user?.phoneNumber;

    if (submission.mentions == null || submission.mentions == '') {
      submissionMentions = "No mentions was submitted";
    } else {
      submissionMentions = submission.mentions;
    }

    this.submissionDetailsFG = new FormGroup({
      candidateNameDetails: new FormControl(candidateCompleteName),
      appliedPositionDetails: new FormControl(appliedPositionName),
      dateOfSubmissionDetails: new FormControl(submissionDate),
      mentionsDetails: new FormControl(submissionMentions)
    });

    this.candidateDetailsFG = new FormGroup({
      candidateCompleteNameDetails: new FormControl(candidateCompleteName),
      candidateBirthDateDetails: new FormControl(birthDateFormatted),
      candidateAddressDetails: new FormControl(address),
      candidateEmailDetails: new FormControl(emailAddress),
      candidatePhoneDetails: new FormControl(phone)
    });
  }

  public openDelete(targetModal: any, submission: Submission) {
    this.deleteId = submission.submissionId!;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  public onDelete() {
    this.submissionService.removeSubmission(this.crtSubmission.submissionId!).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeDeleteSubmissionPopup();
    }, error => {
      console.log(`Error: {error.error}`);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public openCV() {
    this.submissionService.getCurrentSubmissionCV(this.selectedSubmissionId).then((response) => {
      if (response.cv != null) {
        console.log(response);
        this.selectedSubmissionCVlink = response.cv;
        window.open(this.selectedSubmissionCVlink, "_blank");
      }
      else {
        this.showNoCVAvailableToast();
      }
    })
  }

  public exportData() {
    this.submissionService.exportSubmissionData().then((response) => {
      this.downloadExcelFile(response);
      this.showSuccessExportDataSubmissions();
    });
  }

  public downloadExcelFile(data: any) {
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, "dd/MM/yyyy HH:mm:ss", "en-US");

    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Submissions ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  public openFastInterviewWindow(targetModal: any, submission: Submission) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.currentSubmissionId = submission.submissionId!;

    let submissionDate = submission.dateOfSubmission;

    let submissionDateNotFormatted = new DatePipe('en-US');
    let submissionDateFormatted = submissionDateNotFormatted.transform(submissionDate, "dd/MM/yyyy HH:mm:ss");


    let positionName = submission.appliedPosition.positionName;
    let candidateCompleteName = submission.user?.firstName + " " + submission.user?.lastName;

    this.fastInterviewCreatorFG = new FormGroup({
      fastInterviewSubmissionDate: new FormControl(''),
      fastInterviewPositionName: new FormControl(''),
      fastInterviewCompleteName: new FormControl(''),
      fastInterviewMentions: new FormControl('', [
        Validators.minLength(10),
        Validators.maxLength(150)
      ])
    });

    this.fastInterviewCreatorFG.controls['fastInterviewSubmissionDate'].setValue(submissionDateFormatted);
    this.fastInterviewCreatorFG.controls['fastInterviewPositionName'].setValue(positionName);
    this.fastInterviewCreatorFG.controls['fastInterviewCompleteName'].setValue(candidateCompleteName);
  }

  public scheduleFastInterview() {
    let currentUserId: string;

    let user = this.accountService.currentUserValue;
    
    var fastInterviewData = {
      submissionId: this.currentSubmissionId,
      createdById: user.id!
    };

    this.submissionService.scheduleFastInterview(fastInterviewData).then((response) => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });


    // this.accountService.getCurrentUser().then((response) => {
    //   var fastInterviewData = {
    //     submissionId: this.currentSubmissionId,
    //     createdById: response.id!
    //   };

    //   this.submissionService.scheduleFastInterview(fastInterviewData).then((response) => {
    //     this.ngOnInit();
    //     this.toastr.success(`Server response: ${response}`);
    //     this.modalService.dismissAll();
    //   }, error => {
    //     console.log(error.error);
    //     this.toastr.error(`Error: ${error.error}`);
    //   });
    // }, error => {
    //   console.log(error.error);
    //   this.toastr.error(`Error: ${error.error}`);
    // });


  }

  public showNoCVAvailableToast() {
    this.toastr.warning("No CV available for this submission");
  }

  public showSuccessExportDataSubmissions() {
    this.toastr.success("The submissions data was successfully exported. Check your download tab.");
  }

  //DxPopups functions

  //Open popups functions - Submissions
  public openSubmissionUpdatePopup = (event): void =>  {
    this.isUpdateSubmissionAvailable = !this.isUpdateSubmissionAvailable;
    
    this.crtSubmission = this.submissions[event.row.rowIndex];

    var positionName = this.crtSubmission.appliedPosition.positionName;
    var candidateName = this.crtSubmission.user?.firstName + " " + this.crtSubmission.user?.lastName;
    
    let pipe = new DatePipe('en-US');
    let formattedSubmissionDate = pipe.transform(this.crtSubmission.dateOfSubmission, 'dd/MM/yyyy');
    
    var mentions = this.crtSubmission.mentions;

    this.submissionUpdateForm = new FormGroup({
      positionName: new FormControl(positionName, [
        Validators.required
      ]),
      candidateName: new FormControl(candidateName, [
        Validators.required
      ]),
      submissionDate: new FormControl(formattedSubmissionDate, [
        Validators.required
      ]),
      mentions: new FormControl(mentions, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public openDeleteSubmissionPopup = (event) : void => {
    this.isDeleteSubmissionAvailable = !this.isDeleteSubmissionAvailable;
    this.crtSubmission = this.submissions[event.row.rowIndex];
  }

  //Open popups functions - Candidates
  public openRemovalProposal = (event): void => {
    this.crtCandidate = this.candidates[event.row.rowIndex];
    this.isRemovalProposalAvailable = !this.isRemovalProposalAvailable;
  }

  //Close popups functions - Submissions
  public closeUpdateSubmissionPopup() {
    this.isUpdateSubmissionAvailable = !this.isUpdateSubmissionAvailable;
  }

  public closeDeleteSubmissionPopup() {
    this.isDeleteSubmissionAvailable = !this.isDeleteSubmissionAvailable;
  }


  //Close popups functions - Candidates
  public closeCandidateRemovalPopup() {
    this.isRemovalProposalAvailable = !this.isRemovalProposalAvailable;
  }

  //Called functions (Update, remove etc) - Submissions
  
  public updateSubmissionData() {
    var submissionDetails = {
      mentions: this.submissionUpdateForm.value.mentions
    };

    this.submissionService.updateSubmissionDetails(submissionDetails, this.crtSubmission.submissionId!).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeUpdateSubmissionPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public showCV = (event): void => {
    this.crtSubmission = this.submissions[event.row.rowIndex];

    if(this.crtSubmission.cv == null) {
      this.toastr.warning("No CV available for the selected submission");
    } else if(this.crtSubmission.cv == "Waiting"){
      var cvOpenLink = this.crtSubmission.cv;
      window.open(cvOpenLink, "_blank");
      this.changeCandidateStatus();
    } else {
      var cvOpenLink = this.crtSubmission.cv;
      window.open(cvOpenLink, "_blank");
    }
  }

  public changeCandidateStatus() {
    this.submissionService.changeCandidateStatus(this.crtSubmission.submissionId!).then(async response => {
      this.ngOnInit();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  //Called functions - Candidates
  public onSendRemoval() {
    this.crtUser = this.accountService.currentUserValue;
    
    console.log(this.crtCandidate.email);

    let values = {
      emailSender: this.crtUser.email,
      emailCandidate: this.crtCandidate.email
    };

    this.submissionService.requestCandidateRemoval(values).then(async response => {
      this.toastr.success(`Server response: ${response}`);
      this.closeCandidateRemovalPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }


  //other functions
  public calculateCompleteNameCell = (event): string => {
    let user = this.submissions[event.row.rowIndex].user!;
    console.log('Ajunge la calculateCompleteNameCell??');
    return user.firstName + " " + user.lastName;
  }

}
