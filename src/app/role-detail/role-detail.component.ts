import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { User } from '../objects/User';
import { RoleService } from '../_services/role.service';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.css']
})
export class RoleDetailComponent implements OnInit {

  @Input() key!: string;

  public rolesByUser: any[] = [];

  public users: User[] = [];

  //devextreme not retrieved users elements
  public allMode!: string;
  public checkBoxesMode!: string;


  public isRemovalFromRoleAvailable: boolean = false;
  public crtSelectedUser!: User;


  constructor(private roleService: RoleService, private toastr: ToastrService) { 
    this.allMode = 'allPages';
    this.checkBoxesMode = 'onClick';
  }

  ngOnInit(): void {
    this.getAllRolesByUser();
  }


  public getAllRolesByUser() {
    console.log(this.key);
    this.roleService.getAllRolesByUser(this.key).then(async response => {
      this.rolesByUser = [];
      response.forEach(role => {
        this.rolesByUser.push(role);
      });
    });
  }

  public openUserInRoleRemoval = (event):void => {
    this.isRemovalFromRoleAvailable = true;
    this.crtSelectedUser = this.rolesByUser[event.row.rowIndex];
  }

  public closeUserInRoleRemoval() {
    this.isRemovalFromRoleAvailable = !this.isRemovalFromRoleAvailable;
  }

  public removeUserInRole() {
    if(this.crtSelectedUser != null) {
      let userId = this.crtSelectedUser.id;
      this.roleService.removeUserInRole(userId).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeUserInRoleRemoval();
      }, error => {
        console.log(`Error ${error}`);
        this.toastr.error(`Error ${error}`);
        this.closeUserInRoleRemoval();
      });
    }
  }

}
