import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Submission } from '../objects/Submission';
import { User } from '../objects/User';
import { TransferService } from '../services/transfer.service';
import { SubmissionService } from '../_services/submission.service';

@Component({
  selector: 'app-user-applications',
  templateUrl: './user-applications.component.html',
  styleUrls: ['./user-applications.component.css']
})
export class UserApplicationsComponent implements OnInit {

  public submissionsSent!: Submission[];

  public availableFields!: string[];

  public obtainedUser!: User;


  constructor(
    private toastr: ToastrService, 
    private transferService: TransferService,
    private submissionService: SubmissionService) {
    this.getUserFromMain();
    this.GetUserSubmissions();
  }

  ngOnInit(): void {
    this.GetUserSubmissions();
  }

  public GetUserSubmissions(){
    var currentUserId = this.obtainedUser.id;
    this.submissionService.getUserSubmissions(currentUserId).then((response) => {
      console.log(response);
      this.submissionsSent = response;
    });
  }

  public getUserFromMain(){
    this.obtainedUser = this.transferService.getData();
    console.log(this.obtainedUser);
  }

  public withdrawSubmission(submission: Submission){
    var submissionDeleteId : number;
    submissionDeleteId = submission.submissionId!;

    this.submissionService.removeSubmission(submissionDeleteId).then(()=>{
      this.toastr.success('Withdrawal of submission was a success!');
      this.ngOnInit();
    });
  }
}
