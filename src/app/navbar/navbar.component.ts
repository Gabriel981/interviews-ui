import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Notification } from '../objects/Notification';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { NotificationService } from '../_services/notification.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public obtainedUser!: User;

  public hidden = false;
  public enabledNotifications: boolean = false;
  public numberOfNotifications!: number;

  public availableNotifications!: Notification[];

  constructor(private accountService: AccountService, private notificationService: NotificationService, private toastr: ToastrService) { 
    this.obtainedUser = this.accountService.currentUserValue;
  }

  ngOnInit(): void {
    // this.getAvailableNotifications();
    // this.getCurrentUser();
  }

  // public getCurrentUser() {
  //   this.accountService.getCurrentUser().then(user => {
  //     this.obtainedUser = user;
  //     this.getAvailableNotifications();
  //   }, error => {
  //     console.log(error.error);
  //     this.toastr.error(`Error: ${error.error}`);
  //   });
  // }

  public get noOfNotifications() {
    return this.numberOfNotifications;
  }

  public toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }

  public getAvailableNotifications() {
    if (this.obtainedUser.enableNotifications == true) {
      var receiverId = this.obtainedUser.id!;
      this.notificationService.getNotifications(receiverId).then((response) => {
        this.availableNotifications = response;
        this.enabledNotifications = true;
        this.numberOfNotifications = response.length;
      });
    }
  }


}
