import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Candidate } from '../objects/Candidate';
import { CandidateService } from '../_services/candidate.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {

  public candidates!: Candidate[];
  public closeResult!: string;
  private deletedId! : number;

  public candidateDetails!: FormGroup;

  constructor(
    private modalService: NgbModal,
    private candidateService: CandidateService)
  {
    this.candidateDetails = new FormGroup({
      emailAddress: new FormControl('', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      firstName: new FormControl('',[
        Validators.required
      ]),
      lastName: new FormControl('',[
        Validators.required
      ]),
      dateOfBirth: new FormControl('', [
        Validators.required
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.pattern("^(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$")
      ]),
      address: new FormControl('', [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.getAllCandidates();
  }

  get emailAddress(){
    return this.candidateDetails.get('emailAddress');
  }
  
  get firstName(){
    return this.candidateDetails.get('firstName');
  }

  get lastName(){
    return this.candidateDetails.get('lastName');
  }

  get dateOfBirth(){
    return this.candidateDetails.get('dateOfBirth');
  }

  get phoneNumber(){
    return this.candidateDetails.get('phoneNumber');
  }

  get address(){
    return this.candidateDetails.get('address');
  }

  public getAllCandidates(){
    this.candidateService.getAllCandidates().then((response) => {
      this.candidates = response;
    });
  }

  public open(content:any){
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
        this.closeResult = `Closed with: $(result)`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
    if(reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK){
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  public onSubmit(){
    if(this.candidateDetails.valid){
      const candidateCollectedVales = {
        firstName: this.candidateDetails.value.firstName,
        lastName: this.candidateDetails.value.lastName,
        dateOfBirth: this.candidateDetails.value.dateOfBirth,
        address: this.candidateDetails.value.address,
        emailAddress: this.candidateDetails.value.emailAddress,
        phoneNumber: this.candidateDetails.value.phoneNumber
      };

      this.candidateService.submitCandidateDetails(candidateCollectedVales).then((response) =>{
        this.ngOnInit();
        this.modalService.dismissAll();
      });
    }
  }

  public openDetails(targetModal: any, candidate:Candidate){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    const dateValidity = new DatePipe('en-GB').transform(candidate.dateOfBirth, 'dd/MM/yyyy'); 

    document.getElementById('candidateID')?.setAttribute('value', candidate.candidateID!.toString());
    document.getElementById('firstName')?.setAttribute('value', candidate.firstName!);
    document.getElementById('lastName')?.setAttribute('value', candidate.lastName!);
    document.getElementById('dateOfBirth')?.setAttribute('value', dateValidity!);
    document.getElementById('address')?.setAttribute('value', candidate.address!);
    document.getElementById('emailAddress')?.setAttribute('value', candidate.email!);
    document.getElementById('phoneNumber')?.setAttribute('value', candidate.phoneNumber!);  
  }

  public openDelete(targetModal: any, candidate:Candidate){
    this.deletedId = candidate.candidateID!;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  public onDelete(){
    this.candidateService.removeCandidate(this.deletedId).then((response) => {
      this.ngOnInit();
      this.modalService.dismissAll();
    });
  }
}
