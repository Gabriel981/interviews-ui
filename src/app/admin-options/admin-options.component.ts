import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AuditLog } from '../objects/AuditLog';
import { AdminService } from '../_services/admin.service';
import { AuditService } from '../_services/audit.service';

@Component({
  selector: 'app-admin-options',
  templateUrl: './admin-options.component.html',
  styleUrls: ['./admin-options.component.css']
})
export class AdminOptionsComponent implements OnInit {


  //devextreme components
  public auditLogs : AuditLog[] = [];



  public availableProperties: string[] = [];

  public finalProperties: string[] = [];

  constructor(private adminService: AdminService, private toastr: ToastrService, private auditService: AuditService) { }

  ngOnInit(): void {
    // this.getAvailableProperties();
    this.getAvailableProperties();
    this.getAuditTrailLogs();
  }

  public getAvailableProperties() {
    this.adminService.GetAvailableHiringFormProperties().then((response) => {
      this.availableProperties = response;
      this.GetNewFields();
    }, error => {
      console.log(error);
      this.toastr.error(`Server error ${error.error}`);
    });
  }

  public drop(event: CdkDragDrop<string[]>) {
    if(event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
  }

  public SendNewApplicationFormat() {
    const mapData : { [key:number]: string; } = {};

    let emailAddressPresent: boolean = false;
    this.finalProperties.forEach(element => {
      if(element == 'EmailAddress') {
        emailAddressPresent = true;
      }
    });

    if(emailAddressPresent != false) {
      for (let index = 0; index < this.finalProperties.length; index++) {
        mapData[index] = this.finalProperties[index];
      }
  
      this.adminService.PostNewFields(mapData).then((response) => {
        this.toastr.success("Field successfully updated in database");
        let element: HTMLElement = document.getElementById('btnDismiss') as HTMLElement;
        element.click();
      }, error => {
        console.log(error.error);
        this.toastr.error(`Error ${error.error}`);
      });  
    } else {
      this.toastr.warning("Email address field is mandatory in Submission form");
    }
  }

  public GetNewFields() {
    this.adminService.GetNewFields().then((response) => {
      console.log(response);
      for(let [key, value] of Object.entries(response)) {
        this.finalProperties.push(value as string);
      }
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error ${error.error}`);
    });
  }



  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // New version - with devextreme
  public getAuditTrailLogs() {
    this.auditService.getAuditTrailLogs().then(async response => {
      this.auditLogs = [];
      this.auditLogs = response;
    });
  }

  public exportAuditLogTrails() {
    this.auditService.exportAuditTrailData().then(async response => {
      this.downloadData(response);

      this.toastr.success('The requested data are ready. Check download tab for generated file');
    }, error => {
      console.log(`Error: ${error}`);
      this.toastr.error(`Error ${error}`);
    });
  }

  public downloadData(data: any) {
    var crtTime = new Date();
    var crtTimeFormatted = formatDate(crtTime, 'dd/MM/yyyy HH:mm:ss', 'en-US');

    let blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;'});
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Audit trail logs: ' + crtTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }
}
