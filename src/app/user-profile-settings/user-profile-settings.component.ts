import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from '../objects/User';
import { TransferService } from '../services/transfer.service';
import { FieldStateMatcher } from '../_helpers/state.matcher.helper';
import { AccountService } from '../_services/account.service';
import { NotificationService } from '../_services/notification.service';

@Component({
  selector: 'app-user-profile-settings',
  templateUrl: './user-profile-settings.component.html',
  styleUrls: ['./user-profile-settings.component.css']
})
export class UserProfileSettingsComponent implements OnInit {

  public obtainedUser!: User;
  
  public isChecked!: boolean;
  public isCheckedTwoFactor!: boolean;

  public notificationTextStatus!: string;

  public notificationSliderStatus: boolean = true;

  public isDisabled: boolean = false;
  public isDisabledTwoFactor: boolean = false;
  public isDisabledTwoFactorEmail: boolean = false;
  public isDisabledTwoFactorGoogle: boolean = false;
  public isCheckedTwoFactorEmail: boolean = false;
  public isCheckedTwoFactorGoogle: boolean = false;

  public isDisabledDelete!: boolean;

  public tokenMatcher = new FieldStateMatcher();

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private notificationService: NotificationService,
    private accountService: AccountService) { }

  ngOnInit(): void {
    this.getUserData();
  }
  
  public getUserData(){
    this.obtainedUser = this.accountService.currentUserValue;
    let role = this.accountService.currentRoleValue;

      if(role== 'Administrator') {
        this.isDisabled = true;
        this.isDisabledDelete = true;
      } else if (role == 'Manager departament resurse umane') {
        this.isDisabled = false;
        this.isDisabledDelete = true;
      } else {
        this.isDisabled = false;
        this.isDisabledDelete = false;
      }

    this.setNotificationStatus();
    this.setTwoFactorStatus();
    this.setTwoFactorEmailStatus();

    // if(this.obtainedUser.role == "Administrator") {
    //   this.isDisabled = true;
    //   this.isDisabledDelete = true;
    // } else if(this.obtainedUser.role == "Manager departament resurse umane"){
    //   this.isDisabled = false;
    //   this.isDisabledDelete = true;
    // } else {
    //   this.isDisabled = false;
    //   this.isDisabledDelete = false;
    // }
    // this.setNotificationStatus();
  }

  public onChange($event: MatSlideToggleChange) {
    this.isChecked = $event.checked;
    this.notificationService.ChangeNotificationStatus(this.obtainedUser.id!, this.isChecked).then(() => {
      this.showSuccessNotificationStatusChange();
      setTimeout(() => {
        this.router.navigate(["/user/options"]);
      }, 2500);
    });
  }

  public onChangeAuth($event: MatSlideToggleChange) {
    this.isCheckedTwoFactor = $event.checked;
    
    let crtUser = this.accountService.currentUserValue;
    let accountSettings = {
      userId: crtUser.id,
      isTwoFactorEnabled: this.isCheckedTwoFactor,
      isNotificationEnabled: this.isChecked,
      enabledTwoFactorGoogle: this.isCheckedTwoFactorGoogle
    };

    this.accountService.changeAccountSettings(accountSettings).then(async response => {
      this.accountService.refreshUser();
      this.ngOnInit();
      this.toastr.success(`Server response ${response}`);
      this.isCheckedTwoFactor = !this.isCheckedTwoFactor;
    }, error => {
      console.log(`Error ${error}`);
      this.toastr.error(`Error ${error}`);
    });
  }

  public onChangeGoogleAuth($event: MatSlideToggleChange) {
    this.isCheckedTwoFactorGoogle = $event.checked;
    console.log(this.isCheckedTwoFactorGoogle);
    
    let crtUser = this.accountService.currentUserValue;

    let accountSettings = {
      userId: crtUser.id,
      isTwoFactorEnabled: this.isCheckedTwoFactor,
      isNotificationEnabled: this.isChecked,
      isTwoFactorGoogleEnabled: this.isCheckedTwoFactorGoogle
    };

    this.accountService.changeAccountSettings(accountSettings).then(async response => {
      if(response == true) {
        this.accountService.refreshUser().then(async () => {
          this.ngOnInit();
        });
      }
      
      this.toastr.success(`Google auth - two factor authentication - enabled`)
      // this.isDisabledTwoFactorGoogle = !this.isDisabledTwoFactorGoogle;
      // this.isCheckedTwoFactor = !this.isCheckedTwoFactor;
    }, error => {
      console.log(`Error ${error}`);
      this.toastr.error(`Error ${error}`);
    });
  }

  public showSuccessNotificationStatusChange() {
    if(this.isChecked == true) {
      this.toastr.success("The notification was succesfully enabled");
    }
    else {
      this.toastr.success("The notifications was successfully disabled");
    }
  }

  public setNotificationStatus() {
    this.isChecked = this.obtainedUser.enableNotifications!;
  }

  public setTwoFactorStatus() {
    this.isCheckedTwoFactor = this.obtainedUser.twoFactorEnabled!;
  }

  public setTwoFactorEmailStatus() {
    if(this.obtainedUser.enabledTwoFactorGoogle == true) {
      console.log('a dat de Google in init');
      this.isDisabledTwoFactorEmail = true;
      this.isCheckedTwoFactorEmail = false;
      this.isCheckedTwoFactorGoogle = true;
    } else {
      this.isDisabledTwoFactorEmail = false;
      this.isCheckedTwoFactorEmail = true;
      this.isCheckedTwoFactorGoogle = false;
    }
  }

  public removeCurrentAccount() {
    const model = {
      userId: this.obtainedUser.id,
      reason: 'Delete'
    };
    
    this.accountService.removeCurrentAccount(model).subscribe((response) => {
      this.toastr.success(`Server response: ${response}`);
    }, error => {
      console.log(error);
      this.toastr.error(`Error ${error}`);
    }, () => {
      this.router.navigate(['/']);
      this.accountService.logout();
    });
  }
}
