import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Position } from '../objects/Position';
import { User } from '../objects/User';
import { RegisterService } from '../services/register.service';
import { TransferService } from '../services/transfer.service';
import { UploadService } from '../services/upload.service';
import { SubmissionService } from '../_services/submission.service';

@Component({
  selector: 'app-application-registration',
  templateUrl: './application-registration.component.html',
  styleUrls: ['./application-registration.component.css']
})
export class ApplicationRegistrationComponent implements OnInit {

  @ViewChild('inputFile') inputFile!: ElementRef;
  
  public positionRetrieved!: Position;

  public data: any = this.tranferService.getData();

  public submissionFormDetails!: FormGroup;

  private tempUploadedFileUser!: any;

  // public submissionFormDetails: any = {};

  public availablePositionField: boolean = false;

  public availableFileField: boolean = false;

  public availablePasswordField: boolean = false;

  public availableFields: string[] = [];

  private registerService: RegisterService = new RegisterService(this.httpClient, this.router, this.modalService);

  hide = true;


  //Stepper FormGroups
  public positionsFormGroup!: FormGroup;
  public personalDetailsFormGroup!: FormGroup;
  public accountDetailsFormGroup!: FormGroup;

  constructor(
    private uploadService: UploadService,
    private toastr: ToastrService,
    private router: Router,
    private tranferService: TransferService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private submissionService: SubmissionService) { 
    if(this.data){
      
    } else {
      this.router.navigate(["/application"]);
    }


    //   this.availableFields.forEach(element => {
    //     if(element == 'firstName' || element == 'lastName' || element == 'address') {
    //       this.submissionFormDetails[`${element}`] = new FormControl('', [
    //         Validators.required,
    //         Validators.minLength(5),
    //         Validators.maxLength(45)
    //       ]);
    //     } else if(element == 'position') {
    //       console.log('ajunge la element == position')
    //       this.submissionFormDetails["positionName"] = new FormControl('', Validators.required);
    //       this.submissionFormDetails["positionDescription"] = new FormControl('', Validators.required);
    //     } else {
    //       this.submissionFormDetails[`${element}`] = new FormControl('', Validators.required);
    //     }
    //   });
    //   this.showData();


    // new FormGroup(this.submissionFormDetails);

    // this.submissionFormDetails = new FormGroup({
    //   positionName: new FormControl(''),
    //   positionDescription: new FormControl(''),
    //   password: new FormControl('', [Validators.required]),
    //   passwordConfirm: new FormControl('', [Validators.required]),
    //   curriculumFile: new FormControl('', [Validators.required]),

    //   firstName: new FormControl('', [Validators.required, Validators.minLength(5)]),
    //   lastName: new FormControl('', [Validators.required]),
    //   dateOfBirth: new FormControl('', [Validators.required]),
    //   address: new FormControl('', [Validators.required]),
    //   emailAddress: new FormControl('', [Validators.required]),
  
    //   securityQuestion: new FormControl('', [Validators.required]),
    //   securityQuestionAnswer: new FormControl('', [Validators.required]),
    //   phoneNumber: new FormControl('', [Validators.required]),
    //   file: new FormControl('')
    // });   

    // this.showData();


    this.positionsFormGroup = new FormGroup({
      positionName: new FormControl('', [
        Validators.required
      ]),
      positionDescription: new FormControl('', [
        Validators.required
      ])
    });

    this.personalDetailsFormGroup = new FormGroup({
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(40)
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(40)
      ]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(12),
        Validators.maxLength(15)
      ])
    });

    this.accountDetailsFormGroup = new FormGroup({
      emailAddress: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      securityQuestion: new FormControl('', [
        Validators.required
      ]),
      securityQuestionAnswer: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30)
      ])
    });

    this.showData();
  }

  public get positionName() {
    return this.submissionFormDetails.get('positionName');
  }

  public get positionDescription(){
    return this.submissionFormDetails.get('positionDescription');
  }


  public get candidateName() {
    return this.submissionFormDetails.get('candidateName');
  }

  public get dateOfBirth() {
    return this.submissionFormDetails.get('dateOfBirth');
  }

  public get address() {
    return this.submissionFormDetails.get('address');
  }

  public get emailAddress() {
    return this.submissionFormDetails.get('emailAddress');
  }

  public get password() {
    return this.submissionFormDetails.get('password');
  }

  public get passwordConfirm() {
    return this.submissionFormDetails.get('passwordConfirm');
  }

  public get phoneNumber() {
    return this.submissionFormDetails.get('phoneNumber');
  }

  public get position() {
    return this.submissionFormDetails.get('position');
  }

  ngOnInit(): void {
    console.log(this.data);   
  }

  public showData(){
    console.log('ajunge aici chiar si la recruiter');
    this.positionsFormGroup.controls["positionName"].setValue(this.data.positionName);
    if(this.data.description == null || this.data.description == '') {
      this.positionsFormGroup.controls['positionDescription'].setValue('No data provided yet');
      this.getCurrentFields();
    } else {
      this.positionsFormGroup.controls['positionDescription'].setValue(this.data.description);
      this.getCurrentFields();
    }

    //To be updated when positionDetails is added

    // this.submissionFormDetails.controls["positionDetails"].setValue(this.data.positionDetails);
  }

  public async applicationRegistration(){
    await this.registerUser();
  }

  public async registerUser() {
    var candidateData = {
        "firstName" : this.personalDetailsFormGroup.value.firstName,
        "lastName" : this.personalDetailsFormGroup.value.lastName,
        "password" : this.accountDetailsFormGroup.value.password,
        "emailAddress" : this.accountDetailsFormGroup.value.emailAddress,
        "phoneNumber" : this.personalDetailsFormGroup.value.phoneNumber,
        "securityQuestion" : this.accountDetailsFormGroup.value.securityQuestion,
        "securityQuestionAnswer" : this.accountDetailsFormGroup.value.securityQuestionAnswer,
        "address": this.personalDetailsFormGroup.value.address
    };

    var fileUpload = this.tempUploadedFileUser;
    //var email = this.submissionFormDetails.value.emailAddress;

    return this.registerService.CandidateRegister(candidateData).then(async response => {
      this.registerSubmission();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  public registerSubmission() {

    var firstName = this.personalDetailsFormGroup.value.firstName;
    var lastName = this.personalDetailsFormGroup.value.lastName;
    var emailAddress = this.accountDetailsFormGroup.value.emailAddress;
    var positionId = this.data.positionId;

    var submissionData : any;

    if(firstName == null || firstName == 'undefined' || lastName == null || lastName == 'undefined') {
      submissionData = {
        'emailAddress': emailAddress,
        'positionId': positionId
      };
    } else {
      submissionData = {      
        "firstName": this.personalDetailsFormGroup.value.firstName,
        "lastName": this.personalDetailsFormGroup.value.lastName,
        "emailAddress": this.accountDetailsFormGroup.value.emailAddress,
        "positionId": this.data.positionId
      };
    }

    console.log(this.availableFileField != false);

    this.submissionService.uploadSubmission(submissionData).then(async response => {
      console.log(this.availableFileField);
      if(this.availableFileField != false) {
        this.uploadCV(submissionData.emailAddress);
      }
      
      this.toastr.success(`Server response ${response}`);
      this.router.navigate(["/"]);
    });
  }

  public uploadCV(emailAddress: string){
      const file = this.tempUploadedFileUser.files[0];
      // this.uploadService.uploadFile(file, "ceva").subscribe(data => {

      // }, error => {
      //   console.log(error);
      // });

      console.log("ajunge fix inainte de uploadFile()");
      this.uploadService.uploadFile(file, emailAddress).subscribe(data => {
        console.log('aici nu cred ca mai ajunge');
      }, error => {
        console.log(error);
      });
  }

  public storeTempFile(event){
      if(event.target.files.length > 0){
        // const file = event.target.files[0];
        this.tempUploadedFileUser = event.target;
        console.log(this.tempUploadedFileUser);
    }
  }
  
  public getCurrentFields() {
    this.submissionService.getSortedFields().then((response) => {
      console.log(response);
      for(let [key, value] of Object.entries(response)) {
        var convertedValue = this.convertToCamelCase(value as string);
        if(convertedValue != 'position') {
          if(convertedValue == 'file') {
            this.availableFileField = true;
          } else {
            if(convertedValue == 'password') {
              this.availablePasswordField = true;
            } else {
              this.availableFields.push(convertedValue);
            }
          }
        } else {
          this.availablePositionField = true;
        }
      }
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public convertToCamelCase(value: string) {
    value = value.replace(/[-_\s.]+(.)?/g, (_, c) => c ? c.toUpperCase() : '');
    return value.substr(0,1).toLowerCase() + value.substr(1);
  }

}
