export class ApplicationRole {
    public id!: string;
    public name!: string;
    public normalizedName!: string;

    constructor(data: any) {
        Object.assign(this, data);
    }
}