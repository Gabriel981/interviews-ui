import { Position } from "./Position";
import { Role } from "./Role";

export class Employee {
    firstName?: string;
    lastName?: string;
    address?: string;
    phoneNumber?: string;
    position?: Position;
    professionalismLevel?: number;
    mentions?: string;
    email?: string;
    password?: string;
    token?: string;
    securityQuestion?: string;
    securityQuestionAnswer?: string;
    role?:string;
    rolle?: Role;

}