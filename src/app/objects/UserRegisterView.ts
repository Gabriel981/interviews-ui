export class UserRegisterView {
    firstName!: string;
    lastName!: string;
    password!: string;
    emailAddress!: string;
    phoneNumber!: string;
    securityQuestion!: string;
    securityQuestionAnswer!: string;
    address!: string;

    constructor(data: any) {
        Object.assign(data);
    }
}