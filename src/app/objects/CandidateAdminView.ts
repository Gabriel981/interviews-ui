export class CandidateAdminView{
    firstName!: string;
    lastName!: string;
    address!: string;
    dateOfBirth!: Date;
    submissionDate!: Date;
    email!: string;
    positionName!: string;
    candidateSituation!: string;

    constructor(data: any){
        Object.assign(this, data);
    }
}