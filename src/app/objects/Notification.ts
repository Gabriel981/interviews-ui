export class Notification {
    senderId!: string;
    receiverId!: string;
    firstNameReceiver!: string;
    lastNameReceiver!: string;
    firstNameSender!: string;
    lastNameSender!: string;
    emailReceiver!: string;
    emailSender!: string;
    timestamp!: string;
    title!: string;
    message!: string;
    positionName!: string;
    notificationId!: string;

    public constructor(data:any){
        Object.assign(data);
    }
}