import { Position } from "./Position";

export class User {
    id?: string;
    firstName?: string;
    lastName?: string;
    dateOfBirth?: Date;
    address?: string;
    email?: string;
    phoneNumber?: string;
    position?: Position;
    positionId?: number;
    role?: string;
    mentions?: string;
    jwtToken?: string;
    refreshToken?: string;
    securityQuestion?: string;
    answer?: string;
    profilePhotoURL?: string;
    enableNotifications?: boolean;
    twoFactorEnabled?: boolean;
    enabledTwoFactorGoogle?: boolean;

    constructor(data: any){
        Object.assign(this, data);
    }
}