export class AuditLog {
    public id!: string;
    public userId!: string;
    public type!: string;
    public tableName!: string;
    public dateTime!: Date;
    public oldValues!: string;
    public newValues!: string;
    public affectedColumns!: string;
    public primaryKey!: string;

    constructor(data: any) {
        Object.assign(this, data);
    }

}