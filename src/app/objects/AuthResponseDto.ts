import { User } from "./User";

export class AuthResponseDto {
    public isAuthSuccesful!: boolean;
    public errorMessage!: string;
    public appUser!: User;
    public refreshToken!: string;
    public role!: string;
    public is2StepVerificationRequired!: boolean;
    public provider!: string;

    constructor(data: any) {
        Object.assign(this, data);
    }
}