
import { Candidate } from "./Candidate";
import { Interview } from "./Interview";
import { Position } from "./Position";
import { Submission } from "./Submission";

export class HiringProcess {
    hiringId?: number;
    candidateSituation?: string;
    submission?: Submission;
    candidate!: Candidate;
    appliedPosition?: Position;
    interview?: Interview;

    constructor(data: any){
        Object.assign(this, data);
    }
}