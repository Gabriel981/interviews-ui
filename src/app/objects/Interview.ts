import { Candidate } from "./Candidate";
import { HiringProcess } from "./HiringProcess";
import { Quiz } from "./Quiz";
import { Submission } from "./Submission";
import { User } from "./User";

export class Interview {
    interviewId?: number;
    interviewDate?: Date;
    communicationChannel?: string;
    candidateSituation?: string;
    hiringProcess?: HiringProcess;
    candidateFirstName?: string;
    candidateLastName?: string;
    positionName?: string;
    submission?: Submission;
    createdBy?: User;
    lastModifiedBy?: User;
    quiz?: Quiz;

    public constructor(data: any){
        Object.assign(data);
    }
}