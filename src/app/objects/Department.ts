export class Department {
    departmentId!: number;
    departmentName!: string;
    description!: string;
    active!: boolean;
    currentNumberOfPositions!: number;

    constructor(data: any) {
        Object.assign(this, data);
    }
}