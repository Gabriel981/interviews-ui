import { Position } from "./Position";

export class InterviewParticipantView {
    public participantId!: string;
    public firstName!: string;
    public lastName!: string;
    public email!: string;
    public phoneNumber!: string;
    public interviewId!: number;
    public position!: Position;
    public attendanceDate!: Date;

    constructor(data: any){
        Object.assign(this, data);
    }
}