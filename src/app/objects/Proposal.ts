import { User } from "./User";

export class Proposal {
    admin!: User;
    candidate!: User;
    message!: string;
    timestamp!: Date;

    constructor(data: any){
        Object.assign(this, data);
    }
}