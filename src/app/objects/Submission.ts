import { Candidate } from "./Candidate";
import { Position } from "./Position";
import { User } from "./User";

export class Submission {
    submissionId?: number;
    dateOfSubmission!: Date;
    mentions?:string;
    positionName?: string;
    firstName?: string;
    user?: User;
    appliedPosition!: Position;
    candidateId?: string;
    status!: string;
    cv!: string;

    constructor(data: any){
        Object.assign(this, data);
    }
}