export class Candidate {
    id?: string;
    firstName!: string;
    lastName!: string;
    completeName!: string;
    dateOfBirth?: Date;
    address?: string;
    email?: string;
    phoneNumber?: string;
    submissionDate?: Date;
    candidateSituation?: string;
    positionName?: string;

    constructor(data: any){
        Object.assign(this, data);
    }
}