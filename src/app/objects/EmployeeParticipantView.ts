export class EmployeeParticipantView {
        public firstName!: string;
        public lastName!: string;
        public email!: string;
        public phoneNumber!: string;
        public address!: string;
        public dateOfBirth!: Date;
        public positionId!: number;
        public attentedInterviewId!: number;

        constructor(data: any){
            Object.assign(this, data);
        }
}