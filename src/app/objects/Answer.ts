import { User } from "./User";

export class Answer {
    answerId!: number;
    answerBody!: string;
    questionId!: number;
    createdBy!: User;
    createdAt!: string;

    constructor(data: any) {
        Object.assign(this, data);
    }
}