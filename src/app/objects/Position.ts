import { Department } from "./Department";

export class Position {
    positionId?: number;
    positionName!: string;
    department?: Department;
    description?: string;
    noVacantPositions?: number;
    availableForRecruting?: boolean;

    constructor(data:any){
        Object.assign(this, data);
    }
}