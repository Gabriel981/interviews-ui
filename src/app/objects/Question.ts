export class Question {
    questionId!: number;
    questionBody!: string;
    createdBy!: string;
    createdAt!: string;
    noOfAnswers!: number;
    position!: string;
    lastModifiedBy!: string;

    constructor(data: any) {
        Object.assign(this, data);
    }
}