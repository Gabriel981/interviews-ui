export class ParticipantView {
    public id!: string;
    public firstName!: string;
    public lastName!: string;
    public positionName!: string;
    public email!: string;
    public phoneNumber!: string;
    public interviewScheduledOn!: Date;
    public interviewScheduledFor!: string;
    public attendedOn!: Date;
    public interviewId!: number;

    public constructor(data: any) {
        Object.assign(this, data);
    }
}