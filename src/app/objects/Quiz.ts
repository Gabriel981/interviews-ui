import { Position } from "./Position";
import { User } from "./User";

export class Quiz {
    quizId?: number;
    mentions?: string;
    scoreResult?: number;
    position?: Position;
    positionId?: number;
    createdAt?: string;
    createdBy?: User;
    
    public constructor(data: any) {
        Object.assign(data);
    }
}