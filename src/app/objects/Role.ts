export enum Role {
    Candidate = 'Registered Candidate',
    Recruiter = 'Recruiter'
}