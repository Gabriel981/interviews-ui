import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FieldStateMatcher } from 'src/app/_helpers/state.matcher.helper';
import { AccountService } from 'src/app/_services/account.service';

@Component({
  selector: 'app-two-step-verification-google',
  templateUrl: './two-step-verification-google.component.html',
  styleUrls: ['./two-step-verification-google.component.css']
})
export class TwoStepVerificationGoogleComponent implements OnInit {

  public imageUrl!: string;
  public manualKey!: string;
  public _email!: string;

  public twoStepForm!: FormGroup;

  public verificationMatcher = new FieldStateMatcher();

  constructor(private route: ActivatedRoute, private router: Router, private accountService: AccountService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.manualKey = sessionStorage.getItem('setupCode')!;
    this.imageUrl = sessionStorage.getItem("qrCode")!;
    this._email = this.route.snapshot.queryParams['email'];
  
    this.twoStepForm = new FormGroup({
      verificationCode: new FormControl('', [
        Validators.required,
        Validators.min(6)
      ])
    });
  }

  public loginUser = (twoStepFromValue) => {
 
    const formValue = {
      tokenCode: twoStepFromValue.tokenCode
    };

    let details: Object = {
      email: this._email,
      userKey: this.twoStepForm.value.verificationCode
    };

    this.accountService.twoStepGoogleLogin(details).then(async response => {
      this.toastr.success(`Succesfully logged in !\nWelcome back ${this.accountService.currentUserValue.firstName} ${this.accountService.currentUserValue.lastName}`);
      setTimeout(() => {
        this.router.navigate(["/"]).then(() => {
          window.location.reload();
          sessionStorage.removeItem('qrCode');
          sessionStorage.removeItem('setupCode');
        });
      }, 2500);
    });
  }

}
