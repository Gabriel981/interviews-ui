import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TwoFactorDto } from 'src/app/objects/TwoFactorDto';
import { User } from 'src/app/objects/User';
import { FieldStateMatcher } from 'src/app/_helpers/state.matcher.helper';
import { AccountService } from 'src/app/_services/account.service';

@Component({
  selector: 'app-two-step-verification',
  templateUrl: './two-step-verification.component.html',
  styleUrls: ['./two-step-verification.component.css']
})
export class TwoStepVerificationComponent implements OnInit {

  public twoStepForm!: FormGroup;

  private _provider!: string;
  private _email!: string;
  private _returnUrl!: string;
  public user!: User;

  public tokenMatcher = new FieldStateMatcher();
  
  constructor(private route: ActivatedRoute, private router: Router, private accountService: AccountService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.twoStepForm = new FormGroup({
      tokenCode: new FormControl('', [
        Validators.required,
        Validators.min(6),
        Validators.max(7)
      ])
    });

    this._provider = this.route.snapshot.queryParams['provider'];
    this._email = this.route.snapshot.queryParams['email'];
    // this._returnUrl = this.route.snapshot.queryParams['returnUrl'];
  }

  public loginUser = (twoStepFromValue) => {
 
    const formValue = {
      tokenCode: twoStepFromValue.tokenCode
    };

    let twoFactorDto: TwoFactorDto = {
      email: this._email,
      provider: this._provider,
      token: formValue.tokenCode
    };

    this.accountService.twoStepLogin(twoFactorDto).then(async response => {
      this.toastr.success(`Succesfully logged in !\nWelcome back ${this.accountService.currentUserValue.firstName} ${this.accountService.currentUserValue.lastName}`);
      setTimeout(() => {
        this.router.navigate(["/"]).then(() => {
          window.location.reload();
        });
      }, 2500);
    });
  }

}
