import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { PositionComponent } from './position/position.component';
import { SubmissionComponent } from './submission/submission.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterviewComponent } from './interview/interview.component';
import { HiringProcessComponent } from './hiring-process/hiring-process.component';
import { Globals } from './global';
import { LoginComponent } from './login/login.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuard } from './guards/auth-guard.service';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule, MAT_DATE_FORMATS } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { UserSubmissionComponent } from './user-submission/user-submission.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { IgxAvatarModule, IgxCardModule, 	IgxButtonModule, IgxIconModule } from 'igniteui-angular';
import { EmployeeComponent } from './employee/employee.component';
import { ProfileComponent } from './profile/profile.component';
import { MatButtonModule } from '@angular/material/button';
import { CandidateAdminComponent } from './candidate-admin/candidate-admin.component';
import { ApplicationRegistrationComponent } from './application-registration/application-registration.component';
import { MatIconModule } from '@angular/material/icon';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { UserApplicationsComponent } from './user-applications/user-applications.component';
import { RegisteredSubmissionComponent } from './registered-submission/registered-submission.component';
import { ToastrModule } from 'ngx-toastr';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ManagerPanelComponent } from './manager-panel/manager-panel.component';
import { CandidateTrackingComponent } from './candidate-tracking/candidate-tracking.component';
import { MatBadgeModule } from '@angular/material/badge';
import { InterviewTrackingComponent } from './interview-tracking/interview-tracking.component';
import { InterviewParticipantComponent } from './interview-participant/interview-participant.component';
import { UserProfileSettingsComponent } from './user-profile-settings/user-profile-settings.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { JwtInterceptor } from './guards/JwtInterceptor';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CandidateComponent } from './candidate/candidate.component';
import { CommonModule } from '@angular/common';
import { AdminOptionsComponent } from './admin-options/admin-options.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatStepperModule } from '@angular/material/stepper';
import { MatExpansionModule } from '@angular/material/expansion';
import { QuestionComponent } from './question/question.component';
import { MatSortModule } from '@angular/material/sort';
import { DepartmentComponent } from './department/department.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { DxChartModule, DxPieChartModule, DxScrollViewModule } from 'devextreme-angular';

import {DxButtonModule, DxDataGridModule, DxPopupModule} from 'devextreme-angular';
import { JwtRefreshTokenInterceptorInterceptor } from './guards/jwt-refresh-token-interceptor.interceptor';
import { FooterComponent } from './_page_elements/footer/footer.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { RolesComponent } from './roles/roles.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { TwoStepVerificationComponent } from './authentication/two-step-verification/two-step-verification.component';
import { TwoStepVerificationGoogleComponent } from './authentication/two-step-verification-google/two-step-verification-google.component';


export function tokenGetter() {
  return localStorage.getItem("jwt");
}

export const DateFormats = {
  parse: {
    dateInput: 'DD-MM-YYYY',
  },
  display: {
    dateInput: 'MMM DD, YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

//I keep the new line
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PositionComponent,
    SubmissionComponent,
    InterviewComponent,
    HiringProcessComponent,
    LoginComponent,
    RegisterEmployeeComponent,
    UserSubmissionComponent,
    AdminDashboardComponent,
    EmployeeComponent,
    ProfileComponent,
    CandidateAdminComponent,
    ApplicationRegistrationComponent,
    UserSettingsComponent,
    UserApplicationsComponent,
    RegisteredSubmissionComponent,
    NotificationDetailsComponent,
    ManagerPanelComponent,
    CandidateTrackingComponent,
    InterviewTrackingComponent,
    InterviewParticipantComponent,
    UserProfileSettingsComponent,
    NavbarComponent,
    SidebarComponent,
    UserProfileComponent,
    CandidateComponent,
    AdminOptionsComponent,
    QuestionComponent,
    DepartmentComponent,
    FooterComponent,
    PasswordResetComponent,
    RolesComponent,
    RoleDetailComponent,
    TwoStepVerificationComponent,
    TwoStepVerificationGoogleComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatSelectModule,
    IgxAvatarModule,
    IgxCardModule,
    IgxIconModule,
    IgxButtonModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatBadgeModule,
    DragDropModule,
    MatStepperModule,
    MatExpansionModule,
    MatSortModule,
    DxDataGridModule,
    DxButtonModule,
    DxPopupModule,
    MatCheckboxModule,
    MatCardModule,
    DxScrollViewModule,
    DxChartModule,
    DxPieChartModule,
    ToastrModule.forRoot(),
    MatSlideToggleModule,
    JwtModule.forRoot({
      config: {
        // tokenGetter: tokenGetter,
        disallowedRoutes: [],
        authScheme: 'JWT'
      }
    })
  ],
  bootstrap: [AppComponent],
  providers: [Globals, AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtRefreshTokenInterceptorInterceptor, multi: true},
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    MatDatepickerModule,
    MatNativeDateModule,
    { provide: MAT_DATE_FORMATS, useValue: DateFormats }
  ]
})
export class AppModule {
}
