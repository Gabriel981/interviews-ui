import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../guards/AuthenticationService';
import { ManagerService } from '../services/manager.service';

@Component({
  selector: 'app-manager-panel',
  templateUrl: './manager-panel.component.html',
  styleUrls: ['./manager-panel.component.css']
})
export class ManagerPanelComponent implements OnInit {

  public candidates!: any[];
  public availableInterviews!: any[];

  public numberOfCandidates!: number;
  public numberOfInterviews!: number;

  private managerService: ManagerService = new ManagerService(this.httpClient, this.authenticationService);

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.GetAllAvailableCandidates();
    this.getAllAvailableInterviews();
  }

  public get noOfCandidates(){
    return this.numberOfCandidates;
  }
  
  public get noOfInterviews() {
    return this.numberOfInterviews;
  }

  public GetAllAvailableCandidates(){
    this.managerService.GetAllAvailableCandidates().then((response) => {
      this.candidates = response;
      this.numberOfCandidates = response.length;
    });
  }

  public getAllAvailableInterviews(){
    this.managerService.GetAllAvailableInterviews().then((response) => {
      if(response != null){
        this.availableInterviews = response;
        this.numberOfInterviews = response.length;
      }
    });
  }

  

}
