import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { timeStamp } from 'console';
import { ToastrService } from 'ngx-toastr';
import { Department } from '../objects/Department';
import { DepartmentService } from '../_services/department.service';


@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  public isUpdatePopupVisible: boolean = false;
  public isDeletePopupVisibile: boolean = false;
  public isAddDepartmentVisible: boolean = false;

  public isAvailableDepartment: boolean  = false;

  public departments: Department[] = [];

  public crtDepartment!: Department;

  //FormGroup declarations
  public updateDetails!: FormGroup;
  public departmentDetails!: FormGroup;

  departmentMatcher = new ErrorStateMatcher();
  departmentDescriptionMatcher = new ErrorStateMatcher();
  
  constructor(private departmentService: DepartmentService, private toastr: ToastrService) { 
    this.departmentDetails = new FormGroup({
      departmentName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ]),
      isAvailableDepartment: new FormControl('', [
        Validators.required
      ])
    });
  }

  ngOnInit(): void {
    this.loadDepartments();
  }

  public loadDepartments() {
    this.departmentService.getDepartments().then((response: any) => {
      if(response.length != 0) {
        this.departments = response;
      } 
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  //Departments methods for opening popups
  public openUpdateDialog = (event) : void => {
    this.isUpdatePopupVisible = !this.isUpdatePopupVisible;
    this.crtDepartment = this.departments[event.row.rowIndex];


    var departmentName = this.crtDepartment.departmentName;
    var description = this.crtDepartment.description;
    var active = this.crtDepartment.active;

    this.updateDetails = new FormGroup({
      departmentName: new FormControl(departmentName, [
        Validators.required,
        Validators.min(2),
        Validators.max(30)
      ]),
      description: new FormControl(description, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ]),
      availableDepartment: new FormControl('', [
        Validators.required
      ])
    });

    this.updateDetails.controls['availableDepartment'].setValue(active);
  }

  public openDeleteDialog = (event): void => {
    this.isDeletePopupVisibile = !this.isDeletePopupVisibile;
    this.crtDepartment = this.departments[event.row.rowIndex];
  }

  public openAddDialog() {
    this.isAddDepartmentVisible = !this.isAddDepartmentVisible;
  }



  //Department methods for closing popups
  public closeUpdateDialog() {
    this.isUpdatePopupVisible = !this.isUpdatePopupVisible;
  }

  public closeDeleteDialog() {
    this.isDeletePopupVisibile = !this.isDeletePopupVisibile;
  }

  public closeAddDialog() {
    this.isAddDepartmentVisible = !this.isAddDepartmentVisible;
  }


  //Various methods inside popup
  public onSaveDepartmentDetails() {
    let departmentDetails = {
      departmentName: this.updateDetails.value.departmentName,
      description: this.updateDetails.value.description,
      active: this.updateDetails.value.availableDepartment
    };

    this.departmentService.updateDepartmentDetails(this.crtDepartment.departmentId, departmentDetails).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeUpdateDialog();
    }, error => {
      console.log(error.error);
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onDeleteDepartment() {
    this.departmentService.removeDepartment(this.crtDepartment.departmentId).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeDeleteDialog();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onSubmitDepartment(f: any) {
    let departmentDetails = {
      departmentName: f.value.departmentName,
      description: f.value.description,
      active: f.value.availableDepartment
    };

    this.departmentService.addDepartment(departmentDetails).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeAddDialog();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }


}
