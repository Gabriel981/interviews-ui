import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { CandidateAdminView } from '../objects/CandidateAdminView';
import { ParticipantView } from '../objects/ParticipantView';
import { Position } from '../objects/Position';
import { User } from '../objects/User';

@Injectable({
    providedIn: 'root'
})
export class ApplicationService {

    private defaultURI = defaultVars.defaultURI;

    constructor(private httpClient: HttpClient) { }

    public getAllAvailablePositions() {
        return this.httpClient.get<Position[]>(`${this.defaultURI}api/candidate/available/positions`)
            .toPromise().then((response: Position[]) => {
                let positionList: Position[] = [];

                response.forEach(position => {
                    positionList.push(position);
                });

                return positionList;
            });
    }

    public getAllAvailableEmployees() {
        return this.httpClient.get<User[]>(`${this.defaultURI}api/admin/employees`)
            .toPromise().then((response) => {
                let employeeList: User[] = [];

                response.forEach(employee => {
                    employeeList.push(employee);
                });

                return employeeList;
            });
    }

    public getAllNotRetrievedUsers() {
        return this.httpClient.get<any>(`${this.defaultURI}api/admin/notretrived`).toPromise()
            .then(async response => {
                let notRetrievedList: User[] = [];

                response.forEach(notRetrieved => {
                    notRetrievedList.push(notRetrieved);
                });

                return notRetrievedList;
            });
    }

    public updateAvailableEmployee(values: any, updateId: string) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.put<User>(`${this.defaultURI}api/admin/` + updateId, values, requestOptions).toPromise();
    }

    public removeAvailableEmployee(employeeRemoveId: string) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.delete<User>(`${this.defaultURI}api/admin/` + employeeRemoveId, requestOptions).toPromise();
    }

    public updateAvailableCandidate(data: any, updateId: string) {
        return this.httpClient.put<User>(`${this.defaultURI}api/user/update/` + updateId, data).toPromise();
    }

    public getAllAvailableCandidates() {
        return this.httpClient.get<CandidateAdminView[]>(`${this.defaultURI}api/admin/candidates`).toPromise()
            .then((response: CandidateAdminView[]) => {
                let candidatesList: CandidateAdminView[] = [];

                response.forEach(candidate => {
                    candidatesList.push(candidate);
                });

                return candidatesList;
            });
    }

    public proposeRemovalOfCandidate(candidateData: any) {
        const requestOptions: Object = {
            responseType: 'text'
        };
        
        return this.httpClient.post(`${this.defaultURI}api/admin/removal_proposal`, candidateData, requestOptions).toPromise();
    }

    public customSearchByCandidateNameFilter(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/candidates/name`, key).toPromise();
    }

    public customSearchByPositionName(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/candidates/position`, key).toPromise();
    }

    public customSearchBySubmissionDate(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/candidates/date`, key).toPromise();
    }
    
    public allocateUserToRole(values: any) {
        let requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/allocate`, values, requestOptions).toPromise();
    }

    public getFilteredPositions() {
        return this.httpClient.get<any>(`${this.defaultURI}api/admin/positions`).toPromise()
            .then(async response => {
                let filteredPositions: Position[] = [];

                response.forEach(filteredPosition => {
                    filteredPositions.push(filteredPosition);
                });

                return filteredPositions;
            });
    }

    public rejectUser(values: any) {
        let requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/reject`, values, requestOptions).toPromise();
    }

    public sendUrgentlyPasswordRequest(values: any) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/request_password_urgent`, values, requestOptions).toPromise();
    }

    public sendAutoGeneratedPasswords(values: any) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/request_password_generated`, values, requestOptions).toPromise();
    }

    public changePassword(values : any) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/reset_password`, values, requestOptions).toPromise();
    }

    public exportEmployeesData() {
        const requiredOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${this.defaultURI}api/admin/employees_report`, requiredOptions).toPromise();
    }

    public removeAllEmployees() {
        const requiredOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.delete<any>(`${this.defaultURI}api/admin/remove_employees`, requiredOptions).toPromise();
    }

    public removeUnretrievedUsers() {
        const requiredOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.delete<any>(`${this.defaultURI}api/admin/remove_unretrieved`, requiredOptions).toPromise();
    }

    public addUserRole(values: any) {
        const requiredOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/create_role`, values, requiredOptions).toPromise();
    }

    public getParticipants() {
        return this.httpClient.get<any>(`${this.defaultURI}api/admin/participants`).toPromise()
            .then(async response => {
                let participants : ParticipantView[] = [];
                
                response.forEach(participant => {
                    participants.push(participant);
                });

                return participants;
            });
    }

    public updateParticipantDetails(values: any) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/admin/participant_update`, values, requestOptions).toPromise();
    }

    public removeParticipationInterview(interviewId: number, participantId: string) {
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.delete<any>(`${this.defaultURI}api/admin/remove_participation/${interviewId}/${participantId}`, requestOptions).toPromise();
    }

    public getUsers() {
        return this.httpClient.get<any>(`${this.defaultURI}api/admin/users`).toPromise()
            .then(async response => {
                let users: User[] = [];

                response.forEach(user => {
                    users.push(user);
                });

                return users;
            });
    }

    public getEmployeeChartData() {
        return this.httpClient.get<any>(`${this.defaultURI}api/admin/chart/employees`).toPromise()
            .then(async response => {
                let employeeChartData: any[] = [];

                response.forEach(departmentDetails => {
                    employeeChartData.push(departmentDetails);
                });

                return employeeChartData;
            });
    }
}
