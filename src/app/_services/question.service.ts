import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getAllQuestions() {
    return this.httpClient.get<any>(`${this.defaultURI}api/questions`).toPromise()
      .then((response: any[]) => {
        let questionsList : any[] = [];

        response.forEach(question => {
          questionsList.push(question);
        });
        
        return questionsList;
      });
  }

  public getQuestionAnswers(questionId: number) {
    return this.httpClient.get<any>(`${this.defaultURI}api/questions/answers/` + questionId).toPromise()
      .then((response: any[]) => {
        let questionAnswers: any[] = [];

        response.forEach((answer) => {
          questionAnswers.push(answer);
        });

        return questionAnswers;
      })
  }

  public updateQuestion(questionId: number, questionUpdateValues: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.put<any>(`${this.defaultURI}api/questions/` + questionId, questionUpdateValues, requestOptions).toPromise();
  }

  public removeQuestion(questionId: number) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<any>(`${this.defaultURI}api/questions/` + questionId, requestOptions).toPromise();
  }
}
