import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public UpdateCandidateDetailsByAdmin(candidateDetails: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.put<any>(`${this.defaultURI}api/admin/candidate/update`, candidateDetails, requestOptions).toPromise();
  }

  public UpdateCandidateSituation(candidateResetDetails: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}api/admin/candidate/status`, candidateResetDetails, requestOptions).toPromise();
  } 

  public GetAvailableHiringFormProperties() {
    return this.httpClient.get<any>(`${this.defaultURI}api/admin/properties`).toPromise()
      .then((response: string[]) => {
        let propertiesList : string[] = [];

        response.forEach(property => {
          propertiesList.push(property);
        });

        return propertiesList;
      });
  }

  public PostNewFields(applicationForm: any) {
    const requestedOptions : Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}api/admin/properties/set`, applicationForm, requestedOptions).toPromise();
  }

  public GetNewFields() {
    return this.httpClient.get<any>(`${this.defaultURI}api/admin/currentFields`).toPromise();
  }

  public searchByKey(key : any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/search`, key).toPromise();
  }

  public searchByName(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/name`, key).toPromise();
  }

  public searchByPosition(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/position`, key).toPromise();
  }

  public searchByDate(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/date`, key).toPromise();
  }

  public searchByKeyInterview(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/search/interview`, key).toPromise();
  }

  public searchByNameInterview(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/interview/name`, key).toPromise();
  }

  public searchByPositionNameInterview(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/interview/position`, key).toPromise();
  }

  public searchByInterviewDate(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/interview/date`, key).toPromise();
  }

  public searchByKeyEmployee(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/search/employees`, key).toPromise();
  }
  
  public customSearchByEmployeeName(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/employee_name`, key).toPromise();
  }

  public customSearchByPositionName(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/admin/filter/employee/position`, key).toPromise();
  }
 }
