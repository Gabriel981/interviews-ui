import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { Candidate } from '../objects/Candidate';
import { Position } from '../objects/Position';
import { Submission } from '../objects/Submission';

@Injectable({
  providedIn: 'root'
})
export class SubmissionService {

  private defaultURI  = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getAllSubmissions(){
    return this.httpClient.get<Submission[]>(`${this.defaultURI}submissions`).toPromise()
        .then((response: Submission[]) => {
            let submissionList: Submission[] = [];

            response.forEach(submission => {
                submissionList.push(submission);
            });

        return submissionList;
        });
}

  public getAllCandidates(){
      return this.httpClient.get<Candidate[]>(`${this.defaultURI}candidates`).toPromise();
  }

  public getAllPositions(){
      return this.httpClient.get<Position[]>(`${this.defaultURI}api/positions`).toPromise()
          .then((response:Position[]) => {
              let positionList: Position[] = [];

              response.forEach(position => {
                  if(position.availableForRecruting == true && position.noVacantPositions! > 0){
                      positionList.push(position);
                  }
              });

              return positionList;
          });
  }

  public uploadSubmission(values: any){
      const requestOptions: Object = {
        responseType: 'text'
      };

      return this.httpClient.post<any>(`${this.defaultURI}submissions/upload`, values, requestOptions).toPromise();
  }

  public updateSubmissionDetails(values: any, updateId: number){
      const requestOptions: Object = {
          responseType: 'text'
      };

      return this.httpClient.put<any>(`${this.defaultURI}submissions/` + updateId, values, requestOptions).toPromise();
  }

  public removeSubmission(deleteId: number){
      const requestOptions: Object = {
        responseType: 'text'
      };

      return this.httpClient.delete<Submission>(`${this.defaultURI}submissions/` + deleteId, requestOptions).toPromise();
  }

  public getUserSubmissions(userId: any){
      return this.httpClient.get<Submission[]>(`${this.defaultURI}api/candidate/available/submissions/` + userId).toPromise()
          .then((response: Submission[]) => {
              let submissionList: Submission[] = [];

              response.forEach(submission => {
                  submissionList.push(submission);
              });

              return submissionList;
          });
  }

  public getCurrentSubmissionCV(submissionId: number){
      return this.httpClient.get<any>(`${this.defaultURI}submissions/cv/` + submissionId).toPromise();
  }

  public exportSubmissionData(){
      const requiredOptions: Object = {
          responseType: 'arraybuffer'
      };

      return this.httpClient.get<any>(`${this.defaultURI}submissions/export`, requiredOptions).toPromise();
  }

  public getSortedFields() {
    return this.httpClient.get<any>(`${this.defaultURI}api/candidate/available/currentFields`).toPromise();
  }

  public scheduleFastInterview(fastInterviewData: any) {
    const requestOptions: Object = {
        responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}submissions/schedule`, fastInterviewData, requestOptions).toPromise();
  }

  public customSearchByDateFilter(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}submissions/filter/submissions/date`, key).toPromise();
  }

  public customSearchByCandidate(key: any) {
      return this.httpClient.post<any>(`${this.defaultURI}submissions/filter/submissions/candidate`, key).toPromise();
  }

  public changeCandidateStatus(id: number) {
    const requestOptions: Object = {
        responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}submissions/status/change/${id}`, null, requestOptions).toPromise();
  }

  public requestCandidateRemoval(values: any) {
      const requestOptions: Object = {
          responseType: 'text'
      };

      return this.httpClient.post<any>(`${this.defaultURI}api/candidate/available/removal`, values, requestOptions).toPromise();
  }

  public getSubmissionsDataChart() {
      return this.httpClient.get<any>(`${this.defaultURI}submissions/chart`).toPromise()
        .then(async response => {
            let submissionsChartData: any[] = [];

            response.forEach(submissionData => {
                submissionsChartData.push(submissionData);
            });

            return submissionsChartData;
        });
  }
}
