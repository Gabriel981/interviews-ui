import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { Position } from '../objects/Position';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private defaultUri = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getAllRoles() {
    return this.httpClient.get<any[]>(`${this.defaultUri}api/roles/all`).toPromise()
      .then(async response => {
        let roles : any [] = [];

        response.forEach(role => {
          roles.push(role);
        });

        return roles;
      });
  }
  
  public getAllRolesByUser(id: string) {
    return this.httpClient.get<any>(`${this.defaultUri}api/roles/${id}`).toPromise()
      .then(async response => {
        let rolesByUser: any[] = [];

        response.forEach(roleByUser => {
          rolesByUser.push(roleByUser);
        });

        return rolesByUser;
      });
  }

  public createNewRole(roleName: string) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultUri}api/roles/add/${roleName}`, null, requestOptions).toPromise();
  }

  public removeRole(roleId: string) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<any>(`${this.defaultUri}api/roles/${roleId}`, requestOptions).toPromise();
  }

  public assignUserToRole(values: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultUri}api/roles/assign`, values, requestOptions).toPromise();
  }

  public getPositions() {
    return this.httpClient.get<any>(`${this.defaultUri}api/roles/positions`).toPromise()
      .then(async response => {
        let positions: Position[] = [];

        response.forEach(position => {
          positions.push(position);
        });

        return positions;
      });
  }

  public removeUserInRole(userId: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<any>(`${this.defaultUri}api/roles/remove/${userId}`, requestOptions).toPromise();
  }
}
