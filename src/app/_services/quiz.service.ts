import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public addQuiz(quizData: any) {
    const requestOptions : Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}api/quiz/add`, quizData, requestOptions).toPromise();
  }

  public removeQuiz(interviewId: number) {
    const requestOptions : Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<any>(`${this.defaultURI}api/quiz/` + interviewId, requestOptions).toPromise();
  }

  public getQuizQuestions(quizId: number) {
    return this.httpClient.get<any>(`${this.defaultURI}api/quiz/questions/` + quizId).toPromise()
      .then((response: any[]) => {
        let questionList : any[] = [];

        response.forEach(element => {
          questionList.push(element);
        });

        return questionList;
      });
  }

}
