import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { Notification } from '../objects/Notification';
import { Proposal } from '../objects/Proposal';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public sendNotification(notification: any) {
    return this.httpClient.post<Notification>(`${this.defaultURI}api/notifications/send`, notification).toPromise();
  }
  
  public getNotifications(receiverId: string){
      return this.httpClient.get<Notification[]>(`${this.defaultURI}api/notifications/show/` + receiverId).toPromise()
      .then((response: Notification[]) => {
          let notificationList:Notification[] = [];
          
          response.forEach(notification => {
              notificationList.push(notification);
          });

          return notificationList;
      });
}

public removeNotification(notificationView: any){
    return this.httpClient.post<Notification>(`${this.defaultURI}api/notifications/remove`, notificationView).toPromise();
}

public GetNotificationDetails(notificationId: string, senderId: string){
    return this.httpClient.get<Proposal>(`${this.defaultURI}api/notifications/notificationDetails/` + notificationId + "/" + senderId).toPromise();
}

public ChangeNotificationStatus(userId: string, value: boolean) {
    return this.httpClient.post<any>(`${this.defaultURI}api/user/notifications/` + userId + "/" + value, null).toPromise();
}
}
