import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { HiringProcess } from '../objects/HiringProcess';
import { Interview } from '../objects/Interview';

@Injectable({
  providedIn: 'root'
})
export class InterviewService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getStoredInterviews()
    {
        return this.httpClient.get<Interview[]>(`${this.defaultURI}api/interviews`).toPromise()
            .then(async response => {
                let interviewList: Interview[] = [];

                response.forEach(interview => {
                    interviewList.push(interview);
                });

                return interviewList;
            });
    }

    public getStoreHiringProcesses(){
        return this.httpClient.get<HiringProcess[]>(`${this.defaultURI}api/interviews/hirings`).toPromise()
            .then((response: HiringProcess[]) => {
                let hiringsList: HiringProcess[] = [];

                response.forEach(hiringProcess => {
                    hiringsList.push(hiringProcess);
                });

                return hiringsList;
            });
    }

    public scheduleNewInterview(data: any){
        return this.httpClient.post<Interview>(`${this.defaultURI}api/interviews/schedule`, data).toPromise();
    }

    public removeScheduledInterview(deleteId: any){
        const requestOptions: Object = {
            responseType: 'text'
        };
        
        return this.httpClient.delete<Interview>(`${this.defaultURI}api/interviews/` + deleteId, requestOptions).toPromise();
    }

    public updateInterviewDetails(updateId, interview: Interview){
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.put<Interview>(`${this.defaultURI}api/interviews/update/` + updateId, interview, requestOptions).toPromise();
    }

    public allocateParticipantInterview(interviewParticipantData: any, senderId: string){
        const requestOptions: Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/interviews/addParticipant/` + senderId, interviewParticipantData, requestOptions).toPromise();
    }

    public getAllAvailableEmployees(){
        return this.httpClient.get<any>(`${this.defaultURI}api/interviews/employees`).toPromise()
            .then((response: any) => {
                let employeesList : any[] = [];
    
                response.forEach(employee => {
                    employeesList.push(employee);
                });

                return employeesList;
            });
    }

    public getAllAvailableParticipantsByInterview(interviewId: number){
        return this.httpClient.get<any>(`${this.defaultURI}api/interviews/participants/` + interviewId).toPromise()
            .then((response: any[]) => {
                let participantsList : any[] = [];

                response.forEach(participant => {
                    participantsList.push(participant);
                });

                return participantsList;
            });
    }

    public removeParticipantFromInterview(interviewId: number, participantId: string){
        return this.httpClient.delete<any>(`${this.defaultURI}api/interviews/participants/remove/` + interviewId + "/" + participantId).toPromise();
    }

    public exportAllAvailableInterviews(){
        const requiredOptions: Object = {
            responseType: 'arraybuffer'
        };

        return this.httpClient.get<any>(`${this.defaultURI}api/interviews/export`, requiredOptions).toPromise();
    }

    public postQuestionDynamically(questionContent: any) {
        const requiredOptions : Object = {
            responseType: 'text'
        };

        return this.httpClient.post<any>(`${this.defaultURI}api/quiz/postQuestion`, questionContent, requiredOptions).toPromise();
    }

    public customSearchFilterByCandidate(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/interviews/search/interviews/candidate`, key).toPromise();
    }

    public customSearchFilterByStatus(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/interviews/search/interviews/status`, key).toPromise();
    }

    public customSearchFilterByDate(key: any) {
        return this.httpClient.post<any>(`${this.defaultURI}api/interviews/search/interviews/date`, key).toPromise();
    }

}
