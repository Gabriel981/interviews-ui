import { PositionService } from "../../position.service";

export class PositionServiceMock {
    positionService = jasmine
        .createSpyObj<PositionService>('PositionService', [
            'updateVacantPositions',
            'getCurrentPositions',
            'updatePositionDetails',
            'removePosition',
            'createNewPosition'
        ]);

    getMockedObject(): jasmine.SpyObj<PositionService> {
        return this.positionService;
    }
}