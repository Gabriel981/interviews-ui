import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { JwtInterceptor } from "src/app/guards/JwtInterceptor";
import { AccountService } from "../account.service";
import { PositionService } from "../position.service";
import { SubmissionService } from "../submission.service";

describe('Submission service tests - Mocks', () => {
    fdescribe('Submission service essentials - tests', () => {
        let routerObject: Router;
        let submissionService: any;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            submissionService = jasmine.createSpyObj('SubmissionService', ['getAllSubmissions', 'getAllCandidates', 'getAllPositions', 'uploadSubmission',
                'updateSubmissionDetails', 'removeSubmission', 'getUserSubmissions','getCurrentSubmissionCV', 'getSubmissionsDataChart', 'requestCandidateRemoval']);
            
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });

        it('should create submission service', () => {
            expect(submissionService).toBeDefined();
            expect(submissionService).toBeTruthy();
        });
    });
    
    fdescribe('GET actions on submissions', () => {
        let routerObject: Router;
        let submissionServiceSpy: any;

        let userId: string = 'ab334czcaesdggae232';

        let candidates: any[] = [
            {id: 1, firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
            {id: 2, firstName: 'Candidate', lastName: 'Candidate 2', 'dateOfBirth': new Date('15/06/1993'), address: 'Street 2', email: 'candidate.candidate2@mail.com', 
                phoneNumber: '07828187314', positionId: 2, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 2', answer: 'Security answer 2', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
            {id: 3, firstName: 'Candidate', lastName: 'Candidate 3', 'dateOfBirth': new Date('15/06/1994'), address: 'Street 3', email: 'candidate.candidate3@mail.com', 
                phoneNumber: '07828187315', positionId: 3, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 3', answer: 'Security answer 3', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false}
        ];

        let positions: any[] = [
            {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true},
            {positionId: 2, positionName: 'Position name 2', department: {departmentId: 2, departmentName: 'Department 2', description:'Department description 2', active: true,
                currentNumberOfPositions: 2}, description: 'Position description 2', noVacantPositions: 4, availableForRecruiting: true},
            {positionId: 3, positionName: 'Position name 3', department: {departmentId: 3, departmentName: 'Department 3', description:'Department description 3', active: true,
                currentNumberOfPositions: 1}, description: 'Position description 3', noVacantPositions: 3, availableForRecruiting: true}
        ];

        let submissions: any[] = [
            {submissionId: 1, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                    phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                        securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'},
            {submissionId: 2, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                    phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                        securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'}
        ];

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            submissionServiceSpy = jasmine.createSpyObj('SubmissionService', ['getAllSubmissions',
                'getAllCandidates', 'getAllPositions', 'getUserSubmissions']);

            submissionServiceSpy.getAllSubmissions.and.returnValue(of(submissions));
        
            submissionServiceSpy.getAllCandidates.and.returnValue(of(candidates));

            submissionServiceSpy.getAllPositions.and.returnValue(of(positions));

            submissionServiceSpy.getUserSubmissions.withArgs(userId).and.returnValue(of(submissions.find(x => x.user.id == userId)));
        
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        }); 

        it('should return all submissions', (done) => {
            submissionServiceSpy.getAllSubmissions().subscribe(async response => {
                expect(response).toBeDefined();
                expect(submissionServiceSpy.getAllSubmissions).toHaveBeenCalled();
                expect(response.length).toBeGreaterThanOrEqual(2);
                done();
            });
        });

        it('should return all candidates', (done) => {
            submissionServiceSpy.getAllCandidates().subscribe(async response => {
                expect(response).toBeDefined();
                expect(submissionServiceSpy.getAllCandidates).toHaveBeenCalled();
                expect(response.length).toBeGreaterThanOrEqual(2);
                done();
            });
        });

        it('should return all positions', (done) => {
            submissionServiceSpy.getAllPositions().subscribe(async response => {
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(2);
                done();
            });
        });

        it('should return all user submissions', (done) => {
            let userId = "ab334czcaesdggae232";
            submissionServiceSpy.getUserSubmissions(userId).subscribe(async response => {
                expect(response).toBeDefined();
                console.log(response);
                done();
            });
        });
    });

    fdescribe('POST actions on submissions', () => {
        let submissionServiceSpy: any;
        let routerObject: Router;

        let values : any= {
            candidateId: 'ab334czcaesdggae232',
            positionId: 1,
            mentions: 'Submission mentions'
        };

        var fastInterviewData = {
            submissionId: 1,
            createdById: 'ab334czcaesdggae232'
        };

        let submissionId: number = 1;

        var requestRemovalDetails = {
            emailSender: 'manager.manager@mail.com',
            emailCandidate: 'candidate.candidate1@mail.com'
        };

        beforeEach(() => {
            submissionServiceSpy = jasmine.createSpyObj('SubmissionService', ['uploadSubmission', 'scheduleFastInterview',
                 'changeCandidateStatus', 'requestCandidateRemoval']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            submissionServiceSpy.uploadSubmission.withArgs(values).and.returnValue(of('The submission was succesfully registered in database'));
            submissionServiceSpy.scheduleFastInterview.withArgs(fastInterviewData).and.returnValue('The interview was sucesfully created and added in the database');
            submissionServiceSpy.changeCandidateStatus.withArgs(submissionId).and.returnValue('Succesfully updated candidate status');
            submissionServiceSpy.requestCandidateRemoval.withArgs(requestRemovalDetails).and.returnValue('Succesfully sent the request to administrators');


            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provider: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provider: Router,
                        useValue: routerObject
                    }
                ]
            });
        });
        
        it('should upload submission', (done) => {
            let values : any= {
                candidateId: 'ab334czcaesdggae232',
                positionId: 1,
                mentions: 'Submission mentions'
            };

            submissionServiceSpy.uploadSubmission(values);

            submissionServiceSpy.uploadSubmission(values).subscribe(async response => {
                expect(response).toBeDefined();
                expect(submissionServiceSpy.uploadSubmission).toHaveBeenCalled();
                expect(response).toContain('The submission was succesfully registered');
                done();
            });
        });

        it('should schedule fast interview', (done) => {
            var fastInterviewData: any = {
                submissionId: 1,
                createdById: 'ab334czcaesdggae232'
            };

            submissionServiceSpy.scheduleFastInterview(fastInterviewData);
            expect(submissionServiceSpy.scheduleFastInterview).toHaveBeenCalled();
            expect(submissionServiceSpy.scheduleFastInterview(fastInterviewData)).toContain('The interview was sucesfully created');
            done();
        });

        it('should succesfully change candidate status', (done) => {
            let submissionId: number = 1;
            
            submissionServiceSpy.changeCandidateStatus(submissionId);
            expect(submissionServiceSpy.changeCandidateStatus).toHaveBeenCalled();
            expect(submissionServiceSpy.changeCandidateStatus(submissionId)).toEqual('Succesfully updated candidate status');
            done();
        });

        it('should succesfully sent candidate removal request', (done) => {
            var requestRemovalDetails = {
                emailSender: 'manager.manager@mail.com',
                emailCandidate: 'candidate.candidate1@mail.com'
            };

            submissionServiceSpy.requestCandidateRemoval(requestRemovalDetails);
            expect(submissionServiceSpy.requestCandidateRemoval).toHaveBeenCalled();
            expect(submissionServiceSpy.requestCandidateRemoval(requestRemovalDetails)).toEqual('Succesfully sent the request to administrators');
            done();
        });

    });

    fdescribe('UPDATE actions on submissions', () => {
        let submissionServiceSpy: any;
        let routerObject: Router;

        var submissionDetails = {
            mentions: 'Submission mentions after update'
        };

        beforeEach(async () => {
            submissionServiceSpy = jasmine.createSpyObj('SubmissionService', ['updateSubmissionDetails']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            submissionServiceSpy.updateSubmissionDetails.withArgs(submissionDetails).and.returnValue('Succesfully updated submissions details');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provider: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provider: Router,
                        useValue: routerObject
                    }
                ]
            });

            it('should update submission details', (done) => {
                var submissionDetails = {
                    mentions: 'Submission mentions after update'
                };

                submissionServiceSpy.updateSubmissionDetails(submissionDetails);
                expect(submissionServiceSpy.updateSubmissionDetails).toHaveBeenCalled();
                expect(submissionServiceSpy.updateSubmissionDetails(submissionDetails)).toEqual('Succesfully updated submissions details');
                done();
            });

        });
    }); 

    fdescribe('DELETE actions on submissions', () => {
        let submissionServiceSpy: any;
        let routerObject: Router;

        let deleteId: number = 1;

        beforeEach(async () => {
            submissionServiceSpy = jasmine.createSpyObj('SubmissionService', ['removeSubmission']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            submissionServiceSpy.removeSubmission.withArgs(deleteId).and.returnValue('The submission was sucesfully removed from the database');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provider: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provider: Router,
                        useValue: routerObject
                    }
                ]
            });
        });

        it('should remove submission', (done) => {
            let deleteId: number = 1;
            submissionServiceSpy.removeSubmission(deleteId);
            expect(submissionServiceSpy.removeSubmission).toHaveBeenCalled();
            expect(submissionServiceSpy.removeSubmission(deleteId)).toEqual('The submission was sucesfully removed from the database');
            done();
        });
    });
}); 

describe('Submission service tests - Injection', () => {

    
    fdescribe('Submission service essentials - tests', () => {
        let submissionService: SubmissionService;
        let routerObject: Router;
    
        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
    
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
    
            submissionService = TestBed.inject(SubmissionService);
        });

        it('should create submission service', () => {
            expect(submissionService).toBeDefined();
            expect(submissionService).toBeTruthy();
        });
    });

    fdescribe('GET actions on submissions', () => {
        let submissionService: SubmissionService;
        let routerObject: Router;
    
        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
    
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
    
            submissionService = TestBed.inject(SubmissionService);
        });

        it('should get all submissions', (done) => {
            submissionService.getAllSubmissions().then(async response => {
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(1);
                done();
            });
        });

        it('should get all candidates from submission service', (done) => {
            submissionService.getAllCandidates().then(async response => {
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(2);
                done();
            });
        });

        it('should get all positions from submission service', (done) => {
            submissionService.getAllPositions().then(async response => {
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(2);
                done();
            });
        });

        // it('should get user submissions', (done) => {
        //     let candidateId: string = '';
        //     submissionService.getAllCandidates().then(async response => {
        //         expect(response).toBeDefined();
        //         expect(response.length).toBeGreaterThanOrEqual(2);
        //         candidateId =
        //     });
        // });
    });
    
    fdescribe('POST actions on submissions', () => {
        let submissionService: SubmissionService;
        let positionService: PositionService;
        let accountService: AccountService;
        let routerObject: Router;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
            submissionService = TestBed.inject(SubmissionService);
            positionService = TestBed.inject(PositionService);
            accountService = TestBed.inject(AccountService);
        });

        it('should upload submission', (done) => {
            let candidateEmail: string = '';
            let positionId: number;

            submissionService.getAllCandidates().then(async response => {
                expect(response).toBeDefined();
                //expect(response.length).toBeGreaterThanOrEqual(2);
                
                candidateEmail = response[response.length - 1].email!;
            }).then(() => {
                positionService.getCurrentPositions().then(async response => {
                    positionId = response[response.length - 1].positionId!;
                }).then(() => {
                    let values = {
                        emailAddress: candidateEmail,
                        positionId: positionId,
                        mentions: 'Mentions test db submission'
                    };

                    submissionService.uploadSubmission(values).then(async response => {
                        expect(response).toBeDefined();
                        console.log(response);
                        expect(response).toEqual("The submission was succesfully registered in database");
                        done();
                    });
                });
            });
        });

        it('should request candidate removal', (done) => {
            let currentUserEmail: string = accountService.currentUserValue.email!;
            let destinationUserEmail: string = "";

            submissionService.getAllCandidates().then(async response => {
                destinationUserEmail = response[response.length - 1].email!;
            }).then(() => {
                let values = {
                    emailSender: currentUserEmail,
                    emailCandidate: destinationUserEmail
                };

                submissionService.requestCandidateRemoval(values).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toEqual('Succesfully sent the request to administrators');
                    done();
                });
            });
        });

        it('should change candidate status', (done) => {
            let submissionId: number;
            submissionService.getAllSubmissions().then(async response => {
                expect(response).toBeDefined();
                submissionId = response[response.length - 1].submissionId!;
            }).then(() => {
                submissionService.changeCandidateStatus(submissionId).then(async response => {
                   expect(response).toBeDefined();
                   expect(response).toEqual('Succesfully updated candidate status'); 
                   done();
                });
            });
        });
    });

    describe('UPDATE actions on submissions', () => {
        let submissionService: SubmissionService;
        let routerObject: Router;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();

            submissionService = TestBed.inject(SubmissionService);
        });

        it('should update submission details', (done) => {
            let submissionId: number;

            submissionService.getAllSubmissions().then(async response => {
                expect(response).toBeDefined();
                submissionId = response[response.length - 1].submissionId!;
            }).then(() => {
                let updatedSubmissionDetails: any = {
                    mentions: 'Submission test updates'
                };
    
                submissionService.updateSubmissionDetails(updatedSubmissionDetails, submissionId).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toEqual('Succesfully updated submissions details');
                    done();
                });
            }); 
        });
    });

    fdescribe('DELETE actions on submissions', () => {
        let submissionService: SubmissionService;
        let routerObject: Router;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
            submissionService = TestBed.inject(SubmissionService);
        });
        
        it('should remove submission', (done) => {
            let submissionId: number;
            submissionService.getAllSubmissions().then(async response => {
                expect(response).toBeDefined();
                submissionId = response[response.length - 1].submissionId!;
            }).then(() => {
                submissionService.removeSubmission(submissionId).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toContain('The submission was sucesfully removed');
                    done();
                });
            });
        });
    });
    
});