import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { JwtInterceptor } from "src/app/guards/JwtInterceptor";
import { Department } from "src/app/objects/Department";
import { DepartmentService } from "../department.service";

describe('Department service tests - Mocks', () => {
    fdescribe('Department service essentials - test', () => {
        let departmentService: DepartmentService;
        let routerObject: Router;

        beforeEach(async() => {
            departmentService = jasmine.createSpyObj('DepartmentService', ['getDepartments']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports:[HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });
        
        it('should create department service', () => {
            expect(departmentService).toBeDefined();
            expect(departmentService).toBeTruthy();
        });
    });

    fdescribe('GET actions on Department service', () => {
        let departmentServiceSpy: any;
        let routerObject: Router;
        let departments: Department[] = [];

        beforeEach(async () => {
            departmentServiceSpy = jasmine.createSpyObj('DepartmentService', ['getDepartments']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);


            departmentServiceSpy.getDepartments.and.returnValue(of(
                [
                    {departmentId: 1, departmentName: 'Department 1', description: 'Description department 1', active: true, currentNumberOfPositions: 3},
                    {departmentId: 2, departmentName: 'Department 2', description: 'Description department 2', active: false, currentNumberOfPositions: 5},
                    {departmentId: 3, departmentName: 'Department 3', description: 'Description department 3', active: true, currentNumberOfPositions: 1},
                ]
            ));

            TestBed.configureTestingModule({
               imports: [HttpClientModule],
               providers: [
                   {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                   },
                   {
                       provide: Router,
                       useValue:routerObject
                   },
                   {
                       provide: DepartmentService,
                       useValue: departmentServiceSpy
                   }
               ] 
            }).compileComponents();
        });

        it('should get all departments', (done) => {
            departmentServiceSpy.getDepartments().subscribe(async response => {
                expect(response).toBeDefined();
                expect(departmentServiceSpy.getDepartments).toHaveBeenCalled();
                expect(response.length).toBeGreaterThanOrEqual(2);

                done();
            });
        });
    });

    fdescribe('POST actions on Departments service', () => {
        let departmentServiceSpy: any;
        let routerObject: Router;

        let departmentDetails = {
            departmentName: 'Department test',
            description: 'Department test description',
            active: true
        };

        beforeEach(async() => {
            departmentServiceSpy = jasmine.createSpyObj('DepartmentService', ['addDepartment']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            departmentServiceSpy.addDepartment.withArgs(departmentDetails).and.returnValue('The department was succesfully pushed to database');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });

        it('should add a new department', (done) => {
            let departmentDetails = {
                departmentName: 'Department test',
                description: 'Department test description',
                active: true
            };

            departmentServiceSpy.addDepartment(departmentDetails);
            expect(departmentServiceSpy.addDepartment).toHaveBeenCalled();
            expect(departmentServiceSpy.addDepartment(departmentDetails)).toContain('The department was succesfully pushed');
            done();
        });
    });

    fdescribe('UPDATE operations on departments', () => {
        let departmentServiceSpy: any;
        let routerObject: Router;

        let departmentUpdateDetails = {
            departmentName: 'Department test update',
            description: 'Department test description',
            active: false
        };

        let departmentId = 1;

        beforeEach(async() => {
            departmentServiceSpy = jasmine.createSpyObj('DepartmentService', ['updateDepartmentDetails']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            departmentServiceSpy.updateDepartmentDetails.withArgs(departmentId, departmentUpdateDetails)
                .and.returnValue('The department was succesfully updated');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClasS: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });

        it('should update department details', (done) => {
            let departmentUpdateDetails = {
                departmentName: 'Department test update',
                description: 'Department test description',
                active: false
            };
    
            let departmentId = 1;

            departmentServiceSpy.updateDepartmentDetails(departmentId, departmentUpdateDetails);
            expect(departmentServiceSpy.updateDepartmentDetails).toHaveBeenCalled();
            expect(departmentServiceSpy.updateDepartmentDetails(departmentId, departmentUpdateDetails)).toEqual('The department was succesfully updated');
            done();
        });
    });

    fdescribe('DELETE actions on departments', () => {
        let departmentServiceSpy: any;
        let routerObject: Router;

        let departmentId = 1;

        beforeEach(async () => {
            departmentServiceSpy = jasmine.createSpyObj('DepartmentService', ['removeDepartment']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            departmentServiceSpy.removeDepartment.withArgs(departmentId).and.returnValue('The department was succesfully removed from database');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provider: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provider: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });
        
        it('should remove department', (done) => {
            let departmentId = 1;
            departmentServiceSpy.removeDepartment(departmentId);
            expect(departmentServiceSpy.removeDepartment).toHaveBeenCalled();
            expect(departmentServiceSpy.removeDepartment(departmentId)).toEqual('The department was succesfully removed from database');
            done();
        });
    });
});

describe('Department service tests - Injection', () => {
    fdescribe('Department service essentials - tests', () => {
        let routerObject: Router;
        let departmentService: DepartmentService;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provider: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();

            departmentService = TestBed.inject(DepartmentService);
        });

        it('should create department service ', () => {
            expect(departmentService).toBeDefined();
            expect(departmentService).toBeTruthy();
        });
    });

    fdescribe('GET actions on departments', () => {
        let routerObject: Router;
        let departmentService: DepartmentService;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            departmentService = TestBed.inject(DepartmentService);
        });

        it('should get all departments', (done) => {
            departmentService.getDepartments().then(async response => {
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(1);
                done();
            });
        });
    });

    fdescribe('POST actions on departments', () => {
        let routerObject: Router;
        let departmentService: DepartmentService;
        let departments: Department[] = [];

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            departmentService = TestBed.inject(DepartmentService);
        });

        it('should add new department to database', (done) => {
            let departmentDetails = {
                departmentName: 'Department test',
                description: 'Department description test',
                available: false
            };

            let exists = false;

            departmentService.addDepartment(departmentDetails).then(async response => {
                expect(response).toBeDefined();
                expect(response).toEqual('The department was succesfully inserted in the database');
                done();
            }).then(() => {
                departmentService.getDepartments().then(async response => {
                    response.forEach(department => {
                        if(department.departmentName == departmentDetails.departmentName){
                            exists = true;
                        }
                    });
                    
                    expect(exists).toEqual(true);
                });
            });
        });
    });

    fdescribe('UPDATE actions on departments', () => {
        let departmentService: DepartmentService;
        let routerObject: Router;

        beforeEach(() => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            departmentService = TestBed.inject(DepartmentService);
        });

        it('should update department details', (done) => {
            let departmentUpdateDetails = {
                departmentName: 'Department update',
                description: 'Department update description',
                available: false
            };

            let departments: Department[] = [];
            let lastDepartment: Department;

            departmentService.getDepartments().then(async response => {
                departments = response;
                lastDepartment = departments[departments.length - 1];
            }).then(() => {
                departmentService.updateDepartmentDetails(lastDepartment.departmentId, departmentUpdateDetails).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toEqual('The department details was succesfully updated');
                    done();
                });
            });
        });
    });

    fdescribe('REMOVE actions on departments', () => {
        let departmentService: DepartmentService;
        let routerObject: Router;

        beforeEach(() => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();

            departmentService = TestBed.inject(DepartmentService);
        });

        it('should remove department', (done) => {
            let departments: Department[] = [];
            let department: Department;

            departmentService.getDepartments().then(async response => {
                departments = response;
                department = departments[departments.length- 1];
            }).then(() => {
                departmentService.removeDepartment(department.departmentId).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toEqual('The department was succesfully removed from the database');
                    done();
                });
            });
        });
    });
});

