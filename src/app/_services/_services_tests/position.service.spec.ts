import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { JwtInterceptor } from '../../guards/JwtInterceptor';
import { Position } from '../../objects/Position';
import { AccountService } from '../account.service';
import { DepartmentService } from '../department.service';
import { PositionService } from '../position.service';

describe("PositionService", () => {
  let service: PositionService;
  let accountService: AccountService;
  let departmentService: DepartmentService;

  let positions: Position[] = [];

  const router = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptor,
          multi: true
        },
        {
          provide: Router,
          useValue: router
        },
      ]
    });
    service = TestBed.inject(PositionService);
    accountService = TestBed.inject(AccountService);
    departmentService = TestBed.inject(DepartmentService);
  });

  it('should be created position service', () => {
    expect(service).toBeTruthy();
  });

  it('should be created account service', () => {
    expect(accountService).toBeTruthy();
  });

  it('should login into app', (done) => {
    let model = {
      emailAddress: "ionut.vasile@mail.com",
      password: "Pass@word1"
    };

    accountService.login(model).then(async response => {
      expect(response.email).toEqual(model.emailAddress);
      done();      
    });
  });

  it('should return at least one object', (done) => {
    let positions: Position[] = [];
    service.getCurrentPositions().then(response => {
      if(response != null) {
        response.forEach(position => {
          positions.push(position);
        });
      }
      expect(positions.length).toBeGreaterThanOrEqual(1);
      done();
    });
  });

  it('should update vacant positions', async (done) => {
    let vacantFormValuesTest = {
      positionName: "Programmer",
      departmentId: 1015,
      noVacantPositions: 3
    };

    let positionId = 1056;

    service.updateVacantPositions(vacantFormValuesTest, positionId).then(response => {
      expect(response).toEqual("The number of available for recruiting positions was succesfully updated in database");
      done();
    });
  });

  it('should create new position', async (done) => {
    var positionDetailsTest = {
      positionName: "Position test",
      departmentId: 1015,
      description: "Position description",
      noVacantPositions: 3
    };

    service.createNewPosition(positionDetailsTest).then(async response => {
      expect(response).toContain("Position was succesfully");
      done();
    });
  });

  it('should remove recently pushed position', async (done) => {
    var oldLengthValue = 0;
    service.getCurrentPositions().then(async response => {
      positions = response;
      oldLengthValue = response.length;
    }).then(() => {
      let positionId = positions[positions.length -1].positionId;
      service.removePosition(positionId!).then(async response => {
        expect(response).toContain("Selected position was");
        done();
      });
    });
  });
});
