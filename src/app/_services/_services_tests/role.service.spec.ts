import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { JwtInterceptor } from "src/app/guards/JwtInterceptor";
import { User } from "src/app/objects/User";
import { PositionService } from "../position.service";
import { RoleService } from "../role.service";
import { PositionServiceMock } from "./_mocks/position.service.mock";

describe('Role service tests - Mocks', () => {
    fdescribe('Role services essentials - test', () => {
        
        let routerObject: any;
        let roleService: RoleService;

        let positionServiceMock: PositionServiceMock;

        beforeEach(async() => {
            roleService = jasmine.createSpyObj('RoleService', ['getAllRoles', 'getAllRolesByUser', 'createNewRole', 'removeRole', 
                'assignUserToRole', 'getPositions', 'removeUserInRole']);
            positionServiceMock = new PositionServiceMock();
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
        
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    },
                    {
                        provide: PositionService,
                        useValue: positionServiceMock.getMockedObject()
                    }
                ]
            }).compileComponents();
        });

        it('should create role service', () => {
            expect(roleService).toBeDefined();
            expect(roleService).toBeTruthy();
        });

        it('should create positionService', () => {
            expect(positionServiceMock).toBeTruthy();
            expect(positionServiceMock).toBeDefined();
        });
    });

    fdescribe('GET actions on roles', () => {
        let roleServiceSpy : any;
        let routerObject: Router;
        let roles: any[] = [];

        beforeEach(() => {

            roleServiceSpy = jasmine.createSpyObj('RoleService', ['getAllRoles', 'getPositions', 'getAllRolesByUser']);
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            roleServiceSpy.getAllRoles.and.returnValue(of(
                [
                    {id: 'eaf10202sadab123', name: 'RoleTest', normalizedName: 'ROLETEST'},
                    {id: 'ai4i12010201391k1j321', name:'RoleTest2', normalizedName: 'ROLETEST2'}
                ]
            ));

            roleServiceSpy.getPositions.and.returnValue(of(
                [
                    { positionId: '1', positionName: 'Position 1', department: { departmentId: 1, departmentName: 'Department 1', description: 'Description 1', active: true, currentNumberOfPositions: 1},
                        description: 'Position description 1', noVacantPositions: 1, availableForRecruiting: true },
                    
                    { positionId: '2', positionName: 'Position 2', department: { departmentId: 2, departmentName: 'Department 2', description: 'Description 2', active: true, currentNumberOfPositions: 2},
                        description: 'Position description 2', noVacantPositions: 1, availableForRecruiting: true },
                ]
            ));

            roleServiceSpy.getAllRolesByUser.and.returnValue(of(
                [
                    {id: 'a939392kfaqad21', firstName: 'User firstname 1', lastName: 'User lastname 1', dateOfBirth: '05/03/1997', 
                        address: 'Str. Morilor, nr. 5', email: 'user.user1@mail.com', phoneNumber: '073992727182', positionId: 1, role: '', mentions: '', jwtToken: 'af302002138831jlk3u2l13oi', 'refreshToken': 'abs020182831',
                            'securityQuestion': 'Security question 1', answer: 'Security answer 1', profilePhotoUrl:'', enableNotifications: true, twoFactorEnabled: false, enabledTwoFactorGoogle: false },
                    {id: 'a939392kfaqad22', firstName: 'User firstname 2', lastName: 'User lastname 2', dateOfBirth: '05/03/1996', 
                        address: 'Str. Morilor, nr. 6', email: 'user.user2@mail.com', phoneNumber: '073992727183', positionId: 2, role: '', mentions: '', jwtToken: 'af302002138831jlk3u2l13oi', 'refreshToken': 'abs020182831',
                            'securityQuestion': 'Security question 1', answer: 'Security answer 1', profilePhotoUrl:'', enableNotifications: true, twoFactorEnabled: false, enabledTwoFactorGoogle: false },        
                ]
            ));
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    },
                    {
                        provide: RoleService,
                        useValue: roleServiceSpy
                    }
                ]
            });
        });

        it('should return all roles from api', (done) => {
            roleServiceSpy.getAllRoles().subscribe(async response => {
                expect(roleServiceSpy.getAllRoles).toHaveBeenCalled();
                expect(response.length).toBeGreaterThanOrEqual(1);
                done();
            });
        });

        it('should return all users by role id', (done) => {
            let mockedRoleTest: any;
            
            roleServiceSpy.getAllRoles().subscribe(async response => {
                roles = response;
                roles.forEach(role => {
                    if(role.name == 'RoleTest') {
                        mockedRoleTest = role;
                    }
                });

                roleServiceSpy.getAllRolesByUser(mockedRoleTest.id).subscribe(async response => {
                    expect(response.length).toBeGreaterThanOrEqual(1);
                    expect(response).toBeDefined();
                    done();
                });
            });
        });

        it('should return all positions from Role Service', (done) => {
            roleServiceSpy.getPositions().subscribe(async response => {
                expect(roleServiceSpy.getPositions).toHaveBeenCalled();
                expect(response).toBeDefined();
                expect(response.length).toBeGreaterThanOrEqual(1);
                done();
            });
        });
    });

    fdescribe('POST options on role', () => {
        //objects declarations
        let positionServiceMock: PositionServiceMock;
        let httpClient: any;
        let roleService: any;
        let routerObject: any;

        let createRoleArgument = 'Role test';

        let values = {
            userId: 'af030123kk321i1',
            roleId: 'o4o142ii31k2n33'
        };

        beforeEach(async () => {
            positionServiceMock = new PositionServiceMock();
            httpClient = jasmine.createSpyObj('HttpClient', ['get', 'post']); 
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            roleService = jasmine.createSpyObj('RoleService', ['createNewRole', 'assignUserToRole']);

            roleService.createNewRole.withArgs(createRoleArgument).and.returnValue('The role was succesfully pushed to database');

            roleService.assignUserToRole.withArgs(values).and.returnValue('The role was succesfully assigned to the user');

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    },
                    {
                        provide: PositionServiceMock,
                        useValue: positionServiceMock.getMockedObject()
                    }
                ]
            });
        });

        it('should create new role', (done) => {
            createRoleArgument = 'Role test';

            roleService.createNewRole(createRoleArgument);
            done();
            expect(roleService.createNewRole).toHaveBeenCalled();
            expect(roleService.createNewRole(createRoleArgument)).toContain('The role was succesfully pushed');
        });

        it('should assign role to a user', (done) => {
            let values = {
                userId: 'af030123kk321i1',
                roleId: 'o4o142ii31k2n33'
            };

            roleService.assignUserToRole(values);
            done();
            expect(roleService.assignUserToRole).toHaveBeenCalled();
            expect(roleService.assignUserToRole(values)).toContain('The role was succesfully assigned');
        });
    });

    fdescribe('DELETE operations on roles', () => {
        let routerObject: any;
        let roleService: any;

        let roleId: any = 1;
        let userId: any = 1;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            roleService = jasmine.createSpyObj('RoleService', ['removeRole', 'removeUserInRole']);    
            roleService.removeRole.withArgs(roleId).and.returnValue('The role was succesfully removed from database');
            roleService.removeUserInRole.withArgs(userId).and.returnValue('The user was succesfuly removed form role');

            TestBed.configureTestingModule({
                imports:[HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });
        });

        it('should remove the role from database', (done) => {
            let removedRoleId = 1;

            roleService.removeRole(removedRoleId);
            done();
            expect(roleService.removeRole).toHaveBeenCalled();
            expect(roleService.removeRole(removedRoleId)).toContain('The role was succesfully removed');
        });

        it('should remove user in role', (done) => {
            let removedRoleUserId = 1;

            roleService.removeUserInRole(removedRoleUserId);
            done();
            expect(roleService.removeUserInRole).toHaveBeenCalled();
            expect(roleService.removeUserInRole(removedRoleUserId)).toEqual('The user was succesfuly removed form role');
        });
    });
});

describe('Role services test - Injection', () => {
    fdescribe('Role services essentials - test with injection', () => {
        let routerObject: Router;
        let roleService: RoleService;
        let positionService: PositionService;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,

                    }
                ]
            }).compileComponents();

            roleService = TestBed.inject(RoleService);
            positionService = TestBed.inject(PositionService);
        });

        it('should create role service', () => {
            expect(roleService).toBeDefined();
            expect(roleService).toBeTruthy();
        });

        it('should create position service', () => {
            expect(positionService).toBeDefined();
            expect(positionService).toBeTruthy();
        });
    });

    fdescribe('GET actions on roles', () => {
        let routerObject: Router;
        let roleService: RoleService;
        let positionService: PositionService;

        //arrays
        let roles: any[] = [];
        let users: any[] = [];
        let positions: any[] = [];

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            roleService = TestBed.inject(RoleService);
            positionService = TestBed.inject(PositionService);
        });

        it('should return all available roles', (done) => {
            roleService.getAllRoles().then(async response => {
                roles = response;
                expect(response).toBeDefined();
                expect(roles.length).toBeGreaterThanOrEqual(1);
                done();
            });
        });

        it('should get all users by role', (done) => {
            let candidateRoleId = "0b3168c1-e8a2-4b15-ae16-b46b86b4b234";
            roleService.getAllRolesByUser(candidateRoleId).then(async response => {
                if(response != null) {
                    users = [];
                    users = response;
                    expect(response).toBeDefined();
                    expect(users.length).toBeGreaterThanOrEqual(1);
                    done();
                }
            });
        });

        it('should get all positions', (done) => {
            roleService.getPositions().then(async response => {
                if(response != null) {
                    positions = [];
                    positions = response;
                    expect(response).toBeDefined();
                    expect(positions.length).toBeGreaterThanOrEqual(1);
                    done();
                }
            });
        });
    });

    describe('POST actions on roles', () => {
        let routerObject: Router;
        let roleService: RoleService;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            roleService = TestBed.inject(RoleService);
        });

        it('should create new role - injections', (done) => {
            let roleName = 'Test role 2';
            roleService.createNewRole(roleName).then(async response => {
                console.log(response);
                expect(response).toBeDefined();
                expect(response).toContain("A new role was succesfully added");
                done();
            });
        });

        it('should succesfully assign a user to a role', (done) => {
            let values = {
                userId: "99968cc2-46d6-43a1-8f68-0b9ba5f69eba",
                roleId: "ee70d143-65c6-40a4-acc7-1d3c0512a6c4"
            };

            roleService.assignUserToRole(values).then(async response => {
                expect(response).toBeDefined();
                expect(response).toContain('The user was succesfully assigned');
                done();
            });
        });
    });

    describe('DELETE operations on roles', () => {
        let routerObject: Router;
        let roleService: RoleService;

        let roles: any[] = [];
        let testRoleId: string;

        let users: any[] = [];
        let choosedUser: User;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            });

            roleService = TestBed.inject(RoleService);
        });

        it('should remove the role specified', (done) => {
            roleService.getAllRoles().then(async response => {
                roles = response;
                done();
            }).then(() => {
                testRoleId = roles.find(x => x.name == 'Test role2').id;
                roleService.removeRole(testRoleId).then(async response => {
                    expect(response).toBeDefined();
                    expect(response).toContain('The role was succesfully removed');
                    done();
                });
            });
        });

        // it('should remove the user from role', (done) => {
        //     //should get all the user in role
        //     let employeeRoleId = '93d7ae69-a9f6-4643-adf5-acb51b29a140'; 
        //     roleService.getAllRolesByUser(employeeRoleId).then(async response => {
        //         users = response;
        //         done();
        //     }).then(() => {
        //         choosedUser = users[users.length - 1];
        //         roleService.removeUserInRole(choosedUser.id).then(async response => {
        //             expect(response).toBeDefined();
        //             expect(response).toContain('The user was succesfully removed');
        //             done();
        //         });
        //     });
        // });
    });
});