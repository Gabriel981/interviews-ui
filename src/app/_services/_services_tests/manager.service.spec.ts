import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { JwtInterceptor } from "@auth0/angular-jwt";
import { of } from "rxjs";
import { Submission } from "src/app/objects/Submission";
import { User } from "src/app/objects/User";

describe('Manager service testing - Mocks', () => {
    fdescribe('Manager service testing - essentials', () => {
        let managerServiceSpy: any;
        let routerObject: Router;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            managerServiceSpy = jasmine.createSpyObj('ManagerService', ['GetNotificationDetails', 'GetAllSubmissionsData',
                'ApproveCandidate', 'RejectCandidate', 'RequestCandidateRemoval', 'GetAllAvailableCandidates', 'GetAllAvailableInterviews', 'RequestInterviewChangeDetails',
                    'RequestInterviewRemovalRecruiter', 'RequestSpecialInterviewRemoval', 'GetAllParticipantsByInterviewId', 'GetSelectedCandidateCV']);

            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();
        });

        it('should create manager service', () => {
            expect(managerServiceSpy).toBeDefined();
            expect(managerServiceSpy).toBeTruthy();
        });
    });

    fdescribe('GET actions on manager service', () => {
        let routerObject: Router;
        let managerServiceSpy: any;

        let submissions: any[] = [
                {submissionId: 1, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                    phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                        securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'},
            {submissionId: 2, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                    phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                        securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'}
        ];

        let candidates: any[] = [
            {id: 1, firstName: 'Candidate', lastName: 'Candidate 1', 'dateOfBirth': new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
            {id: 2, firstName: 'Candidate', lastName: 'Candidate 2', 'dateOfBirth': new Date('15/06/1993'), address: 'Street 2', email: 'candidate.candidate2@mail.com', 
                phoneNumber: '07828187314', positionId: 2, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 2', answer: 'Security answer 2', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
            {id: 3, firstName: 'Candidate', lastName: 'Candidate 3', 'dateOfBirth': new Date('15/06/1994'), address: 'Street 3', email: 'candidate.candidate3@mail.com', 
                phoneNumber: '07828187315', positionId: 3, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                    securityQuestion: 'Security question 3', answer: 'Security answer 3', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false}
        ];

        let interviews: any[] = [
            { interviewId: 1, interviewDate: new Date(), communicationChannel: 'ONLINE', candidateSituation: 'ReadyForInterview', hiringProcess: { hiringId: 1, candidateSituation: 'ReadyForInterview', 
                submission: {submissionId: 1, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                    user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', dateOfBirth: new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                        phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                            securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'},
                        candidate: { id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate1', completeName: 'Candidate Candidate1', dateOfBirth: new Date('15/06/1992'), address: 'Street 1', 
                            email: 'candidate.candidate1@mail.com', phoneNumber: '072727273738', submissionDate: new Date(), candidateSituation: 'ReadyForInterview', positionName: 'Registered candidate'}, 
                                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                                currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}}},
            { interviewId: 1, interviewDate: new Date(), communicationChannel: 'ONLINE', candidateSituation: 'ReadyForInterview', hiringProcess: { hiringId: 1, candidateSituation: 'ReadyForInterview', 
                submission: {submissionId: 1, dateOfSubmission: new Date(), mentions: 'Mentions test 1', positionName: 'Position test 1', firstName: 'Ionel',
                    user: {id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate 1', dateOfBirth: new Date('15/06/1992'), address: 'Street 1', email: 'candidate.candidate1@mail.com', 
                        phoneNumber: '07828187313', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1io', refreshToken: '32ab3lku3lk21u0hlg31', 
                            securityQuestion: 'Security question 1', answer: 'Security answer 1', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                    currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}, candidateId: 'ad3j4oiuio14lhhlk41', status: 'Registered', cv: 'firebase.com/cv1'},
                        candidate: { id: 'ab334czcaesdggae232', firstName: 'Candidate', lastName: 'Candidate1', completeName: 'Candidate Candidate1', dateOfBirth: new Date('15/06/1992'), address: 'Street 1', 
                            email: 'candidate.candidate1@mail.com', phoneNumber: '072727273738', submissionDate: new Date(), candidateSituation: 'ReadyForInterview', positionName: 'Registered candidate'}, 
                                appliedPosition: {positionId: 1, positionName: 'Position name 1', department: {departmentId: 1, departmentName: 'Department 1', description:'Department description 1', active: true,
                                currentNumberOfPositions: 3}, description: 'Position description 1', noVacantPositions: 3, availableForRecruiting: true}}}
        ];

        let participantsByInterviewId: any[] =  [
            {id: 'ab334czcaesdggae235', firstName: 'Participant', lastName: 'Participant 1', dateOfBirth: new Date('15/06/1992'), address: 'Street 2', email: 'participant.participant1@mail.com', 
                phoneNumber: '07828187315', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1ieq', refreshToken: '32ab3lku3lk21u0hoeqw1', 
                    securityQuestion: 'Security question 2', answer: 'Security answer 2', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false},
            {id: 'ab334czcaesdggae235', firstName: 'Participant', lastName: 'Participant 2', dateOfBirth: new Date('15/06/1992'), address: 'Street 3', email: 'participant.participant2@mail.com', 
                phoneNumber: '07828187315', positionId: 1, mentions: 'Submission mentions', jwtToken: '92u213lyhoy3o1ieq', refreshToken: '32ab3lku3lk21u0hoeqw1', 
                    securityQuestion: 'Security question 3', answer: 'Security answer 3', enableNotifications: true, twoFactorEnabled: true, enabledToFactorGoogle: false}
        ];

        let interviewId: number = 1;

        beforeEach(async () => {
            routerObject = jasmine.createSpyObj('Router', ['navigate']);
            managerServiceSpy = jasmine.createSpyObj('ManagerService', ['GetNotificationDetails', 'GetAllSubmissions', 'GetAllAvailableCandidates',
                'GetAllAvailableInterviews', 'GetAllParticipantsByInterviewId']);

            managerServiceSpy.GetAllSubmissions.and.returnValue(submissions);
            managerServiceSpy.GetAllAvailableInterviews.and.returnValue(interviews);
            managerServiceSpy.GetAllAvailableCandidates.and.returnValue(candidates);
            managerServiceSpy.GetAllParticipantsByInterviewId.withArgs(interviewId).and.returnValue(participantsByInterviewId);
            
            TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                    },
                    {
                        provide: Router,
                        useValue: routerObject
                    }
                ]
            }).compileComponents();;
        });

        it('should get all submissions', (done) => {
            managerServiceSpy.GetAllSubmissions()
            expect(managerServiceSpy.GetAllSubmissions).toHaveBeenCalled();
            expect(managerServiceSpy.GetAllSubmissions().length).toBeGreaterThanOrEqual(2);
            done();       
        });

        it('should return all available interviews', (done) => {
            managerServiceSpy.GetAllAvailableInterviews();
            expect(managerServiceSpy.GetAllAvailableInterviews).toHaveBeenCalled();
            expect(managerServiceSpy.GetAllAvailableInterviews().length).toBeGreaterThanOrEqual(2);
            done();
        });
        
        it('should return all available candidates', (done) => {
            managerServiceSpy.GetAllAvailableCandidates();
            expect(managerServiceSpy.GetAllAvailableCandidates).toHaveBeenCalled();
            expect(managerServiceSpy.GetAllAvailableCandidates().length).toBeGreaterThanOrEqual(2);
            done();
        });

        it('should return all candidates by interview id', (done) =>{
            let interviewId = interviews[interviews.length - 1].interviewId;
            managerServiceSpy.GetAllParticipantsByInterviewId(interviewId);
            expect(managerServiceSpy.GetAllParticipantsByInterviewId).toHaveBeenCalled();
            expect(managerServiceSpy.GetAllParticipantsByInterviewId(interviewId).length).toBeGreaterThanOrEqual(2);
            done();
        })
    });     
});