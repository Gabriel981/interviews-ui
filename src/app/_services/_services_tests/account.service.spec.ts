import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { JwtInterceptor } from "src/app/guards/JwtInterceptor";
import { TwoFactorDto } from "src/app/objects/TwoFactorDto";
import { User } from "src/app/objects/User";
import { AccountService } from "../account.service";

fdescribe('Account service tests', () => {
    let accountService: AccountService;

    let currentAccount: User;

    const router = jasmine.createSpyObj('Router', ['navigate']);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [
                { provide : HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
                { provide: Router, useValue: router}
            ]
        });

        accountService = TestBed.inject(AccountService);
    });

    it('should inject account service', () => {
        expect(accountService).toBeTruthy();
    });

    it('should be logged in', (done) => {
        let model = {
            emailAddress: "ionut.vasile@mail.com",
            password: "Pass@word1"
        };

        accountService.login(model).then(async response => {
            expect(response.email).toEqual(model.emailAddress);
            done();
        });
    });

    // it('should be logged with 2-step Google', (done) => {
    //     let twoFactorDTOTesting: TwoFactorDto = {
    //         email: 'ionut.vasile@mail.com',
    //         provider: 'Google',
    //         token: 'to be filled in'
    //     };
    // });
});