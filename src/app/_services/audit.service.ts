import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { AuditLog } from '../objects/AuditLog';

@Injectable({
  providedIn: 'root'
})
export class AuditService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getAuditTrailLogs() {
    return this.httpClient.get<any>(`${this.defaultURI}api/logs`).toPromise()
      .then(async response => {
        let auditTrailLogs: AuditLog[] = [];

        response.forEach(audit => {
          auditTrailLogs.push(audit);
        });

        return auditTrailLogs;
      });
  }

  public exportAuditTrailData() {
    const requiredOptions: Object = {
      responseType: 'arraybuffer'
    };

    return this.httpClient.get<any>(`${this.defaultURI}api/logs/export`, requiredOptions).toPromise();
  }

  public getChartData() {
    return this.httpClient.get<any>(`${this.defaultURI}api/logs/chart`).toPromise()
      .then(async response => {
        let chartData: any[] = [];

        response.forEach(response => {
          chartData.push(response);
        });

        return chartData;
      });
  }
}
