import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { defaultVars } from 'src/environments/environment.prod';
import { Department } from '../objects/Department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getDepartments() {
    return this.httpClient.get<any>(`${this.defaultURI}api/departments`).toPromise()
      .then((response: Department[]) => {
        let retrievedList : Department[] = [];

        response.forEach(result => {
          retrievedList.push(result);
        });

        return retrievedList;
      });
  }

  public updateDepartmentDetails(departmentId: number, updateDetails: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.put<any>(`${this.defaultURI}api/departments/${departmentId}`, updateDetails, requestOptions).toPromise();
  }

  public removeDepartment(departmentId: number) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<any>(`${this.defaultURI}api/departments/${departmentId}`, requestOptions).toPromise();
  }

  public addDepartment(departmentDetails: any) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}api/departments/add`, departmentDetails, requestOptions).toPromise();
  }

  public sendExportRequest() {
    const requiredOptions: Object = {
      responseType: 'arraybuffer'
    };

    return this.httpClient.get<any>(`${this.defaultURI}api/departments/export`, requiredOptions).toPromise();
  }

}
