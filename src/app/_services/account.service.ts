import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { defaultVars } from 'src/environments/environment.prod';
import { AuthResponseDto } from '../objects/AuthResponseDto';
import { TwoFactorDto } from '../objects/TwoFactorDto';
import { User } from '../objects/User';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public defaultURI = defaultVars.defaultURI;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser : Observable<User>;
  public user!: User;
  public role!: string;
  public imageUrl!: string;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public login(model: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/auth/user/login`, model).toPromise().then(async response => {
      console.log(response.appUser);
      if(response.is2StepVerificationRequired && response.method == "Email") {
        this.router.navigate(['twostepverification'], { queryParams: { provider: response.provider, email: model.emailAddress }});
      }

      if(response.is2StepVerificationRequired && response.method == "Google") {
        this.imageUrl = response.imageUrl;
        sessionStorage.setItem('qrCode', this.imageUrl);
        sessionStorage.setItem('setupCode', response.manualEntrySetupCode);
        this.router.navigate(['twostepverificationgoogle'], {queryParams: { email: model.emailAddress }});
      }

      if(response.appUser && response.appUser.jwtToken) {
        this.user = response.appUser;
        console.log(response.role);
        this.user.role = response.role;
        
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        this.currentUserSubject.next(this.user);

        sessionStorage.setItem('userRole', this.user.role!);
      
        return response.appUser;
      }
    });
  }

  public twoStepLogin(data: any)  {
    return this.httpClient.post<any>(`${this.defaultURI}api/auth/twostepverification`, data).toPromise().then(async response => {
      console.log(response);
      if(response.appUser && response.appUser.jwtToken) {
        this.user = response.appUser;
        this.user.role = response.role;
      
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        this.currentUserSubject.next(this.user);

        sessionStorage.setItem('userRole', this.user.role!);
      
        return response.appUser;
      }
    });
  }

  public twoStepGoogleLogin(data: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/auth/googleverification`, data).toPromise().then(async response => {
      if(response.appUser && response.appUser.jwtToken) {
        this.user = response.appUser;
        this.user.role = response.role;
      
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        this.currentUserSubject.next(this.user);

        sessionStorage.setItem('userRole', this.user.role!);
      
        return response.appUser;
      }
    })
  }

  public get currentRoleValue(): string {
    return sessionStorage.getItem('userRole')!;
  }

  public get currentUserValue(): User {
    if(this.currentUserSubject.value == null) {
      this.user = {
        role : 'Anonymous'
      }

      return this.user;
    }
    return this.currentUserSubject.value;
  }

  public refreshUser() {
    return this.httpClient.get<any>(`${this.defaultURI}api/account/refresh`).toPromise().then(async response => {
      this.user = response;
      this.currentUserSubject.next(this.user);
      console.log(this.user);
      localStorage.removeItem('currentUser');
      localStorage.setItem('currentUser', JSON.stringify(this.user));
    });
  }

  public logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null!);
    this.router.navigate(['/']);
  }

  public refreshToken() : Observable<User>{
    let currentUser = JSON.parse(localStorage.getItem('currentUser')!);
    if(!currentUser) {
      this.logout();
    }

    let jwtToken = currentUser.jwtToken;
    let refreshToken = currentUser.refreshToken;

    return this.httpClient.post<any>(`${this.defaultURI}api/auth/user/refresh`, {jwtToken, refreshToken})
      .pipe(map(response => {
        if(response.appUser && response.appUser.jwtToken) {
          this.user = response.appUser;
          this.user.role = response.role;
          localStorage.setItem('currentUser', JSON.stringify(this.user));
        }

        return <User> response.appUser;
      }));
  }

  public getAuthToken() : string {
    let currentUser = JSON.parse(localStorage.getItem('currentUser')!);

    if(currentUser != null) {
      return currentUser.jwtToken
    }

    return '';
  }


//   private currentUserSource = new ReplaySubject<User>(1);
//   currentUser$ = this.currentUserSource.asObservable();

//   constructor(private httpClient: HttpClient) { }

//   public login(model: any) {
//     return this.httpClient.post(`${defaultVars.defaultURI}api/auth/user/login`, model).pipe(
//       map((response: User) => {
//         const user = response;
//         if(user) {
//           localStorage.setItem('jwt', JSON.stringify(user));
//           this.currentUserSource.next(user);
//         }
//       })
//     );
//   }

//   public setCurrentUser(user: User) {
//     this.currentUserSource.next(user);
//   }

//   public logout() {
//     localStorage.removeItem('jwt');
//     this.currentUserSource.next(null!);
//   }

//   public getCurrentUser() {
//     // return this.httpClient.get<User>(`${defaultVars.defaultURI}api/auth/currentUser`).toPromise();

//   }

  public removeCurrentAccount(model: any) {
    const requestOptions: Object = {
        responseType: 'text'
    };

    return this.httpClient.post<any>(`${defaultVars.defaultURI}api/auth/scheduleDelete`, model, requestOptions);
  }

  public resetChangePassword(email: string, token: string, newPassword: string, confirmPassword: string) {
    const requestOptions: Object = {
        responseType: 'text'
    };

    return this.httpClient.post(`${defaultVars.defaultURI}api/auth/reset/password`, {newPassword, confirmPassword, email, token}, requestOptions);
  }

  public getEmail(id: string) {
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.get<any>(`${this.defaultURI}api/auth/get_email/${id}`, requestOptions).toPromise();
  }

  public changeAccountSettings(details: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/account/settings`, details).toPromise();
  }
}
