import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { Position } from '../objects/Position';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  updateVacantPositions(position: Position, positionId: number){
    const requestOptions : Object = {
        responseType: 'text'
    };

    return this.httpClient.put<any>(`${this.defaultURI}api/positions/vacant/` + positionId, position, requestOptions)
        .toPromise();        
  }

  getCurrentPositions(){
      return this.httpClient.get<Position[]>(`${this.defaultURI}api/positions`).toPromise()
          .then((response: Position[]) => {
              let positionList: Position[] = [];

              response.forEach(position => {
                  positionList.push(position);
              })

              return positionList;
          });
  }

  updatePositionDetails(position: Position, positionId: number){
      const requestOptions: Object = {
          responseType: 'text'
      };

      return this.httpClient.put<any>(`${this.defaultURI}api/positions/` + positionId, position, requestOptions)
          .toPromise();
  }

  removePosition(positionId: number){
    const requestOptions : Object = {
      responseType: 'text'
    };

    return this.httpClient.delete<Position>(`${this.defaultURI}api/positions/` + positionId, requestOptions).toPromise();
  }

  createNewPosition(position: Position){
    const requestOptions: Object = {
      responseType: 'text'
    };

    return this.httpClient.post<any>(`${this.defaultURI}api/positions`, position, requestOptions).toPromise();
  }

  public exportData() {
    const requiredOptions: Object = {
      responseType: 'arraybuffer'
    };
    
    return this.httpClient.get<any>(`${this.defaultURI}api/positions/export`, requiredOptions).toPromise();
  }

  public customSearchByPosition(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/positions/filter/positions/name`, key).toPromise();
  }

  public customSearchByDepartment(key: any) {
    return this.httpClient.post<any>(`${this.defaultURI}api/positions/filter/positions/department`, key).toPromise();
  }

}
