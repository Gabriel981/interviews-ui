import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public addAnswer(answerContent: any) {
    const requiredOptions : Object = {
      responseType: 'text'
    };
    
    return this.httpClient.post<any>(`${this.defaultURI}api/answers/post/answers`, answerContent, requiredOptions).toPromise();
  }

  public getAllAnswers() {
    return this.httpClient.get<any>(`${this.defaultURI}api/answers`).toPromise()
      .then((response: any[]) => {
        let answersList : any[] = [];

        response.forEach((answer) => {
          answersList.push(answer);
        });

        return answersList;
      });
  }
  
}
