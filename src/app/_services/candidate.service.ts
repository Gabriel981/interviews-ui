import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defaultVars } from 'src/environments/environment.prod';
import { Candidate } from '../objects/Candidate';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  private defaultURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) { }

  public getAllCandidates(){
    return this.httpClient.get<Candidate[]>(`${this.defaultURI}candidates`).toPromise();
  }

  public submitCandidateDetails(values: any){
      return this.httpClient.post<Candidate>(`${this.defaultURI}candidates/`, values).toPromise();
  }

  public removeCandidate(deleteId: number){
      return this.httpClient.delete<Candidate>(`${this.defaultURI}candidates/` + deleteId).toPromise(); 
  }
}
