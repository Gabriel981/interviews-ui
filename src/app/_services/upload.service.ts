import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { defaultVars } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private defaultURI = defaultVars.defaultURI + "api/candidate/available/";
  private uploadURI = this.defaultURI + "upload";

  private baseURI = defaultVars.defaultURI;

  constructor(private httpClient: HttpClient) {}

  public uploadFile(file: Blob, email: string): Observable<HttpEvent<void>> {
    let customHeaders2 = new HttpHeaders();
    customHeaders2.set('Content-Type', 'multipart/form-data');

    const formData = new FormData();
    formData.append('file', file);
    formData.append('email', email);

    return this.httpClient.post<any>(this.uploadURI, formData, {headers: customHeaders2});
}

public uploadProfilePhoto(file: Blob, email: string): Observable<HttpEvent<void>> {
    let customHeaders = new HttpHeaders();
    customHeaders.append('Content-Type', 'multipart/form-data');
    
    const formData = new FormData();
    formData.append('file', file);
    formData.append('email', email);

    return this.httpClient.post<any>(`${this.baseURI}api/user/upload`, formData, {headers: customHeaders});
}

}
