import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Candidate } from '../objects/Candidate';
import { HiringProcess } from '../objects/HiringProcess';
import { Position } from '../objects/Position';
import { Submission } from '../objects/Submission';
import { HiringServices } from '../services/hiring.service';

@Component({
  selector: 'app-hiring-process',
  templateUrl: './hiring-process.component.html',
  styleUrls: ['./hiring-process.component.css']
})
export class HiringProcessComponent implements OnInit {

  public hirings!: HiringProcess[];

  private hiringService: HiringServices = new HiringServices(this.httpClient);
  public hiringDetails!: FormGroup;

  constructor(
    private httpClient: HttpClient,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getAllHiringProcesses();
  }

  public getAllHiringProcesses(){
    this.hiringService.getAllHiringProcesses().then((response) => {
      this.hirings = response;
    });
  }

  public openDetails(targetModal: any, hiringProcess: HiringProcess){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.initializeFormGroupDetails(hiringProcess);
  }

  private initializeFormGroupDetails(hiringProcess: HiringProcess){
    let candidate_name = hiringProcess.candidate?.firstName + ' ' + hiringProcess.candidate?.lastName;
    let position_name = hiringProcess.appliedPosition?.positionName;
    let created_at = new DatePipe('en-US');
    let create_at_formatted = created_at.transform(hiringProcess.submission?.dateOfSubmission, 'dd/MM/yyyy hh:mm:ss');
    let candidate_situation = hiringProcess.candidateSituation;
    let interview_scheduled_date = "No interview scheduled yet";

    this.hiringDetails = new FormGroup({
      hiring_candidate_name: new FormControl(candidate_name),
      hiring_position_name: new FormControl(position_name),
      hiring_created_at: new FormControl(create_at_formatted),
      hiring_candidate_situation: new FormControl(candidate_situation),
      hiring_interview_scheduled_date: new FormControl(interview_scheduled_date)
    });
  }

}
