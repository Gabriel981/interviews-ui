import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { defaultVars } from 'src/environments/environment';
import { AuthenticationService } from '../guards/AuthenticationService';
import { User } from '../objects/User';
import { TransferService } from '../services/transfer.service';
import { AccountService } from '../_services/account.service';
import { SubmissionService } from '../_services/submission.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

  public obtainedUser!: User;

  public numberOfSubmissionSent!: number;

  // private submissionService: SubmissionService = new SubmissionService(this.httpClient, this.authenticationService);

  constructor(
    private toastr: ToastrService,
    private httpClient: HttpClient,
    private router: Router,
    private transferService: TransferService,
    private accountService: AccountService,
    private submissionService: SubmissionService) {
    this.obtainCurrentUser();
  }

  ngOnInit(): void {
  }

  public obtainCurrentUser(){

    // return this.httpClient.get<User>(`${defaultVars.defaultURI}api/auth/currentUser`).toPromise().then(response => {
    //   if(response != null){
    //     this.obtainedUser = response;
    //     this.GetUserSubmissions(response.id!);
    //   }
    // });

    this.obtainedUser = this.accountService.currentUserValue;
    this.GetUserSubmissions(this.obtainedUser.id!);
  }

  public get noOfSubmissions(){
    return this.numberOfSubmissionSent;
  }

  public transferDataToChild(){
    this.transferService.setData(this.obtainedUser);
    this.router.navigate(['/user/applications']);
  }

  public transferDataToSettings(){
    this.transferService.setData(this.obtainedUser);
    this.router.navigate(['user/settings']);
  }

  public GetUserSubmissions(currentUserId: string){
      if(this.obtainedUser.role == 'Candidate registered') {
        this.submissionService.getUserSubmissions(currentUserId).then(async response => {
          console.log(response);
          this.numberOfSubmissionSent = response.length;
        });
      }
  //   if(this.obtainedUser.role == "Candidate registered") {
  //     this.submissionService.getUserSubmissions(currentUserId).then((response) => {
  //       this.numberOfSubmissionSent = response.length;
  //     });
  //   }
  // }
  }
}
