import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { defaultVars } from 'src/environments/environment';
import { AuthenticationService } from '../guards/AuthenticationService';
import { Position } from '../objects/Position';
import { User } from '../objects/User';
import { ApplicationService } from '../services/application.service';
import { SubmissionService } from '../services/submission.service';
import { TransferService } from '../services/transfer.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../services/notification.service';
import { Notification } from '../objects/Notification';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-registered-submission',
  templateUrl: './registered-submission.component.html',
  styleUrls: ['./registered-submission.component.css']
})
export class RegisteredSubmissionComponent implements OnInit {

  public availablePositions!: Position[];
  private applicationService: ApplicationService = new ApplicationService(this.httpClient, this.authenticationService);
  // private transferService: TransferService = new TransferService(this.router);
  private submissionService: SubmissionService = new SubmissionService(this.httpClient, this.authenticationService);
  private notificationService: NotificationService = new NotificationService(this.httpClient, this.authenticationService);
  

  public data!: Position;
  public message!: string;
  public obtainedUser!: User;
  public notification!: Notification;

  constructor(
    private toastr: ToastrService, 
    private router: Router, 
    private httpClient: HttpClient, 
    private authenticationService: AuthenticationService, 
    private transferService: TransferService,
    private accountService: AccountService) {
    this.obtainCurrentUser();
  }

  ngOnInit(): void {
    this.GetAvailablePositions();
  }

  public get notificationArraySize(){
    return this.availablePositions.length;
  }

  public GetAvailablePositions(){
    this.applicationService.getAllAvailablePositions().then(response => {
      console.log(response);
      this.availablePositions = response;
    });
  }

  public SendData(position: Position){
    this.data = position;
    // console.log(this.data);

    this.transferService.setData(this.data);
    // console.log(this.data);
    this.router.navigate(["/applications/application_registration"]);
  }


  public obtainCurrentUser(){
    // var currentUser = this.authenticationService.currentUserValue;
    // let customHeaders = new HttpHeaders({ Authorization: "Bearer " + currentUser.jwtToken });

    // return this.httpClient.get<User>(`${defaultVars.defaultURI}api/auth/currentUser`, {headers: customHeaders}).toPromise().then(response => {
    //   if(response != null){
    //     console.log(response);
    //     this.obtainedUser = response;
    //   }
    // });

    this.obtainedUser = this.accountService.currentUserValue;
  }

  public sendSubmission(availablePosition: Position){
    var submissionData = {      
      "firstName": this.obtainedUser.firstName,
      "lastName": this.obtainedUser.lastName,
      "emailAddress": this.obtainedUser.email,
      "positionId": availablePosition.positionId
    };

    this.submissionService.uploadSubmission(submissionData).then(()=>{
      console.log('ajunge aici pentru notification');
      this.sendNotification(availablePosition.positionId!);
    });    
  }

  public showToaster(){
    this.toastr.success("Operation was a success!");
  }

  public sendNotification(positionId: number){
    console.log('ajunge la notification');
    console.log(this.obtainedUser.email);

    var notificationData = {
      firstName: this.obtainedUser.firstName,
      lastName: this.obtainedUser.lastName,
      email: this.obtainedUser.email,
      message: "test",
      positionId: positionId
    }

    this.notificationService.sendNotification(notificationData).then(() => {
      this.showToaster();
      this.router.navigate(["/"]);
    });
  }


}
