import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { defaultVars } from 'src/environments/environment';
import { Submission } from '../objects/Submission';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { ApplicationService } from '../_services/application.service';
import { SubmissionService } from '../_services/submission.service';
import { UploadService } from '../_services/upload.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  public profilePhotoUrl = "";

  public obtainedUser!: User;
  public profileDetailsForm!: FormGroup;
  public profilePasswordResetForm!: FormGroup;

  public submissions!: Submission[];
  
  public noCurrentApplications!: number;

  constructor(
    private toastr: ToastrService, 
    private accountService: AccountService,
    private submissionService: SubmissionService,
    private applicationService: ApplicationService,
    private uploadService: UploadService) {

    this.profileDetailsForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      emailAddress: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required])
    });

    this.profilePasswordResetForm = new FormGroup({
      actualPassword: new FormControl('',
      [
        Validators.required
      ]),
      newPassword: new FormControl('', 
      [
        Validators.required
      ]),
      confirmPassword: new FormControl('',
      [
        Validators.required
      ])
    });
    // this.getNumberOfCurrentApplications();

    this.obtainedUser = this.accountService.currentUserValue;
  }

  public get firstName(){
    return this.profileDetailsForm.get('firstName');
  }

  public get lastName(){
    return this.profileDetailsForm.get('lastName');
  }

  public get address(){
    return this.profileDetailsForm.get('address');
  }

  public get emailAddress(){
    return this.profileDetailsForm.get('emailAddress');
  }

  public get dateOfBirth(){
    return this.profileDetailsForm.get('dateOfBirth');
  }

  public get noOfCurrentApplications(){
    return this.noCurrentApplications;
  }

  ngOnInit(): void {
    // this.obtainCurrentUser();
    // this.populateInputs();

    this.populateInputs();
  }

  // public get usernameA() {
  //   if(this.obtainedUser.role != "Anonymous"){
  //     let customName = this.obtainedUser.firstName + " " + this.obtainedUser.lastName;
  //     return customName;
  //   } else {
  //     let customName = "Anonymous";
  //     return customName;
  //   }
  // }

  // public obtainCurrentUser(){
  //   this.accountService.getCurrentUser().then((response) => {
  //     if(response != null) {
  //       this.obtainedUser = response;
  //       this.populateInputs();
  //     }
  //   });
  // }

  public populateInputs(){
    this.profileDetailsForm.controls.firstName.setValue(this.obtainedUser.firstName);
    this.profileDetailsForm.controls.lastName.setValue(this.obtainedUser.lastName);
    this.profileDetailsForm.controls.address.setValue(this.obtainedUser.address);
    this.profileDetailsForm.controls.emailAddress.setValue(this.obtainedUser.email);
    this.profileDetailsForm.controls.dateOfBirth.setValue(this.obtainedUser.dateOfBirth);
    if(this.obtainedUser.profilePhotoURL == null){
      this.profilePhotoUrl = "../assets/images/user-profile-default.png";
    } else {
      this.profilePhotoUrl = this.obtainedUser.profilePhotoURL!;
    }
  }

  public getNumberOfCurrentApplications(){
    var submissions: Submission[] = [];
    this.submissionService.getAllSubmissions().then((response) => {
      this.submissions = response;
      console.log(this.obtainedUser);
      this.submissions.forEach(submission => {
        if(submission.candidateId == this.obtainedUser.id){
          submissions.push(submission);
        }
      });
    });

    if(submissions.length > 0){
      this.noCurrentApplications = submissions.length;
      console.log(this.noCurrentApplications);
    } else {
      this.noCurrentApplications = 0;
      console.log(this.noCurrentApplications);
    }
  }

  public updateCurrentDetails(){
    var updatedDetails = {
      firstName: this.profileDetailsForm.value.firstName,
      lastName: this.profileDetailsForm.value.lastName,
      address: this.profileDetailsForm.value.address,
      email: this.profileDetailsForm.value.emailAddress,
      dateOfBirth: this.profileDetailsForm.value.dateOfBirth
    };

    console.log(updatedDetails);

    var updateId = this.obtainedUser.id;
    this.applicationService.updateAvailableCandidate(updatedDetails, updateId!).then(()=>{
      this.toastr.success('Details updated successfully!');
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    });
  }

  public uploadProfilePhoto(event)
  {
    if(event.target.files.length > 0)
    {
      const file = event.target.files[0];
      const userEmailAddress = this.obtainedUser.email;

      this.uploadService.uploadProfilePhoto(file, userEmailAddress!).subscribe(data => {
        console.log('ajunge aici sau nu?');
        this.showToastSuccessUploadPhoto();

        setTimeout(() => {
          window.location.reload();
        }, 1500);
        
      }, error => {
        console.log(error);
      });
    } 
  }

  public openFileChoser(){
    let element: HTMLElement = document.getElementById('myFile') as HTMLElement;
    element.click();
  }

  public showToastSuccessUploadPhoto()
  {
    this.toastr.success("The photo was successfully uploaded and your profile picture was changed");
  }

  public resetPasswordChanged() {
    var emailAddress = this.obtainedUser.email;
    var token : any = this.obtainedUser.jwtToken;

    this.accountService.resetChangePassword(emailAddress!, token!, this.profilePasswordResetForm.value.newPassword, this.profilePasswordResetForm.value.confirmPassword).subscribe((response) => {
      this.toastr.success(`Server response: ${response}`);
    }, error => {
      console.log(error);
      this.toastr.error(`Error ${error.error}`);
    }, () => {
      window.location.reload();
    });
  }
}
