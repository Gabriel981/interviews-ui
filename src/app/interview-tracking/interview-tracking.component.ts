import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Interview } from '../objects/Interview';
import { InterviewParticipantView } from '../objects/InterviewParticipantView';
import { Submission } from '../objects/Submission';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { AdminService } from '../_services/admin.service';
import { ManagerService } from '../_services/manager.service';

@Component({
  selector: 'app-interview-tracking',
  templateUrl: './interview-tracking.component.html',
  styleUrls: ['./interview-tracking.component.css']
})
export class InterviewTrackingComponent implements OnInit {

  public availableInterviews!: Interview[];

  public availableParticipants!: InterviewParticipantView[];

  public interviewDetails!: FormGroup;
  public interviewRequestDetails!: FormGroup;
  public interviewRemovalRequest!: FormGroup;

  public currentRequester!: User;

  private interviewCreator!: User;

  private interviewCurrentId!: number;

  private currentInterview!: Interview;


  //paginator
  public pageLength: number = 0;
  public splicedData!: Interview[];
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  //another elements related to the new angular table
  public dataSource = new MatTableDataSource<Interview>();
  public timer = 1000;
  public timeoutVal = 1000;

  public displayedColumns: string[] = ['completeName', 'positionName', 'interviewDate', 'moreDetails', 'participants', 'removalRequest', 'changeRequest'];
  public displayedColumnsFilters: string[] = ['completeNameFilter', 'positionNameFilter', 'interviewDateFilter'];


  //date scheduler elements
  public fromDate!: any;
  public toDate!: any;

  @ViewChild(MatPaginator) paginator : MatPaginator | undefined;

  constructor(
    private toastr: ToastrService,
    private managerService: ManagerService,
    private accountService: AccountService,
    private adminService: AdminService,
    private modalService: NgbModal) {
      this.currentRequester = this.accountService.currentUserValue;
  }

  ngOnInit(): void {
    // this.getCurrentRequester();
    this.getAllAvailableInterviews();
  }

  public resetForm(){
    this.fromDate = undefined;
    this.toDate = undefined;

    this.getAllAvailableInterviews();
  }

  public applySubmissionDateFilter(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    let dataRangeStartValue = dateRangeStart.value.toString();
    let dataRangeEndValue = dateRangeEnd.value.toString();

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        startDate: dataRangeStartValue,
        lastDate: dataRangeEndValue
      };

      this.getSubmissionsByDateFilter(elements);
    }, this.timeoutVal);
  }

  public getSubmissionsByDateFilter(key: any) {
    if(key.startDate == "" && key.endDate == "") {
      this.getAllAvailableInterviews();
    } else {
      this.adminService.searchByInterviewDate(key).then((response) => {
        this.pageLength = response.length;
        this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
  
        this.dataSource.data = this.splicedData;
        this.pageSizeOptions = [1, response.length];
      }, error => {
        console.log(error.error);
        this.toastr.error(`Error: ${error.error}`);
      });
    }
  }

  public getAllAvailableInterviews(){
    this.managerService.GetAllAvailableInterviews().then((response) => {
      this.availableInterviews = response;

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public pageChangeEvent(event) {
    const offset = ((event.pageIndex +1) - 1) * event.pageSize;
    this.dataSource.data = this.availableInterviews.slice(offset).slice(0, event.pageSize);
  }

  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if(!sort.active || sort.direction == '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch(sort.active) {
        case 'completeName': return this.compare(a.candidateFirstName!, b.candidateFirstName!, isAsc);
        case 'positionName': return this.compare(a.positionName!, b.positionName!, isAsc);
        case 'interviewDate': return this.compare(a.interviewDate!.toString(), b.interviewDate!.toString(), isAsc);
        default: return 0;
      }
    });
  }

  public applyFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSearchResults(elements);
    }, this.timeoutVal);
  }

  public getSearchResults(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableInterviews();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.adminService.searchByKeyInterview(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public applyNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSubmissionsByNameFilter(elements);
    }, this.timeoutVal);
  }

  public getSubmissionsByNameFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableInterviews();
      } else {
        this.adminService.searchByNameInterview(key).then((response) =>{
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public applyPositionFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSubmissionByPositionNameFilter(elements);
    }, this.timeoutVal);
  }

  public getSubmissionByPositionNameFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableInterviews();
      } else {
        this.adminService.searchByPositionNameInterview(key).then((response) =>{
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ?-1: 1) * (isAsc ? 1: -1);
  }

  public openInterviewDetails(targetModal: any, interview: Interview){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    const candidateCompleteName = interview.candidateFirstName + " " + interview.candidateLastName;

    var notFormatedInterviewDate = new DatePipe('en-US');
    var formatedInterviewDate = notFormatedInterviewDate.transform(interview.interviewDate, 'dd/MM/yyyy HH:mm:ss');

    var notFormatedSubmissionDate = new DatePipe('en-US');
    var formatedSubmissionDate = notFormatedSubmissionDate.transform(interview.submission!.dateOfSubmission, 'dd/MM/yyyy HH:mm:ss');

    const creatorCompleteName = interview.createdBy?.firstName + " " + interview.createdBy?.lastName; 

    this.interviewDetails = new FormGroup({
      candidateCompleteNameInterview: new FormControl(candidateCompleteName),
      appliedPositionInterview: new FormControl(interview.positionName),
      candidateStatusInterview: new FormControl(interview.candidateSituation),
      scheduledDateInterview: new FormControl(formatedInterviewDate),
      submissionDateInterview: new FormControl(formatedSubmissionDate),
      interviewScheduledBy: new FormControl(creatorCompleteName)
    });
  }

  public openChangeRequestDetails(targetModal: any, interview: Interview){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.interviewCurrentId = interview.interviewId!;

    const creatorCompleteName = interview.createdBy?.firstName + " " + interview.createdBy?.lastName;

    

    this.interviewRequestDetails = new FormGroup({
      creatorCompleteNameRequest: new FormControl(creatorCompleteName),
      changeInterviewDetailsReasonRequest: new FormControl('', 
      [
        Validators.required,
        Validators.min(10),
        Validators.max(150)
      ])
    });

    this.currentInterview = interview;
    this.interviewCreator = interview.createdBy!;
  }

  public openRemovalRequestDetails(targetModal: any, interview: Interview){
    this.modalService.open(targetModal, {
      centered : true,
      backdrop: 'static',
      size : 'lg'
    });

    this.interviewCurrentId = interview.interviewId!;
    const creatorCompleteName = interview.candidateFirstName + " " + interview.candidateLastName;

    this.interviewRemovalRequest = new FormGroup({
      creatorCompleteNameRemovalRequest: new FormControl(creatorCompleteName),
      removalInterviewRequestReason: new FormControl('', 
      [
        Validators.required,
        Validators.min(10),
        Validators.max(10)
      ])
    });

    this.interviewCreator = interview.createdBy!;
  }

  public sendInterviewChangeRequest() {
    var interviewRequestDetails = {
      requesterId: this.currentRequester.id,
      reason: this.interviewRequestDetails.value.changeInterviewDetailsReasonRequest,
      receiverId: this.interviewCreator.id,
      interviewId: this.currentInterview.interviewId
    };

    this.managerService.RequestInterviewChangeDetails(interviewRequestDetails).then((response) => {
      this.showRequestSentSuccess();
      let element : HTMLElement = document.getElementById('contentRequestChangeDismissBtn') as HTMLElement;
      element.click();
    });
  }

  public sendInterviewRemovalRequestRecruiter() {
    var interviewRequestDetails = {
      requesterId: this.currentRequester.id,
      reason: this.interviewRemovalRequest.value.removalInterviewRequestReason,
      receiverId: this.interviewCreator.id,
      interviewId: this.interviewCurrentId
    };

    this.managerService.RequestInterviewRemovalRecruiter(interviewRequestDetails).then((response) => {
      this.showRemovalRequestSentSuccessRecruiter();
      let element: HTMLElement = document.getElementById('contentRemovalInterviewRequestBtn') as HTMLElement;
      element.click();
    });
  }

  public sendInterviewSpecialRemovalRequest() {
    var interviewDetails = {
      requesterId: this.currentRequester.id,
      receiverId: this.interviewCreator.id,
      interviewId: this.interviewCurrentId
    };

    this.managerService.RequestSpecialInterviewRemoval(interviewDetails).then((response) => {
      this.showRemovalRequestSentSuccessAdmin();
      let element: HTMLElement = document.getElementById('contentRemovalInterviewRequestBtn') as HTMLElement;
      element.click();
    });
  }

  public openParticipantsListView(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered : true,
      backdrop : 'static',
      size : 'lg'
    });

    this.interviewCurrentId = interview.interviewId!;

    this.getAvailableParticipantsByInterview();
  }

  public exportData(){
    this.managerService.ExporInterviewsDataExcel().then((response) => {
      this.downloadFile(response);
      this.showSuccessfullExportInterviewsToast();
    })
  }

  public downloadFile(data: any){
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, 'dd/MM/yyyy HH:mm:ss', 'en-US');

    let blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;'});
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Interviews ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  public getAvailableParticipantsByInterview(){
    this.managerService.GetAllParticipantsByInterviewId(this.interviewCurrentId).then((response) => {
      if(response != null){
        this.availableParticipants = response;
      }
    });
  }

  // public getCurrentRequester(){
  //   this.accountService.getCurrentUser().then((response) =>{
  //     this.currentRequester = response;
  //     this.getAllAvailableInterviews();
  //   });
  // }

  public showRequestSentSuccess(){
    this.toastr.success("Change details request successfully sent to the recruiter");
  }

  public showRemovalRequestSentSuccessRecruiter(){
    this.toastr.success("Removal request was successfully sent to the recruiter");
  }

  public showRemovalRequestSentSuccessAdmin(){
    this.toastr.success("Removal request was successfully sent to the administrators");
  }

  public showSuccessfullExportInterviewsToast(){
    this.toastr.success("The export was successfully completed. Check your download for file");
  }
}
