import { DatePipe, formatDate } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DxDataGridComponent } from 'devextreme-angular';
import { ToastrService } from 'ngx-toastr';
import { Employee } from '../objects/Employee';
import { Interview } from '../objects/Interview';
import { ParticipantView } from '../objects/ParticipantView';
import { Position } from '../objects/Position';
import { User } from '../objects/User';
import { FieldStateMatcher } from '../_helpers/state.matcher.helper';
import { AccountService } from '../_services/account.service';
import { AdminService } from '../_services/admin.service';
import { ApplicationService } from '../_services/application.service';
import { InterviewService } from '../_services/interview.service';
import { PositionService } from '../_services/position.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  public availableEmployees: User[] = [];
  public availablePositions: Position[] = [];

  public notRetrievedUsers: User[] = [];

  //paginator
  public pageLength: number = 0;
  public splicedData!: any[];
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort, {static: false}) sort: MatSort | undefined;

  public employeeEditForm!: FormGroup;
  public employeeContentDetailsFG!: FormGroup;

  public selectedValue = 'Manager';

  // private adminServices: ApplicationService = new ApplicationService(this.httpClient, this.authentificationService);  
  private employeeUpdateId!: string;
  private employeeRemoveId!: string;

  //new table
  public dataSource = new MatTableDataSource<User>();
  public timer = 1000;
  public timeoutVal = 1000;

  public displayedColumns: string[] = ['employeeName', 'occupiedPosition', 'moreDetails', 'updateEmployee', 'deleteEmployee'];
  public filterColumns: string[] = ['employeeNameFilter', 'employeeOccupiedPositionFilter'];


  //devextreme not retrieved users
  public isAvailableAllocation: boolean = false;
  public crtNotAllocatedUser!: User;
  public userAllocationForm!: FormGroup;
  public filteredPositions: Position[] = [];
  public isAvailableRejection: boolean = false;
  public userRejectionForm!: FormGroup;
  public selectedUsers: any[] = [];
  public isAddMultipleUsersAvailable: boolean = false;
  public assignMultipleFormGroup!: FormGroup;
  public resetUnretrievedPasswordFromGroup!: FormGroup;
  public isResetUnretrievedAvailable: boolean = false;
  public userOptions = "User options";

  @ViewChild('gridUnretrieved', { static: false }) unretrievedDataGrid!: DxDataGridComponent;

  //devextreme not retrieved users elements
  public allMode!: string;
  public checkBoxesMode!: string;

  //devextreme employee
  public isAvailableEmployeeUpdate: boolean = false;
  public isDeleteEmployeeAvailable: boolean = false;
  public employeeUpdateForm!: FormGroup;
  public crtEmployee!: User;
  public hide: boolean = true;
  public startBirthDate: Date = new Date(1930, 0, 1);
  public crtDate: Date = new Date();
  public endBirthDate: Date = new Date(this.crtDate.getFullYear() - 16, 0, 1);
  public isResetPasswordAvailable: boolean = false;
  public resetPasswordFormGroup!: FormGroup;
  public isRemoveEmployeesAvailable: boolean = false;
  public isAddUserRoleAvailable: boolean = false;
  public roleAddFormGroup!: FormGroup 
  public isRemoveUnretrievedAvailable: boolean = false;

  //devextreme participants - users
  public participants: ParticipantView[] = [];
  public updateParticipantFormGroup!: FormGroup;
  public isParticipantDetailsAvailable: boolean = false;
  public interviews: Interview[] = [];
  public crtParticipant!: ParticipantView;
  public isRemoveParticipationAvailable: boolean = false;
  public isAssignInterviewAvailable: boolean = false;
  public assingInterviewFormGroup!: FormGroup;
  public crtSelectedParticipant!: User;
  public crtSelectedInterview!: Interview;
  public removalAssigningFormGroup!: FormGroup;
  public isRemovalAssigningAvailable: boolean = false;
  public removeAssigningAllFormGroup!: FormGroup;
  public isRemoveParticipationAllAvailable: boolean = false;

  //state matchers
  public reasonMatcher = new FieldStateMatcher();
  public roleMatcher = new FieldStateMatcher();
  public multiplePositionMatcher = new FieldStateMatcher();
  public removalReasonMatcher = new FieldStateMatcher();
  public reasonAllMatcher = new FieldStateMatcher();

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private positionService: PositionService,
    private applicationService: ApplicationService,
    private adminService: AdminService,
    private interviewService: InterviewService,
    private accountService: AccountService,
    private toastr: ToastrService) {
    this.employeeEditForm = new FormGroup({
      employeeName: new FormControl('', [Validators.required]),
      positionName: new FormControl('', [Validators.required]),
      department: new FormControl('', []),
      position: new FormControl('', [Validators.required]),
      employeeAddress: new FormControl('', [Validators.required])
    });

    this.employeeEditForm = this.formBuilder.group({
      employeeName: [''],
      positionName: [''],
      department: [''],
      position: [''],
      employeeAddress: ['']
    });

    this.allMode = 'allPages';
    this.checkBoxesMode = 'onClick';
  }

  public get employeeName() {
    return this.employeeEditForm.get('employeeName');
  }

  public get positionName() {
    return this.employeeEditForm.get('positionName');
  }

  public get department() {
    return this.employeeEditForm.get('department');
  }

  public get position() {
    return this.employeeEditForm.get('position');
  }

  public get employeeAddress() {
    return this.employeeEditForm.get('employeeAddress');
  }

  ngOnInit(): void {
    this.getAllAvailableEmployees();
    this.getAllAvailablePositions();
    this.getAllNotRetrievedUsers();
    this.getFilteredPositions();
    this.getParticipants();
    this.getInterviews();
  }

  //sort methods
  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if(!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch(sort.active) {
        case 'employeeName': return this.compare(a.firstName, b.firstName, isAsc);
        case 'occupiedPosition': return this.compare(a.position.positionName, a.position.positionName, isAsc);
        default: return 0;
      }
    });
  }

  public compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  //filter methods
  public applyFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSearchResultsEmployees(elements);
    }, this.timeoutVal);
  }

  public getSearchResultsEmployees(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableEmployees();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.adminService.searchByKeyEmployee(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
      
    }
  }

  // custom filters
  public applyNameFilter(event: any) {
      let element = (<HTMLInputElement>event).value;

      window.clearTimeout(this.timer);
      this.timer = window.setTimeout(() => {
        let elements = {
          key: element
        };

        this.getCustomEmployeeName(elements);
      }, this.timeoutVal);
  }

  public getCustomEmployeeName(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableEmployees();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.adminService.customSearchByEmployeeName(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
      
    }
  }

  public applyPositionNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getCustomPositionName(elements);
    }, this.timeoutVal);
  }

  public getCustomPositionName(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.getAllAvailableEmployees();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.adminService.customSearchByPositionName(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
      
    }
  }
  

  public pageChangeEvent(event) {
    const offset = ((event.pageIndex + 1) - 1) * event.pageSize;
    this.splicedData = this.availableEmployees.slice(offset).slice(0, event.pageSize);
  }

  public getAllAvailableEmployees() {
    return this.applicationService.getAllAvailableEmployees().then(response => {
      let retrievedEmployees = response;

      this.availableEmployees = [];

      retrievedEmployees.forEach(response => {
        this.availableEmployees.push(response);
      });

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    });
  }

  public getAllNotRetrievedUsers() {
    return this.applicationService.getAllNotRetrievedUsers().then(async response => {
      this.notRetrievedUsers = response;
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public getAllAvailablePositions() {
    return this.positionService.getCurrentPositions().then(response => {
      let retrievedPositions = response;

      this.availablePositions = [];

      retrievedPositions.forEach(retrievedPosition => {
        if (retrievedPosition.positionName != "Candidate registered") {
          this.availablePositions.push(retrievedPosition);
        }
      });
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  openEdit(targetModal: any, employee: User) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.employeeUpdateId = employee.id!;

    var employeeCompleteName = employee.firstName + ' ' + employee.lastName;

    this.employeeEditForm.controls['employeeName'].setValue(employeeCompleteName);
    this.employeeEditForm.controls['positionName'].setValue(employee.position?.positionName);
    this.employeeEditForm.controls['department'].setValue(employee.position?.department);

    for (let index = 0; index < this.availablePositions.length; index++) {
      if (this.availablePositions[index].positionName == employee.position?.positionName) {
        this.employeeEditForm.controls['position'].setValue(this.availablePositions[index]);
      }
    }

    this.employeeEditForm.controls['employeeAddress'].setValue(employee.address);
  }

  onSave() {
    const userData = {
      positionId: this.employeeEditForm.value.position.positionId
    };

    this.applicationService.updateAvailableEmployee(userData, this.employeeUpdateId).then((response) => {
      this.toastr.success(`Server response: ${response}`);
      this.availablePositions = [];
      this.ngOnInit();
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  // Remove operation
  public openDelete(targetModal: any, employee: User) {
    this.employeeRemoveId = employee.id!;

    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  public openDetails(targetModal: any, employee: User) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    let employeeCompleteName = employee.firstName + " " + employee.lastName;
    let employeeBirthDate = employee.dateOfBirth;
    let employeeAddress = employee.address;
    let employeeEmail = employee.email;
    let employeeOcupiedPosition = employee.position?.positionName;

    let notFormattedDate = new DatePipe('en-US');
    let formattedDate = notFormattedDate.transform(employeeBirthDate, "dd/MM/yyyy HH:mm:ss");


    this.employeeContentDetailsFG = new FormGroup({
      employeeCompleteName: new FormControl(employeeCompleteName),
      employeeBirthDate: new FormControl(formattedDate),
      employeeAddress: new FormControl(employeeAddress),
      employeeEmail: new FormControl(employeeEmail),
      employeeOcupiedPosition: new FormControl(employeeOcupiedPosition)
    });
  }

  //devextreme - Not allocated users to positions functions

  //Open modals functions
  public openAllocationPopup = (event): void =>{{
    this.crtNotAllocatedUser = this.notRetrievedUsers[event.row.rowIndex];
    this.isAvailableAllocation = !this.isAvailableAllocation;

    let username = this.crtNotAllocatedUser.firstName + " " + this.crtNotAllocatedUser.lastName;

    this.userAllocationForm = new FormGroup({
      username: new FormControl(username),
      position: new FormControl('', [
        Validators.required
      ])
    });
  }}

  public openRejectionPopup = (event): void => {
    this.crtNotAllocatedUser = this.notRetrievedUsers[event.row.rowIndex];
    this.isAvailableRejection = !this.isAvailableRejection;

    let username = this.crtNotAllocatedUser.firstName + " " + this.crtNotAllocatedUser.lastName;
    let email  = this.crtNotAllocatedUser.email;
    let phoneNumber = this.crtNotAllocatedUser.phoneNumber;

    this.userRejectionForm = new FormGroup({
      username: new FormControl(username),
      email: new FormControl(email),
      phoneNumber: new FormControl(phoneNumber),
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public get username() {
    return this.userRejectionForm.get('username');
  }

  public get email() {
    return this.userRejectionForm.get('email');
  }

  public get phoneNumber() {
    return this.userRejectionForm.get('phoneNumber');
  }

  public get reason() {
    return this.userRejectionForm.get('reason');
  }


  //Close modals functions
  public closeAllocationPopup() {
    this.isAvailableAllocation = !this.isAvailableAllocation;
  }

  public closeRejectionPopup() {
    this.isAvailableRejection = !this.isAvailableRejection;
  }

  //Other operations
  public getFilteredPositions() {
    this.applicationService.getFilteredPositions().then(async response => {
      this.filteredPositions = response;
    }, error => {
      console.log(`Error ${error.erorr}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onSaveAllocation() {
    let values = {
      userId: this.crtNotAllocatedUser.id,
      positionId: this.userAllocationForm.value.position
    };

    this.applicationService.allocateUserToRole(values).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeAllocationPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public onSaveRejection() {
    let values = {
      userId: this.crtNotAllocatedUser.id,
      reason: this.userRejectionForm.value.reason
    };

    this.applicationService.rejectUser(values).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeRejectionPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }


  //devextreme employee functios

  //open popups functions
  public openUpdatePopup = (event): void => {
    this.isAvailableEmployeeUpdate = !this.isAvailableEmployeeUpdate;
    this.crtEmployee = this.availableEmployees[event.row.rowIndex];

    let firstName = this.crtEmployee.firstName;
    let lastName = this.crtEmployee.lastName;

    let pipe = new DatePipe('en-US');
    let formattedEmployeeBirthDate = pipe.transform(this.crtEmployee.dateOfBirth, 'dd/MM/yyyy');

    let homeAddress = this.crtEmployee.address;
    let emailAddress = this.crtEmployee.email;
    let phoneNumber = this.crtEmployee.phoneNumber;
    let crtPosition = this.crtEmployee.position?.positionId;
    let securityQuestion = this.crtEmployee.securityQuestion;
    let answer = this.crtEmployee.answer;

    this.employeeUpdateForm = new FormGroup({
      firstName: new FormControl(firstName, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      lastName: new FormControl(lastName, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      birthDate: new FormControl(this.crtEmployee.dateOfBirth, [
        Validators.required
      ]),
      address: new FormControl(homeAddress, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ]),
      email: new FormControl(emailAddress, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
      ]),
      phoneNo: new FormControl(phoneNumber, [
        Validators.required,
        Validators.minLength(12),
        Validators.maxLength(16)
      ]),
      position: new FormControl('', [
        Validators.required
      ]),
      securityQuestion: new FormControl('', [
        Validators.required
      ]),
      answer: new FormControl(answer, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15)
      ])
    });

    this.employeeUpdateForm.controls["position"].setValue(crtPosition);
    this.employeeUpdateForm.controls["securityQuestion"].setValue(securityQuestion);
  }

  public openDeleteEmployeePopup = (event): void => {
    this.crtEmployee = this.availableEmployees[event.row.rowIndex];
    this.isDeleteEmployeeAvailable = !this.isDeleteEmployeeAvailable;
  }

  public openResetPassword() {
    this.isResetPasswordAvailable = !this.isResetPasswordAvailable;

    this.resetPasswordFormGroup = new FormGroup({
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public openRemoveEmployeesPopup() {
      this.isRemoveEmployeesAvailable = !this.isRemoveEmployeesAvailable;
  }

  public openUserAddRolePopup() {
    this.isAddUserRoleAvailable = !this.isAddUserRoleAvailable;

    this.roleAddFormGroup = new FormGroup({
      roleName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ])
    });
  }

  public openAssignMultiplePopup() {
    if(this.unretrievedDataGrid.instance.getSelectedRowsData().length == 0) {
      this.toastr.warning("Cannot open popup. No rows selected");
    } else if(this.unretrievedDataGrid.instance.getSelectedRowsData().length > 0 && this.unretrievedDataGrid.instance.getSelectedRowsData().length < 2) {
      this.toastr.warning("Select at least two users to assign to a position");
    } else {
      this.isAddMultipleUsersAvailable = !this.isAddMultipleUsersAvailable;
      this.assignMultipleFormGroup = new FormGroup({
        position: new FormControl('', [
          Validators.required
        ])
      });
    }
  }

  public openNonRetrievedPasswordPopoup() {
    this.isResetUnretrievedAvailable = !this.isResetUnretrievedAvailable;
    this.resetUnretrievedPasswordFromGroup = new FormGroup({
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public openUnretrievedDelete() {
    this.isRemoveUnretrievedAvailable = !this.isRemoveEmployeesAvailable;
  }

  public openUpdateParticipantPopup = (event): void => {
    this.isParticipantDetailsAvailable = !this.isParticipantDetailsAvailable;

    this.crtParticipant = this.participants[event.row.rowIndex];

    let name = this.crtParticipant.firstName + " " + this.crtParticipant.lastName;
    let email = this.crtParticipant.email;
    let phoneNumber = this.crtParticipant.phoneNumber;
    let position = this.crtParticipant.positionName;
    let attendedOn = this.crtParticipant.attendedOn;
    let interviewId = this.crtParticipant.interviewId;

    this.updateParticipantFormGroup = new FormGroup({
      name: new FormControl(name),
      email: new FormControl(email),
      phoneNumber: new FormControl(phoneNumber),
      position: new FormControl(position),
      attendedOn: new FormControl(attendedOn),
      interviewAssigned: new FormControl('', [
        Validators.required
      ])
    });

    this.updateParticipantFormGroup.controls["interviewAssigned"].setValue(interviewId);
  }

  public openParticipationRemovalPopup = (event): void => {
    this.isRemoveParticipationAvailable = true;
    this.crtParticipant = this.participants[event.row.rowIndex];
  }

  public openAssignationPopup() {
    this.isAssignInterviewAvailable = !this.isAssignInterviewAvailable;

    this.assingInterviewFormGroup = new FormGroup({
      interview: new FormControl('', [
        Validators.required
      ]),
      participant: new FormControl('', [
        Validators.required
      ])
    });

    this.assingInterviewFormGroup.controls['interview'].setValue(this.interviews[0].interviewId);
    this.assingInterviewFormGroup.controls['participant'].setValue(this.availableEmployees[0].id);

    this.crtSelectedInterview = this.interviews[0];
    this.crtSelectedParticipant = this.availableEmployees[0];
  }

  public onSelectionInterviewChange(event: any ) {
    let interviewId = event.source.value;
    const interview = this.interviews.find(i => i.interviewId == interviewId);

    this.crtSelectedInterview = interview!;
  }

  public onSelectionParticipantChange(event: any) {
    let participantId = event.source.value;
    const participant = this.availableEmployees.find(e => e.id == participantId);

    this.crtSelectedParticipant = participant!;
  }

  public openRemovalParticipantPopup = (event): void => {
    this.crtParticipant = this.participants[event.row.rowIndex];
    this.isRemovalAssigningAvailable = true;

    let name = this.crtParticipant.firstName + " " + this.crtParticipant.lastName;
    let position = this.crtParticipant.positionName;


    this.removalAssigningFormGroup = new FormGroup({
      name: new FormControl(name),
      position: new FormControl(position),
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  public openRemoveAllParticipantionsPopup() {
    this.isRemoveParticipationAllAvailable = true;
    this.removeAssigningAllFormGroup = new FormGroup({
      reason: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(350)
      ])
    });
  }

  //close popups functions
  public closeEmployeeUpdatePopup() {
    this.isAvailableEmployeeUpdate = !this.isAvailableEmployeeUpdate;
  }

  public closeDeleteEmployeePopup() {
    this.isDeleteEmployeeAvailable = !this.isDeleteEmployeeAvailable;
  }

  public closePasswordReset() {
    this.isResetPasswordAvailable = false;
  }

  public closeEmployeesPopup() {
    this.isRemoveEmployeesAvailable = !this.isRemoveEmployeesAvailable;
  }

  public closeUserAddPopup() {
    this.isAddUserRoleAvailable = !this.isAddUserRoleAvailable;
  }

  public closeMultipleUserAllocation() {
    this.isAddMultipleUsersAvailable = !this.isAddMultipleUsersAvailable;
  }

  public closeNonRetrievedPasswordPopup() {
    this.isResetUnretrievedAvailable = false
  }

  public closeRemoveUnretrievedPopup() {
    this.isRemoveUnretrievedAvailable = !this.isRemoveUnretrievedAvailable;
  }

  public closeParticipantUpdate() {
    this.isParticipantDetailsAvailable = !this.isParticipantDetailsAvailable;
  }

  public closeRemovalParticipationPopup() {
    this.isRemovalAssigningAvailable = !this.isRemovalAssigningAvailable;
  }

  public closeAllocateInterview() {
    this.isAssignInterviewAvailable = !this.isAssignInterviewAvailable;
  }

  public closeAllParticipations() {
    this.isRemoveParticipationAllAvailable = !this.isRemoveParticipationAllAvailable;
  }


  //other functions
  public sendEmployeeUpdate() {
    let values = {
      firstName: this.employeeUpdateForm.value.firstName,
      lastName: this.employeeUpdateForm.value.lastName,
      emailAddress: this.employeeUpdateForm.value.email,
      address: this.employeeUpdateForm.value.address,
      dateOfBirth: this.employeeUpdateForm.value.birthDate,
      phoneNumber: this.employeeUpdateForm.value.phoneNo,
      positionId: this.employeeUpdateForm.value.position,
      securityQuestion: this.employeeUpdateForm.value.securityQuestion,
      securityQuestionAnswer: this.employeeUpdateForm.value.answer
    };

    this.applicationService.updateAvailableEmployee(values, this.crtEmployee.id!).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeEmployeeUpdatePopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }
  
  public sendOnDeleteEmployee() {
    this.applicationService.removeAvailableEmployee(this.crtEmployee.id!).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeDeleteEmployeePopup();
    }, error =>{ 
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public sendPasswordRequests() {
    if(this.resetPasswordFormGroup.valid) {
      this.availableEmployees.forEach(availableEmployee => {
        let values = {
          email: availableEmployee.email,
          reason: this.resetPasswordFormGroup.value.reason
        };
  
        this.applicationService.sendUrgentlyPasswordRequest(values).then(async response =>{
          this.ngOnInit();
          this.toastr.success(`Server response: ${response}`);
          this.closePasswordReset();
        }, error => {
          console.log(`Error ${error.error}`);
          this.toastr.error(`Error ${error.error}`);
        });
      });
    }
  }

  public sendAutoPasswordsRequests() {
    if(this.resetPasswordFormGroup.valid) {
      this.availableEmployees.forEach(availableEmployee => {
        let values = {
          email: availableEmployee.email,
          reason: this.resetPasswordFormGroup.value.reason
        };

        this.applicationService.sendAutoGeneratedPasswords(values).then(async response => {
          this.ngOnInit();
          this.toastr.success(`Server response: ${response}`);
          this.closePasswordReset();
        }, error => {
          console.log(`Error ${error.error}`);
          this.toastr.error(`Error ${error.error}`);
        });
      })
    }
  }

  public exportEmployeesData() {
    this.applicationService.exportEmployeesData().then(async response => {
      this.exportEmployeesExcel(response);
      this.toastr.success(`Employees report was successfully generated and will appear soon in 
        Download tab inside your browser`);
    })
  }

  public exportEmployeesExcel(data: any) {
    var crtTime = new Date();
    var crtTimeFormatted = formatDate(crtTime, "dd/MM/yyyy HH:mm:ss", "en-US");

    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;'});
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Employees ' + crtTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  public removeEmployees() {
    this.applicationService.removeAllEmployees().then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response ${response}`);
      this.closeEmployeesPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public addUserRole() {
    if(this.roleAddFormGroup.valid){
      let values = {
        roleName: this.roleAddFormGroup.value.roleName
      };
  
      this.applicationService.addUserRole(values).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeUserAddPopup();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }
  }

  public assignUnretrieved() {
    if(this.assignMultipleFormGroup.valid) {
      this.selectedUsers = this.unretrievedDataGrid.instance.getSelectedRowsData();
    
      let selectedPositionId = this.assignMultipleFormGroup.value.position;
  
      let serverResponseMessage = '';
      this.selectedUsers.forEach(selectedUser => {
        let values = {
          userId : selectedUser.id,
          positionId: selectedPositionId
        };
  
        this.applicationService.allocateUserToRole(values).then(async response => {
          serverResponseMessage = response;
          this.ngOnInit();
        }, error => {
          console.log(`Error ${error.error}`);
          this.toastr.error(`Error ${error.error}`);
        });
      });
  
      this.toastr.success(`All selected users were assigned to the selected position`);
      this.closeMultipleUserAllocation();
    } else {
      this.toastr.warning("Cannot send the request. Select one position to proceed");
    }
  }

  public sendPasswordRequestsUnretrieved() {
    if(this.resetUnretrievedPasswordFromGroup.valid) {

      this.notRetrievedUsers.forEach(notRetrievedUser => {
        let values = {
          email: notRetrievedUser.email,
          reason: this.resetUnretrievedPasswordFromGroup.value.reason
        };
  
        this.applicationService.sendUrgentlyPasswordRequest(values).then(async response =>{
          this.ngOnInit();
          this.toastr.success(`Server response: ${response}`);
          this.closeNonRetrievedPasswordPopup();
        }, error => {
          console.log(`Error ${error.error}`);
          this.toastr.error(`Error ${error.error}`);
        });
      });
    }
  }

  public sendAutogeneratePasswordsUnretrieved() {
    if(this.resetUnretrievedPasswordFromGroup.valid) {
      this.notRetrievedUsers.forEach(notRetrievedUser => {
        let values = {
          email: notRetrievedUser.email,
          reason: this.resetUnretrievedPasswordFromGroup.value.reason
        };

        this.applicationService.sendAutoGeneratedPasswords(values).then(async response => {
          this.ngOnInit();
          this.closeNonRetrievedPasswordPopup();
        }, error => {
          console.log(`Error ${error.error}`);
          this.toastr.error(`Error ${error.error}`);
        });
      })
    }
  }

  public removeUnretrievedUsers() {
    this.applicationService.removeUnretrievedUsers().then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeRemoveUnretrievedPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  //Participants other methods
  public getParticipants() {
    this.applicationService.getParticipants().then(async response => {
      this.participants = [];
      response.forEach(participant => {
        this.participants.push(participant);
      });
    });
  }

  public getInterviews() {
    this.interviewService.getStoredInterviews().then(async response => {
      this.interviews = [];
      response.forEach(interview => {
        this.interviews.push(interview);
      });
    });
  }

  public updateParticipantDetails() {
    if(this.updateParticipantFormGroup.valid) {
      let values = {
        participantId: this.crtParticipant.id,
        interviewId: this.updateParticipantFormGroup.value.interviewAssigned,
        oldInterviewId: this.crtParticipant.interviewId
      };

      this.applicationService.updateParticipantDetails(values).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeParticipantUpdate();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }
  }

  public removeParticipation() {
    if(this.removalAssigningFormGroup.valid) { 
      this.applicationService.removeParticipationInterview(this.crtParticipant.interviewId, this.crtParticipant.id).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response ${response}`);
        this.closeRemovalParticipationPopup();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }
  }

  public allocateParticipantInterview() {
    if(this.assingInterviewFormGroup.valid) {
      let values = {
        firstName: this.crtSelectedParticipant.firstName,
        lastName: this.crtSelectedParticipant.lastName,
        email: this.crtSelectedParticipant.email,
        phoneNumber: this.crtSelectedParticipant.phoneNumber,
        address: this.crtSelectedParticipant.address,
        dateOfBirth: this.crtSelectedParticipant.dateOfBirth,
        positionId: this.crtSelectedParticipant.position?.positionId,
        attendedInterviewId: this.crtSelectedInterview.interviewId
      };

      let currentUser = this.accountService.currentUserValue;

      this.interviewService.allocateParticipantInterview(values, currentUser.id!).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeAllocateInterview();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
        this.closeAllocateInterview();
      });
    }
  }

  public removeAllUsers() {
    if(this.removeAssigningAllFormGroup.valid) {
      let message = "";
      let isError = false;
      this.participants.forEach(participant => {
        this.applicationService.removeParticipationInterview(participant.interviewId, participant.id).then(async response => {
          this.ngOnInit();
          message = response;
          isError = false;
          this.closeAllParticipations();
        }, error =>{ 
          console.log(`Error ${error.error}`);
          isError = true;
          this.closeAllParticipations();
        });
      });

      if(isError) {
        this.toastr.success(`Server response: ${message}`);
      }
    }
  }

}
