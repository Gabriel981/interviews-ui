import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeParticipantView } from '../objects/EmployeeParticipantView';
import { HiringProcess } from '../objects/HiringProcess';
import { Interview } from '../objects/Interview';
import { InterviewParticipantView } from '../objects/InterviewParticipantView';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { AnswerService } from '../_services/answer.service';
import { InterviewService } from '../_services/interview.service';
import { QuizService } from '../_services/quiz.service';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css']
})

export class InterviewComponent implements OnInit {

  // New table elements
  public dataSource = new MatTableDataSource<EmployeeParticipantView>();
  public displayedColumns: string[] = ['scheduledDate', 'candidateName', 'candidateCurrentStatus', 'moreDetails', 'updateDetails',
    'deleteDetails', 'addParticipant', 'seeParticipants'];
  public filterColumns: string[] = ['submissionDateFilter', 'candidateNameFilter', 'candidateCurrentStatusFilter'];
  public timer = 1000;
  public timeoutVal = 1000;
  public timeoutValDate = 1500;
  public fromDate!: any;
  public toDate!: any;

  interviews: Interview[] = [];
  hiringProcesses!: HiringProcess[];

  public availableEmployees!: EmployeeParticipantView[];
  public availableParticipants: InterviewParticipantView[] = [];

  public communicationChannels = [
    { id: 1, name: 'ONLINE' },
    { id: 2, name: 'ON-SITE' }
  ];

  //paginator
  public pageLength: number = 0;
  public splicedData!: any[];
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  public interviewScheduleForm!: FormGroup;
  public interviewDetailsForm!: FormGroup;
  public interviewUpdateForm!: FormGroup;
  public interviewParticipantPost!: FormGroup;
  public interviewParticipantDetails!: FormGroup;
  public interviewQuizCreator!: FormGroup;
  public contentQuizDetailsFG!: FormGroup;
  public dynamicQuestionCreator!: FormGroup;

  // Stepper form groups
  public questionFormGroup!: FormGroup;
  public secondQuestionFromGroup!: FormGroup;
  public items!: FormArray;

  public isLinear = true;

  public openedState = false;

  public closedResult: string = '';

  public selectedCommunication;

  public currentUser!: User;

  public currentInterviewId!: number;

  public currentInterview!: Interview;

  private questionId!: number;

  private selectedEmployeeParticipant!: EmployeeParticipantView;

  public questionRequieredInterview!: Interview;

  public questions: any[] = [];

  public communicationDefault!: string;

  // private interviewService: InterviewService = new InterviewService(this.httpClient, this.authenticationService);
  // private authenticationUserService: AuthenticationUserService = new AuthenticationUserService(this.httpClient, this.authenticationService);

  private deleteId!: number;
  private updateId!: number;
  private quizDeleteInterviewId!: number;

  constructor(
    private toastr: ToastrService,
    private accountService: AccountService,
    private interviewService: InterviewService,
    private quizService: QuizService,
    private answerService: AnswerService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {
    this.questionFormGroup = this.formBuilder.group({
      questionBody: ['', Validators.required],
    });

    this.secondQuestionFromGroup = this.formBuilder.group({
      // firstAnswerBody: ['', Validators.required],
      // secondAnswerBody: [''],
      // thirdAnswerBody: ['']
      items: this.formBuilder.array([this.createItem()])
    });

    this.interviewScheduleForm = new FormGroup({
      interviewDate: new FormControl('', [

      ]),
      hiringProcess: new FormControl('', [
        Validators.required
      ])
    });

    this.contentQuizDetailsFG = new FormGroup({
      quizCreatedOnDetails: new FormControl(''),
      positionQuizDetails: new FormControl(''),
      createdByQuizDetails: new FormControl(''),
      scoreQuizDetails: new FormControl(''),
      mentionsQuizDetails: new FormControl('')
    });

    this.dynamicQuestionCreator = new FormGroup({
      positionNameQuizCreator: new FormControl(''),
      quizCreatorBody: new FormControl('', [
        Validators.required,
        Validators.minLength(15),
        Validators.maxLength(150)
      ])
    });

    // this.interviewUpdateForm = new FormGroup({
    //   candidateName: new FormControl(),
    //   interviewScheduledDate: new FormControl('', [Validators.required]),
    //   communicationChannelUpdate: new FormControl('', [Validators.required])
    // });

    this.currentUser = this.accountService.currentUserValue;
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      answerBody: new FormControl('', Validators.required)
    });
  }

  addItem(): void {
    this.items = this.secondQuestionFromGroup.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  removeItem(i: number) {
    const control = this.secondQuestionFromGroup.get('items') as FormArray;
    control.removeAt(i);
  }

  public get interviewDate() {
    return this.interviewScheduleForm.get('interviewDate');
  }

  public get hiringProcess() {
    return this.interviewScheduleForm.get('hiringProcess');
  }

  public get candidateName() {
    return this.interviewUpdateForm.get('candidateName');
  }

  public get interviewScheduledDate() {
    return this.interviewUpdateForm.get('interviewScheduledDate');
  }

  public get communicationChannelUpdate() {
    return this.interviewUpdateForm.get('communicationChannelUpdate');
  }

  public get employeeParticipant() {
    return this.interviewParticipantPost.get('employeeParticipant');
  }

  ngOnInit(): void {
    // this.getCurrentUser();
    this.getAllInterviews();
    this.getAllHiringProcesses();
    this.getAllAvailableEmployees();
  }

  // Sort functions
  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if (!sort.active || sort.direction == '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'scheduledDate': return this.compare(a.interviewDate.toString(), b.interviewDate.toString(), isAsc);
        case 'candidateName': return this.compare(a.candidateFirstName!, b.candidateFirstName!, isAsc);
        case 'candidateCurrentStatus': return this.compare(a.candidateSituation, b.candidateSituation, isAsc);
        default: return 0;
      }
    });
  }

  public compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  // Filter functions
  public applyNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getInterviewCandidate(elements);
    }, this.timeoutVal);
  }

  public getInterviewCandidate(key: any) {
    if (key != null) {
      if (key.key == "") {
        this.getAllInterviews();
      } else {
        this.interviewService.customSearchFilterByCandidate(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public applySubmissionDateFilter(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    let dataRangeStartValue = dateRangeStart.value.toString();
    let dataRangeEndValue = dateRangeEnd.value.toString();

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        startDate: dataRangeStartValue,
        lastDate: dataRangeEndValue
      };

      this.getSubmissionByDateFilter(elements);
    }, this.timeoutValDate);
  }

  public getSubmissionByDateFilter(key: any) {
    if (key.startDate == "" && key.endDate == "") {
      this.getAllInterviews();
    } else {
      this.interviewService.customSearchFilterByDate(key).then((response) => {
        this.pageLength = response.length;
        this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

        this.dataSource.data = this.splicedData;
        this.pageSizeOptions = [1, response.length];
      }, error => {
        console.log(error.error);
        this.toastr.error(`Error: ${error.error}`);
      });
    }
  }

  public applyStatusFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getInterviewByStatus(elements);
    }, this.timeoutVal);
  }

  public getInterviewByStatus(key: any) {
    if (key != null) {
      if (key.key == "") {
        this.getAllInterviews();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.interviewService.customSearchFilterByStatus(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public resetForm() {
    this.fromDate = undefined;
    this.toDate = undefined;

    this.getAllInterviews();
  }


  public pageChangeEvent(event) {
    const offset = ((event.pageIndex + 1) - 1) * event.pageSize;
    this.splicedData = this.interviews.slice(offset).slice(0, event.pageSize);
  }

  public getAllInterviews() {
    this.interviewService.getStoredInterviews().then(response => {
      let retrievedInterviews = response;
      retrievedInterviews.forEach(retrievedInterview => {
        this.interviews.push(retrievedInterview);
      });

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public getAllHiringProcesses() {
    this.interviewService.getStoreHiringProcesses().then(response => {
      this.hiringProcesses = response;
    });
  }

  open(content: any) {
    if (this.hiringProcesses.length == 0) {
      this.showNoHiringsFoundToast();
    } else {
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
        this.closedResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closedResult = `Dismissied ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on backdrop';
    } else {
      return `with ${reason}`;
    }
  }

  public onSubmit() {
    var interview = {
      interviewDate: this.interviewScheduleForm.value.interviewDate,
      hiringId: this.interviewScheduleForm.value.hiringProcess.hiringId,
      createdById: this.currentUser.id
    }

    var currentUser: any;

    // this.accountService.getCurrentUser().then(user => {
    //   this.currentUser = user;
    // });

    // const currentUser = this.accountService.currentUserValue;

    if (this.interviewScheduleForm.valid) {
      this.interviewService.scheduleNewInterview(interview).then(() => {
        this.showScheduledSentSuccessfully();
        this.ngOnInit();
        this.modalService.dismissAll();
      });
    }
  }

  public openDetailForm(targetModal: any, interview: Interview) {

    let candidateCompleteName = interview.candidateFirstName + ' ' + interview.candidateLastName;

    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });


    var datePipe = new DatePipe('en-US');

    var submissionDate = datePipe.transform(interview.submission?.dateOfSubmission, 'dd/MM/yyyy');
    var interviewSheduledDate = datePipe.transform(interview.interviewDate, 'dd/MM/yyyy');

    this.interviewDetailsForm = this.formBuilder.group({
      interviewNameDetails: candidateCompleteName,
      interviewPositionName: interview.positionName,
      interviewSubmissionDate: submissionDate,
      interviewScheduledDate: interviewSheduledDate
    });

    console.log(candidateCompleteName);
    console.log(interview.positionName);
  }

  public openDelete(targetModal: any, interview: Interview) {
    this.deleteId = interview.interviewId!;

    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  public onDelete() {
    this.interviewService.removeScheduledInterview(this.deleteId!).then(response => {
      this.toastr.success(`Server response: ${response}`);
      this.ngOnInit();
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public openInterviewUpdate(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.updateId = interview.interviewId!;

    let candidateCompleteName = interview.candidateFirstName + ' ' + interview.candidateLastName;

    var datePipe = new DatePipe('en-US');
    var interviewScheduledDateNew = datePipe.transform(interview.interviewDate, 'dd/MM/yyyy');

    const commDefault = this.communicationChannels.find(c => c.name == interview.communicationChannel?.toString());
    this.selectedCommunication = commDefault?.name;

    this.interviewUpdateForm = this.formBuilder.group({
      candidateName: [''],
      interviewScheduledDate: [''],
      communicationChannelUpdate: [this.selectedCommunication]
    });


    this.interviewUpdateForm.controls['interviewScheduledDate'].setValue(interview.interviewDate);
    this.interviewUpdateForm.controls['candidateName'].setValue(candidateCompleteName);
    // this.interviewUpdateForm.controls['communicationChannelUpdate'].setValue(this.communicationChannels[communicationChannelKey].name.toString());


    // this.interviewUpdateForm.get('communicationChannelUpdate')?.setValue(commDefault?.name);

    this.interviewUpdateForm.controls['communicationChannelUpdate'].setValue(commDefault?.name);

  }

  public openParticipantPostForm(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.currentInterviewId = interview.interviewId!;

    const candidateCompleteName = interview.candidateFirstName + " " + interview.candidateLastName;
    var scheduledDateNotFormatted = new DatePipe('en-US');
    var scheduledDateFormatted = scheduledDateNotFormatted.transform(interview.interviewDate, 'dd/MM/yyyy HH:mm:ss');

    this.interviewParticipantPost = new FormGroup({
      candidateCompleteNameInterviewParticipantPost: new FormControl(candidateCompleteName),
      appliedPositionInterviewParticipantPost: new FormControl(interview.positionName),
      interviewScheduledDateInterviewParticipantPost: new FormControl(scheduledDateFormatted),
      employeeParticipant: new FormControl('',
        [
          Validators.required
        ])
    });
  }

  public openAvailableInterviewsQuiz(targetModal: any) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
  }

  public openQuizCreator(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.interviewQuizCreator = new FormGroup({
      interviewScheduledOnQuiz: new FormControl(''),
      interviewPositionNameQuiz: new FormControl(''),
      interviewMentionsQuiz: new FormControl('')
    });

    this.interviewQuizCreator.controls['interviewScheduledOnQuiz'].setValue(interview.interviewDate);
    this.interviewQuizCreator.controls['interviewPositionNameQuiz'].setValue(interview.positionName);

    this.currentInterviewId = interview.interviewId!;
    this.currentInterview = interview;
  }

  public openAvailableParticipantDetails(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.currentInterviewId = interview.interviewId!;
    this.getAllAvailableParticipants();
  }

  public openRemovalConfirmation(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.quizDeleteInterviewId = interview.interviewId!;
  }

  public openQuizDetails(targetModal: any, interview: Interview) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    var createdOnDateNotFormatted = new DatePipe('en-US');
    var createdOnDateFormatted = createdOnDateNotFormatted.transform(interview.quiz?.createdAt, 'dd/MM/yyyy HH:mm:ss');

    var createdOn = createdOnDateFormatted
    var position = interview.positionName;
    var createdBy = interview.quiz?.createdBy?.firstName + " " + interview.quiz?.createdBy?.lastName;
    var score = interview.quiz?.scoreResult;
    var mentions = interview.quiz?.mentions;

    this.contentQuizDetailsFG.controls['quizCreatedOnDetails'].setValue(createdOn);
    this.contentQuizDetailsFG.controls['positionQuizDetails'].setValue(position);
    this.contentQuizDetailsFG.controls['createdByQuizDetails'].setValue(createdBy);
    this.contentQuizDetailsFG.controls['scoreQuizDetails'].setValue(score);
    this.contentQuizDetailsFG.controls['mentionsQuizDetails'].setValue(mentions);

    this.questionRequieredInterview = interview;
    this.getAllQuestions();
  }

  public openDynamicQuestionCreator(targetModal: any) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    var positionName = this.questionRequieredInterview.positionName;

    this.dynamicQuestionCreator.controls['positionNameQuizCreator'].setValue(positionName);
  }

  public submitParticipantInterview() {
    this.selectedEmployeeParticipant = this.interviewParticipantPost.value.employeeParticipant;

    var interviewParticipantDetails = {
      firstName: this.selectedEmployeeParticipant.firstName,
      lastName: this.selectedEmployeeParticipant.lastName,
      email: this.selectedEmployeeParticipant.email,
      phoneNumber: this.selectedEmployeeParticipant.phoneNumber,
      address: this.selectedEmployeeParticipant.address,
      dateOfBirth: this.selectedEmployeeParticipant.dateOfBirth,
      positionId: this.selectedEmployeeParticipant.positionId,
      attendedInterviewId: this.currentInterviewId
    };

    const currentSenderId = this.currentUser.id;

    this.interviewService.allocateParticipantInterview(interviewParticipantDetails, currentSenderId!).then((response) => {
      this.showPartipantSubmissionSuccess(true);
      let element: HTMLElement = document.getElementById('contentParticipantCloseBtn') as HTMLElement;
      element.click();
    });
  }

  public onSaveUpdate() {
    console.log(this.interviewUpdateForm.value.communicationChannelUpdate.name);

    const valuesToBeUpdated = {
      interviewId: this.updateId,
      interviewDate: this.interviewUpdateForm.value.interviewScheduledDate,
      communicationChannel: this.interviewUpdateForm.value.communicationChannelUpdate.name
    };

    this.interviewService.updateInterviewDetails(this.updateId, valuesToBeUpdated).then(response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.modalService.dismissAll();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public exportData() {
    this.interviewService.exportAllAvailableInterviews().then((response) => {
      this.downloadExcelFile(response);
      this.showExportInterviewDataSuccessToast();
    });
  }

  public downloadExcelFile(data: any) {
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, "dd/MM/yyyy HH:mm:ss", "en-US");

    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Interviews ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  public getAllAvailableEmployees() {
    this.interviewService.getAllAvailableEmployees().then((response) => {
      this.availableEmployees = response;
    });
  }

  public getAllAvailableParticipants() {
    this.interviewService.getAllAvailableParticipantsByInterview(this.currentInterviewId).then((response) => {
      if (response.length > 0) {
        let receivedParticipants = response;
        receivedParticipants.forEach(receivedParticipant => {
          this.availableParticipants.push(receivedParticipant);
        });
      } else {
        this.availableParticipants = [];
        this.toastr.warning("No available participants found for this interview");
        let element: HTMLElement = document.getElementById('contentParticipantCloseBtn') as HTMLElement;
        element.click();
      }
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public removeParticipantFromInterview(participantId: string) {
    this.interviewService.removeParticipantFromInterview(this.currentInterviewId, participantId).then(() => {
      this.showParticipantRemovalSuccess();
      this.getAllAvailableParticipants();
    });
  }

  public showNoHiringsFoundToast() {
    this.toastr.warning("No candidates available for interviews were found in the database");
  }

  public showScheduledSentSuccessfully() {
    this.toastr.success("The schedule was successfully pushed to database");
  }

  public showNoEmployeesFound() {
    this.toastr.warning("No employees available to be allocated to the interviews found in the database!");
  }

  public showPartipantSubmissionSuccess(isSent: boolean) {
    if (isSent == true) {
      this.toastr.success("The participant submission was successfully sent to the database!");
    }
    else {
      this.toastr.error("The participant submission cannot be sent to the database. An error occured during the process!");
    }
  }

  public showParticipantRemovalSuccess() {
    this.toastr.success("The selected participant was successfully detached from this interview");
  }

  public showExportInterviewDataSuccessToast() {
    this.toastr.success("The interviews data was successfully exported. Check your download tab.");
  }

  // public getCurrentUser() {
  //   this.accountService.getCurrentUser().then((response) => {
  //     this.currentUser = response;
  //     this.getAllInterviews();
  //     this.getAllHiringProcesses();
  //     this.getAllAvailableEmployees();
  //   });
  // }

  public PostQuiz() {
    let quizData: any = {
      mentions: this.interviewQuizCreator.value.interviewMentionsQuiz,
      interviewId: this.currentInterviewId,
      createdById: this.currentUser.id
    };

    this.quizService.addQuiz(quizData).then((response) => {
      this.toastr.success(`Server response: ${response}`);
      let element: HTMLElement = document.getElementById('contentQuizCreatorInterviewCloseBtn') as HTMLElement;
      element.click();
      this.ngOnInit();
    }, error => {
      console.log(error);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public removeQuiz() {
    this.quizService.removeQuiz(this.quizDeleteInterviewId!).then((response) => {
      console.log(response);
      this.toastr.success(`Server response: ${response}`);
      let element: HTMLElement = document.getElementById('quizContentDeleteInterviewBtn') as HTMLElement;
      element.click();
      this.ngOnInit();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error ${error.error}`);
    });
  }

  public postQuestion() {

    var currentUserId = this.currentUser.id;
    var positionId = this.questionRequieredInterview.quiz?.positionId;
    var quizId = this.questionRequieredInterview.quiz?.quizId;
    var questionBody = this.questionFormGroup.value.questionBody;

    var questionData = {
      questionBody: questionBody,
      createdById: currentUserId,
      positionId: positionId,
      quizId: quizId
    };

    this.interviewService.postQuestionDynamically(questionData).then(response => {
      this.questionId = response;
      this.toastr.success("The question was succesfully submitted to the database");
      this.postAnswers();
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error ${error.error}`);
    })
    // var quizId = this.questionRequieredInterview.quiz.id

    // var questionData = {
    //   questionBody: this.dynamicQuestionCreator.value.positionNameQuizCreator,
    //   createdById: currentUserId,
    //   positionId: positionId,
    //   quizId: this.
    // }
  }

  public postAnswers() {
    let answersArray: any[] = [];

    this.items.controls.forEach((element, index) => {
      answersArray.push(element.value.answerBody);
    });

    // var answerBody = this.secondQuestionFromGroup.value.firstAnswerBody;
    // var answerBody2 = this.secondQuestionFromGroup.value.secondAnswerBody;
    // var answerBody3 = this.secondQuestionFromGroup.value.thirdAnswerBody;



    // if(answerBody != '') {
    //   answersArray.push(answerBody);
    // }

    // if(answerBody2 != '') {
    //   answersArray.push(answerBody2);
    // }

    // if(answerBody3 != '') {
    //   answersArray.push(answerBody3);
    // }

    var createdById = this.currentUser.id;

    let finalArray: any[] = [];

    if (answersArray.length > 0) {
      answersArray.forEach(answer => {
        if (answer != '') {
          var answerSend = {
            answerBody: answer,
            questionId: this.questionId,
            createdById: createdById
          };

          finalArray.push(answerSend);
        }
      });
    }

    if (finalArray.length > 0) {
      this.answerService.addAnswer(finalArray).then((response) => {
        this.toastr.success(`Server response: ${response}`);
        this.modalService.dismissAll();
      }, error => {
        console.log(error.error);
        this.toastr.error(`Error ${error.error}`);
      });
    }



    // if(answersArray.length > 0) {
    //   answersArray.forEach(answer => {
    //     var answerSend = {
    //       answerBody : answer,
    //       questionId: this.questionId,
    //       createdById: createdById
    //     };

    //     finalArray.push(answerSend);
    //   });

    //   if(finalArray.length > 0) {
    //     this.answerService.addAnswer(finalArray).then((response) => {
    //       this.toastr.success(`Server response: ${response}`);
    //       this.modalService.dismissAll();
    //     }, error => {
    //       console.log(error.error);
    //       this.toastr.error(`Error ${error.error}`);
    //     });
    //   }
    // }
  }

  public getAllQuestions() {
    let currentQuizId = this.questionRequieredInterview.quiz?.quizId;
    this.quizService.getQuizQuestions(currentQuizId!).then(response => {
      this.questions = response;
    }, error => {
      console.log(error.error);
      this.toastr.error(`Error ${error.error}`);
    });
  }
}
