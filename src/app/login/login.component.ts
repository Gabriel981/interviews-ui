import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  
  private authService : RegisterService = new RegisterService(this.httpClient, this.router, this.modalService);
  public candidatesLogin!: FormGroup;

  constructor(private router: Router, private httpClient: HttpClient, private modalService: NgbModal) { 
    // this.candidatesLogin = new FormGroup({
    //   emailAddress: new FormControl('',[
    //   ]),
    //   password : new FormControl('', [

    //   ])
    // })
  }

  public get emailAddress(){
    return this.candidatesLogin.get('emailAddress');
  }

  public get password(){
    return this.candidatesLogin.get('password');
  }

  public doLogin(form: NgForm){
    // const loginData = {'emailAddress': form.value.emailAddress, 'password' : form.value.password}

    const loginData = {'emailAddress' : this.candidatesLogin.value.emailAddress, 'password' : this.candidatesLogin.value.password}

    this.authService.EmployeeLogin(loginData);
  }

}
