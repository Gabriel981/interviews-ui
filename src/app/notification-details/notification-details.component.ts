import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Notification } from '../objects/Notification';
import { Proposal } from '../objects/Proposal';
import { User } from '../objects/User';
import { TransferService } from '../services/transfer.service';
import { AccountService } from '../_services/account.service';
import { NotificationService } from '../_services/notification.service';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.css']
})
export class NotificationDetailsComponent implements OnInit {

  public notificationData!: Notification;
  public notificationFinalData!: Proposal;

  public userFound!: User;

  public subscription!: Subscription;
  public browswerRefresh = false;

  public notificationDataRefresh!: Notification;


  // private managerService: ManagerService = new ManagerService(this.httpClient, this.authenticationService);
  // private authenticationUserService: AuthenticationUserService = new AuthenticationUserService(this.httpClient, this.authenticationService);
  
  // private notificationService: NotificationService = new NotificationService(this.httpClient, this.authenticationService);


  constructor(
    private transferService: TransferService,
    private notificationService: NotificationService,
    private accountController: AccountService,
    private router: Router) { 

      // this.subscription = this.router.events.subscribe((event) => {
      //   if(event instanceof NavigationStart) {
      //     console.log('ajunge aici macar?');
      //   }
      // });

      this.userFound = this.accountController.currentUserValue;
  }

  ngOnInit(): void {
    // if(window.performance.navigation.type == 1) {
    //   console.log(window.performance.navigation.type == 1);
    //   this.router.navigateByUrl("#");
    // }
    // this.getCurrentUser();

    this.PopulateNotificationData();
    this.PopulateNotificationFinalData();
  }

  public get notificationDataHTML(){
    return this.notificationData;
  }

  public PopulateNotificationData(){
    if(window.performance.navigation.type == 1) {
      var retrievedData = sessionStorage.getItem('notificDetails');
      this.notificationData = JSON.parse(retrievedData!);
      // this.notificationData = this.notificationDataRefresh;
    }
    else {
      this.notificationData = this.transferService.getData();
      sessionStorage.setItem('notificDetails', JSON.stringify(this.notificationData));
      // localStorage.setItem('notificDetails', JSON.stringify(this.notificationData));
    }
  }

  public PopulateNotificationFinalData(){
    var userId = this.userFound.id!;

    this.notificationService.GetNotificationDetails(this.notificationData.notificationId, userId).then((response) => {
      console.log(response);
      this.notificationFinalData = response;
    });
  }


  // public getCurrentUser() {
  //   this.accountController.getCurrentUser().then(response => {
  //     if(response != null){
  //       this.userFound = response;
  //       if(this.notificationData == undefined || this.notificationData == null) {
  //         this.PopulateNotificationData();
  //         this.PopulateNotificationFinalData();
  //       } else {
  //         this.PopulateNotificationFinalData();
  //       }
  //     }
  //   });
  // }

  

}
