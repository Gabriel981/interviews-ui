import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationService } from '../guards/AuthenticationService';
import { Position } from '../objects/Position';
import { User } from '../objects/User';
import { ApplicationService } from '../services/application.service';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public freePositions!: Position[];
  private applicationService: ApplicationService = new ApplicationService(this.httpClient, this.authenticationService);
  

  public crtUser!: User;

  constructor(private router: Router,
     private jwtHelper: JwtHelperService,
     private httpClient: HttpClient, 
     private authenticationService: AuthenticationService,
     private accountService: AccountService) { 

      this.getCurrentUser();
  }

  ngOnInit(): void {
    this.populatePositions();
  }

  isUserAuthenticated(){
    const token: any = localStorage.getItem("jwt");
    if(token && !this.jwtHelper.isTokenExpired(token)){
      return true
    }
    else {
      return false;
    }
  }

  logOut(){
    localStorage.removeItem("jwt");
  }

  public populatePositions(){
    return this.applicationService.getAllAvailablePositions().then(response => {
      console.log(response);
      this.freePositions = response;
    });
  }

  public getCurrentUser () {
    this.crtUser = this.accountService.currentUserValue;
  }

}
