import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HighlightSpanKind } from 'typescript';
import { ApplicationRole } from '../objects/ApplicationRole';
import { Position } from '../objects/Position';
import { User } from '../objects/User';
import { FieldStateMatcher } from '../_helpers/state.matcher.helper';
import { ApplicationService } from '../_services/application.service';
import { RoleService } from '../_services/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  //roles elements
  public roles: ApplicationRole[] = [];
  public users: User[] = [];
  public positions: Position[] = [];

   //devextreme not retrieved users elements
   public allMode!: string;
   public checkBoxesMode!: string;

   //role devextreme elements
   public isRoleCreationAvailable: boolean = false;
   public isRoleRemovalAvailable: boolean = false;
   public isAssignRoleAvailable: boolean = false;
   public isRemovalFromRoleAvailable: boolean = false;
   
   public crtRole!: ApplicationRole;
   public crtSelectedUser!: User;

   public roleCreationFormGroup!: FormGroup;
   public assignRoleFormGroup!: FormGroup;


  //state matchers
  public roleNameMatcher = new FieldStateMatcher();
 

  constructor(private roleService: RoleService, private toastr: ToastrService, private applicationService: ApplicationService) {
    this.allMode = 'allPages';
    this.checkBoxesMode = 'onClick';
  }

  ngOnInit(): void {
    this.getAllRoles();
    this.getAllUsers();
    this.getAllPositions();
  }

  public getAllRoles() {
    this.roleService.getAllRoles().then(async response => {
      this.roles = response;
    });
  }

  public getAllUsers() {
    this.applicationService.getUsers().then(async response => {
      this.users = [];
      response.forEach(user => {
        this.users.push(user);
      });
    });
  }

  public getAllPositions() {
    this.roleService.getPositions().then(async response => {
      this.positions = response;
    });
  }

  //open popup functions
  public openRoleCreationPopup() {
    this.isRoleCreationAvailable = !this.isRoleCreationAvailable;
    this.roleCreationFormGroup = new FormGroup({
      roleName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30)
      ])
    });
  }

  public openRoleRemovalPopup = (event): void => {
    this.isRoleRemovalAvailable = !this.isRoleRemovalAvailable;
    this.crtRole = this.roles[event.row.rowIndex];
  }

  public openAssignRolePopup = (event): void => {
    this.isAssignRoleAvailable = !this.isAssignRoleAvailable;
    this.crtRole = this.roles[event.row.rowIndex];
  
    this.assignRoleFormGroup = new FormGroup({
      user: new FormControl('', [
        Validators.required
      ]),
      role: new FormControl(this.crtRole.name)
    });

    this.crtSelectedUser = this.users[0];
    this.crtSelectedUser.position = this.positions.find(p => p.positionId == this.crtSelectedUser.positionId);
    this.assignRoleFormGroup.controls["user"].setValue(this.users[0].id);
  }

  public openUserInRoleRemoval = (event):void => {
    this.isRemovalFromRoleAvailable = !this.isRemovalFromRoleAvailable;
    this.crtSelectedUser = this.users[event.row.rowIndex];
  }

  //close popup functions
  public closeRoleCreationPopup() {
    this.isRoleCreationAvailable = !this.isRoleCreationAvailable;
  }

  public closeRoleRemovalPopup() {
    this.isRoleRemovalAvailable = !this.isRoleRemovalAvailable;
  }

  public closeAssignRolePopup() {
    this.isAssignRoleAvailable = !this.isAssignRoleAvailable;
  }

  public closeUserInRoleRemoval() {
    this.isRemovalFromRoleAvailable = !this.isRemovalFromRoleAvailable;
  }

  //other functions
  public createNewRole() {
    if(this.roleCreationFormGroup.valid) {
      let roleName = this.roleCreationFormGroup.value.roleName;

      this.roleService.createNewRole(roleName).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeRoleCreationPopup();
      }, error => {
        console.log(`Error ${error.error}`);
        this.toastr.error(`Error ${error.error}`);
      });
    }
  }

  public removeRole() {
    this.roleService.removeRole(this.crtRole.id).then(async response => {
      this.ngOnInit();
      this.toastr.success(`Server response: ${response}`);
      this.closeRoleRemovalPopup();
    }, error => {
      console.log(`Error ${error.error}`);
      this.toastr.error(`Error ${error.error}`);
    });
  }


  //selection changes methods
  public onSelectionUserChange(event: any) {
    let userId = event.source.value;
    this.crtSelectedUser = this.users.find(u => u.id == userId)!;
    this.crtSelectedUser.position = this.positions.find(p => p.positionId == this.crtSelectedUser.positionId);
  }

  public assignUserToRole() {
    if(this.assignRoleFormGroup.valid) {
      let values = {
        userId : this.assignRoleFormGroup.value.user,
        roleId : this.crtRole.id
      };

      this.roleService.assignUserToRole(values).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeAssignRolePopup();
      }, error => {
        console.log(`Error ${error}`);
        this.toastr.error(`Error ${error}`);
        this.closeAssignRolePopup();
      });
    }
  }

  public removeUserInRole() {
    if(this.crtSelectedUser != null) {
      let userId = this.crtSelectedUser.id;
      this.roleService.removeUserInRole(userId).then(async response => {
        this.ngOnInit();
        this.toastr.success(`Server response: ${response}`);
        this.closeUserInRoleRemoval();
      }, error => {
        console.log(`Error ${error}`);
        this.toastr.error(`Error ${error}`);
        this.closeUserInRoleRemoval();
      });
    }
  }

}
