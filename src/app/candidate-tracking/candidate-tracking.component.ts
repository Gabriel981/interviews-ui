import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Submission } from '../objects/Submission';
import { User } from '../objects/User';
import { AccountService } from '../_services/account.service';
import { AdminService } from '../_services/admin.service';
import { ManagerService } from '../_services/manager.service';

@Component({
  selector: 'app-candidate-tracking',
  templateUrl: './candidate-tracking.component.html',
  styleUrls: ['./candidate-tracking.component.css']
})
export class CandidateTrackingComponent implements OnInit {

  //table

  public availableSubmissions: Submission[] = [];
  public displayedColumns: string[] = ['completeName', 'positionName', 'submissionDate', 'moreDetails', 'approveCandidate', 'rejectCandidate', 'requestChange'];
  public displayedColumnsFilters: string[] = ['completeNameFilter', 'positionNameFilter', 'submissionDateFilter'];
  // dataSource = this.availableSubmissions;

  //paginator
  public pageLength: number = 0;
  public splicedData!: Submission[];
  public pageSize = 10;
  public pageSizeOptions = [5, 10, 15, 20];

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort, {static: false}) sort: MatSort | undefined;
  // @ViewChild('fromInput', {read: MatInput}) fromInput: MatInput | undefined;
  // @ViewChild('toInput', {read: MatInput}) toInput: MatInput | undefined;

  // public dataSource: any;

  public sortedData!: any;
  
  public candidateSubmissionDetails!: FormGroup;
  public candidateApproval!: FormGroup;
  public candidateRejection!: FormGroup;

  private submissionId!: number;
  private managerUser!: User;
  private submissionStatus!: string;

  private selectedCandidateId!: string;

  public dataSource = new MatTableDataSource<Submission>();
  public timer = 1000;
  public timeoutVal = 1000;

  public searchElement: any;

  public fromDate!: any;
  public toDate!: any;

  constructor(
    private managerService: ManagerService,
    private accountService: AccountService,
    private modalService: NgbModal,
    private adminService: AdminService,
    private toastr: ToastrService) {
      
      this.managerUser = this.accountService.currentUserValue;
  }

  ngOnInit(): void {
    this.GetAllAvailableSubmissions();
    // this.getCurrentUser();
  }

  public sortData(sort: Sort) {
    const data = this.splicedData.slice();
    if(!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch(sort.active) {
        case 'positionName': return this.compare(a.appliedPosition.positionName!, b.appliedPosition.positionName!, isAsc);
        case 'submissionDate': return this.compare(a.dateOfSubmission.toString(), b.dateOfSubmission.toString(), isAsc);
        default: return 0;
      }
    });
  }

  public resetForm() {
    this.fromDate = undefined;
    this.toDate = undefined;

    this.GetAllAvailableSubmissions();
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public applyFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSearchResults(elements);
    }, this.timeoutVal);
  }

  public applyNameFilter(event: any) {
    let element = (<HTMLInputElement>event).value;

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSubmissionsByNameFilter(elements);
    }, this.timeoutVal);
  }

  public applyPositionFilter(event: any) {
    let element = (<HTMLInputElement>event).value;
    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        key: element
      };

      this.getSubmissionByPositionFilter(elements);
    }, this.timeoutVal)
  }

  public applySubmissionDateFilter(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    let dataRangeStartValue = dateRangeStart.value.toString();
    let dataRangeEndValue = dateRangeEnd.value.toString();

    window.clearTimeout(this.timer);
    this.timer = window.setTimeout(() => {
      let elements = {
        startDate: dataRangeStartValue,
        lastDate: dataRangeEndValue
      };

      this.getSubmissionsByDateFilter(elements);
    }, this.timeoutVal);
  }

  public getSubmissionsByNameFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.GetAllAvailableSubmissions();
      } else {
        this.adminService.searchByName(key).then((response) =>{
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public getSubmissionByPositionFilter(key: any) {
    if(key != null) {
      if(key.key == "") {
        this.GetAllAvailableSubmissions();
      } else {
        this.adminService.searchByPosition(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
    }
  }

  public getSubmissionsByDateFilter(key: any) {
    if(key.startDate == "" && key.endDate == "") {
      this.GetAllAvailableSubmissions();
    } else {
      this.adminService.searchByDate(key).then((response) => {
        this.pageLength = response.length;
        this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
  
        this.dataSource.data = this.splicedData;
        this.pageSizeOptions = [1, response.length];
      }, error => {
        console.log(error.error);
        this.toastr.error(`Error: ${error.error}`);
      });
    }
  }

  public handleKeyUp(e) {
    
  }

  public handleKeyPress(e) {
    window.clearTimeout(this.timer);
  }

  public pageChangeEvent(event) {
    const offset = ((event.pageIndex + 1) - 1) * event.pageSize;
    this.dataSource.data = this.availableSubmissions.slice(offset).slice(0, event.pageSize);
  }

  public GetAllAvailableSubmissions(){
    this.managerService.GetAllSubmissionsData().then((response) => {
      this.availableSubmissions = response;
      // let retrievedSubmissions = response;
      // retrievedSubmissions.forEach(retrievedSubmission => {
      //   this.availableSubmissions.push(retrievedSubmission);
      // });

      this.pageLength = response.length;
      this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);

      this.dataSource.data = this.splicedData;
    }, error =>{
      console.log(error.error);
      this.toastr.error(`Error: ${error.error}`);
    });
  }

  public openDetails(targetModal: any, submission:Submission){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    let dateOfSubmissionNotFormatted = new DatePipe('en-US');
    let dateOfSubmissionFormated = dateOfSubmissionNotFormatted.transform(submission.dateOfSubmission, 'dd/MM/yyyy');

    let dateOfBirthNotFormatted = new DatePipe('en-US');
    let dateOfBirthFormatted = dateOfBirthNotFormatted.transform(submission.user?.dateOfBirth, 'dd/MM/yyyy');

    this.candidateSubmissionDetails = new FormGroup({
      candidateFirstName: new FormControl(submission.user?.firstName!),
      candidateLastName: new FormControl(submission.user?.lastName),
      candidateAddress: new FormControl(submission.user?.address),
      candidateDateOfBirth: new FormControl(dateOfSubmissionFormated),
      candidateEmailAddress: new FormControl(submission.user?.email),
      candidatePhoneNumber: new FormControl(submission.user?.phoneNumber),
      candidateSubmissionDate: new FormControl(dateOfSubmissionFormated),
      candidateAppliedPosition: new FormControl(submission.appliedPosition.positionName),
      candidateCurrentStatus: new FormControl(submission.status)
    });

    this.selectedCandidateId = submission.user?.id!;
  }

  public openApproval(targetModal: any, submission: Submission){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.submissionId = submission.submissionId!;

    let dateOfSubmissionApprovedNotFormatted = new DatePipe('en-US');
    let dateOfSubmissionApprovedFormatted = dateOfSubmissionApprovedNotFormatted.transform(submission.dateOfSubmission, 'dd/MM/yyyy');

    this.candidateApproval = new FormGroup({
      candidateFirstNameApproval: new FormControl(submission.user?.firstName),
      candidateLastNameApproval: new FormControl(submission.user?.lastName),
      candidateEmailAddressApproval: new FormControl(submission.user?.email),
      candidatePhoneNumberApproval: new FormControl(submission.user?.phoneNumber),
      candidateSubmissionDateApproval: new FormControl(dateOfSubmissionApprovedFormatted),
      candidateAppliedPositionApproval: new FormControl(submission.appliedPosition.positionName),
      candidateCurrentStatusApproval: new FormControl(submission.status)
    });

    this.submissionStatus = submission.status;
  }

  public get candidateCurrentStatusApproval(){
    return this.candidateApproval.get('candidateCurrentStatusApproval');
  }

  public openRejection(targetModal: any, submission: Submission){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });

    this.submissionId = submission.submissionId!;

    let dateOfSubmissionRejectedNotFormatted = new DatePipe('en-US');
    let dateOfSubmissionRejectedFormatted = dateOfSubmissionRejectedNotFormatted.transform(submission.dateOfSubmission, 'dd/MM/yyyy');

    this.candidateRejection = new FormGroup({
      candidateFirstNameRejection: new FormControl(submission.user?.firstName),
      candidateLastNameRejection: new FormControl(submission.user?.lastName),
      candidateEmailAddressRejection: new FormControl(submission.user?.email),
      candidatePhoneNumberRejection: new FormControl(submission.user?.phoneNumber),
      candidateSubmissionDateRejection: new FormControl(dateOfSubmissionRejectedFormatted),
      candidateAppliedPositionRejection: new FormControl(submission.appliedPosition.positionName),
      candidateCurrentStatusRejection: new FormControl(submission.status)
    });

    this.submissionStatus = submission.status;
  }

  public openRequest(targetModal: any, submission: Submission){
    this.modalService.open(targetModal, {
      centered : true,
      backdrop: 'static',
      size: 'lg'
    });

    this.submissionId = submission.submissionId!;

    this.submissionStatus = submission.status;
  }


  public approveCandidate(){
    if(this.submissionStatus == "Accepted" || this.submissionStatus == "Rejected"){
      this.showApprovalWarningToast();
      this.modalService.dismissAll();
    } else {
      var managerId = this.managerUser.id!;
      this.managerService.ApproveCandidate(this.submissionId, managerId).then((response) => {
        this.showApprovalSuccessToast(true);
        this.modalService.dismissAll();
      });
    }
  }

  public rejectSubmission(){
    if(this.submissionStatus == "Accepted" || this.submissionStatus == "Rejected"){
      this.showApprovalWarningToast();
      this.modalService.dismissAll();
    } else {
      var managerId = this.managerUser.id!;
      this.managerService.RejectCandidate(this.submissionId, managerId).then((response) => {
        this.showApprovalSuccessToast(false);
        this.modalService.dismissAll();
      });
    }
  }

  public requestStatusChange(){
    if(this.submissionStatus == "Accepted" || this.submissionStatus == "Rejected"){
      var managerId = this.managerUser.id!;
      this.managerService.RequestStatusChange(this.submissionId, managerId).then(() => {
        let element: HTMLElement = document.getElementById('auto_trigger_close_request') as HTMLElement;
        element.click();
        this.showRequestSentSuccessToast(true);
      });
    } else {
      this.showRequestNotPermitted();
    }
  }

  public requestCandidateRemoval(){
    if(this.submissionStatus == "Accepted" || this.submissionStatus == "Rejected"){
      var managerId = this.managerUser.id!;
      this.managerService.RequestCandidateRemoval(this.submissionId, managerId).then(() => {
        let element: HTMLElement = document.getElementById('auto_trigger_close_request') as HTMLElement;
        element.click();
        this.showCandidateRemovalRequestSuccess(true);
      });
    } else {
      this.showCandidateRemovalRequestSuccess(false);
    }
  }

  public openAttachedCV(){
    this.managerService.GetSelectedCandidateCV(this.selectedCandidateId).then((response => {
      if(response.cv == null) {
        this.showNoCVAvailableToast();
      } else {
        var cvOpenLink = response.cv;
        window.open(cvOpenLink, "_blank");
      }
    }));
  }

  public exportData(){
    this.managerService.ExportDataExcel().then((response) => {
      console.log(response);
      this.downloadFile(response);
      this.showToastOnGeneratedFileSuccess();
    });
  }


  public getSearchResults(key: any) {
    console.log('get Search results');
    if(key != null) {
      if(key.key == "") {
        this.GetAllAvailableSubmissions();
        this.pageSizeOptions.forEach((element) => {
          this.pageSizeOptions.pop();
        });
        this.pageSizeOptions = [5, 10, 15, 20];
      } else {
        this.adminService.searchByKey(key).then((response) => {
          this.pageLength = response.length;
          this.splicedData = response.slice(((0 + 1) - 1) * this.pageSize).slice(0, this.pageSize);
    
          this.dataSource.data = this.splicedData;
          this.pageSizeOptions = [1, response.length];
          // this.dataSource.data = response as Submission[];
        }, error => {
          console.log(error.error);
          this.toastr.error(`Error: ${error.error}`);
        });
      }
      
    }
  }

  // public getCurrentUser(){
  //   this.accountService.getCurrentUser().then(response => {
  //     if(response != null){
  //       this.managerUser = response;
  //     }
  //   });
  // }

  public showApprovalSuccessToast(isAccepted: boolean){
    if(isAccepted == true){
      this.toastr.success("The selected candidate was approved with success! A notification was sent to him!")
    } else {
      this.toastr.success("The selected candidate was succesfully rejected! A notification was sent to him!");
    }
  }

  public showRequestSentSuccessToast(isSent: boolean){
    if(isSent == true){
      this.toastr.success("A change status notification was successfully sent to the admins!");
    } else {
      this.toastr.error("Cannot send the status change notification to admins! Try again!");
    }
  }

  public showCandidateRemovalRequestSuccess(isSent: boolean){
    if(isSent == true){
      this.toastr.success('A proposal for removing the selected candidate was sent to admins!');
    } else {
      this.toastr.error("Cannot send a proposal for removing the selected candidate! Try again!");
    }
  }
  
  public showRequestNotPermitted(){
    this.toastr.warning("Cannot send a status change notification to admins when the status is another than Accepted or Rejected");
  }

  public showApprovalWarningToast(){
    this.toastr.warning("Cannot send a request to the server because the candidate status is already accepted or rejected");
  }

  public showToastOnGeneratedFileSuccess(){
    this.toastr.success("The export was successfully executed. Check your download tab");
  }

  public downloadFile(data: any){
    var currentTime = new Date();
    var currentTimeFormatted = formatDate(currentTime, 'dd/MM/yyyy HH:mm:ss', 'en-US');

    let blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;'});
    let url = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Submissions ' + currentTimeFormatted);
    document.body.appendChild(link);
    link.click();
  }

  public showNoCVAvailableToast(){
    this.toastr.warning("No CV uploaded in the database");
  }
}
