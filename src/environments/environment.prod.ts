export const environment = {
  production: true
};

export const defaultVars = {
  defaultURI: 'http://localhost:5001/',
  debugURI: 'http://localhost:62247/'
}
